@extends('school.settings.app')

@section('content')
<section class="content">
  <div class="activity_log_main">

    <div class="mdc-top-app-bar__title">Activity Logs</div>
    <div class="card">
      <div class="card-body">
        <div class="text-center pb-3"><strong class="activity-head">September 2018</strong></div>
        <div class="date_sequence">
          <h5 class="card-title py-2 border-top border-bottom"><strong>Yesterday</strong></h5>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-user-o" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created <span class="activity-content">subuser</span> ram(ram009).</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
        </div>
        <div class="date_sequence">
          <h5 class="card-title py-2 border-top border-bottom"><strong>20, September</strong></h5>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-book" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created a&nbsp;<span class="activity-content">course</span>&nbsp;Master of Computer Application</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-user-o" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created <span class="activity-content">subuser</span> ram(ram009).</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
        </div>
      </div>
    </div>

    <div class="card mt-3">
      <div class="card-body">
        <div class="text-center pb-3"><strong class="activity-head">September 2018</strong></div>
        <div class="date_sequence">
          <h5 class="card-title py-2 border-top border-bottom"><strong>Yesterday</strong></h5>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-user-o" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created <span class="activity-content">subuser</span> ram(ram009).</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
        </div>
        <div class="date_sequence">
          <h5 class="card-title py-2 border-top border-bottom"><strong>20, September</strong></h5>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-book" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com</h4><span class="summary">created a&nbsp;<span class="activity-content">course</span>&nbsp;Master of Computer Application</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
           <ul class="activity_head">
              <li class="info-box-text"><i class="fa fa-user-o" aria-hidden="true"></i></li>
              <li class="desc_security">
                 <p class="desc_activity">info@sinhgadeducation.com</h4><span class="summary">created <span class="activity-content">subuser</span> ram(ram009).</span></p>
                 <p class="mb-0"><small>September 13, 2018</small></p>
              </li>
           </ul>
        </div>
      </div>
    </div>

  </div>
</section>    
@endsection