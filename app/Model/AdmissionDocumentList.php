<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionDocumentList extends Model
{
    protected $table = 'tbl_admission_document_list';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['course_detail_id','document_name','deleted','created_at','deleted_at'];    
}
