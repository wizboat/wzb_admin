<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Model\User;
use App\Model\State;
use App\Model\PaymentTransaction;
use App\Model\SchoolPack;
use App\Model\SchoolProfile;
use App\Model\CourseMaster;
use App\Model\LinkCourseSchool;
use App\Mongo\mSchoolProfile;
use App\Mail\EmailVerifyMail;
use Auth;
use Hash;
use Session;

class SchoolStepsController extends Controller{

    public function __construct(){

        $this->middleware('auth');
    }

    protected function schoolProfileStepsViews(){

        if(!Auth::user()->email_verify){

            return view('auth.school.email_verify');

        }else if(!Auth::user()->profile_verify){

            $state = State::all();
            return view('auth.school.profile_verify',compact('state'));

        }else if(!Auth::user()->school_verify){

            return view('auth.school.profile_review');

        }else if(!Auth::user()->course_verify){

            $course = CourseMaster::all();         
            return view('auth.school.course_selection',compact('course'));

        }else{
            return redirect('/');
        }
    }
    
    protected function emailVerifyView(){
        if(!Auth::user()->email_verify){
            return view('auth.school.email_verify');
        }else if(!Auth::user()->profile_verify){
            return redirect('school-profile-verify');
        }else if(!Auth::user()->school_verify){
            return redirect('school-profile-review');
        }else if(!Auth::user()->course_verify){
            return redirect('course-selections');            
        }else{
            return redirect('/');
        }        
    }

    protected function emailVerifyUpdate(Request $request){

        $post = $request->all();   

        $validator = Validator::make($request->all(), [            
            'email_verify_code' => 'required'         
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $otp = Auth::user()->otp;

        if($otp == $request->email_verify_code){

            $user = User::where('id',Auth::user()->id)->first();
            $user->email_verify = 1;
            $user->save();
            return redirect('school-profile-verify');
        }else{
            $validator = array('email_verify_code'=>'Enter valid email verify code');
            return redirect()->back()->withInput()->withErrors($validator);   
        }      
    }

    protected function resendEmail(Request $request){

        $otp = rand(1,999999);            
        $userData = User::where('id',Auth::user()->id)->first();
        $userData->otp = $otp;
        $userData->save();

        Mail::to('chocolateboy7900@gmail.com')->send(new EmailVerifyMail($userData));

        return redirect('school/email-verify');
    }

    protected function reviewVerifyView(){
        if(!Auth::user()->email_verify){
            return redirect('school/email-verify');
        }else if(!Auth::user()->profile_verify){
            return redirect('school-profile-verify');
        }else if(!Auth::user()->school_verify){

            return view('auth.school.profile_review');
        }else if(!Auth::user()->course_verify){
            return redirect('course-selections');            
        }else{
            return redirect('/');
        }
    }
    
  //   protected function paymentVerifyUpdate(Request $request){

  //     $post = $request->all();
  //     $user = User::where('id',Auth::user()->id)->first();
  //     $user->payment_verify = 1;
  //     $user->save();

  //     $PaymentTransaction = new PaymentTransaction();
  //     $PaymentTransaction->user_id = Auth::user()->id;
  //     $PaymentTransaction->user_type = 'school';
  //     $PaymentTransaction->description = 'account_activation';
  //     $PaymentTransaction->transaction_status = 1;
  //     $PaymentTransaction->transaction_type = 'account_activation';
  //     $PaymentTransaction->amount = 3999;
  //     $PaymentTransaction->save();

  //     $SchoolPack = new SchoolPack();
  //     $SchoolPack->user_id = Auth::user()->id;
  //     $SchoolPack->transaction_id = $PaymentTransaction->id;
  //     $SchoolPack->pack_id = 1;
  //     $SchoolPack->start_date = date("Y-m-d");
  //     $SchoolPack->end_date = date('Y-m-d', strtotime('+1 years'));
  //     $SchoolPack->save();

  //     return redirect('school-profile-verify');
  // }

    protected function schoolProfileVerifyView(){
        if(!Auth::user()->email_verify){
            return redirect('school/email-verify');
        }else if(!Auth::user()->profile_verify){
            $state = State::all();
            return view('auth.school.profile_verify',compact('state'));
        }else if(!Auth::user()->school_verify){
            return redirect('/school-profile-review');         
        }else if(!Auth::user()->course_verify){
            return redirect('course-selections');            
        }else{
            return redirect('/');
        }   
    }

    protected function schoolProfileVerifyUpdate(Request $request){

        $post = $request->all();

        $validator = Validator::make($request->all(), [
            '_token' => 'required',
            'school_name' => 'required|min:5|max:70',
            'board' => 'required|min:2|max:50',
            'address1' => 'required|min:5|max:50',
            'address2' => 'nullable|min:5|max:50',
            'city' => 'required|max:70',
            'state' => 'required|max:30',
            'about' => 'required|min:50',
            'pincode' => 'required|numeric|min:6',
            'phone1' => 'required|numeric|min:10',
            'phone2' => 'nullable|numeric',
            'phone3' => 'nullable|numeric',
            'school_email' => 'required|max:50',         
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $state = State::find(request('state'));

        if(!isset($state)){
            return redirect()->back();     
        }

        $user = User::where('id',Auth::user()->id)->first();
        $user->profile_verify = 1;
        $user->save();

        //******************MySql School Profile********************//
        $SchoolProfile = SchoolProfile::where('user_id',Auth::user()->id)->first();      


        if(!isset($schoolProfile->id)){          
            $SchoolProfile = new SchoolProfile();      
        }

        $SchoolProfile->user_id = Auth::user()->id;
        $SchoolProfile->school_name = request('school_name');
        $SchoolProfile->board = request('board');
        $SchoolProfile->address1 = request('address1');
        $SchoolProfile->address2 = request('address2');
        $SchoolProfile->city = request('city');
        $SchoolProfile->state = request('state');
        $SchoolProfile->about = request('about');
        $SchoolProfile->pincode = request('pincode');
        $SchoolProfile->phone1 = request('phone1');
        $SchoolProfile->phone2 = request('phone2');
        $SchoolProfile->school_email = request('school_email');
        $SchoolProfile->save();   
        $SchoolProfile->school_hash = Hash::make($SchoolProfile->id);
        $SchoolProfile->save();


        //******************MongoDB School Profile********************//
        // $mogoSchoolProfile = mSchoolProfile::where('schoolId',Auth::user()->id)->first();      

        // if(!isset($schoolProfile->id)){
        //     $mogoSchoolProfile = new mSchoolProfile();      
        // }

        // $mogoSchoolProfile                  = new mSchoolProfile();
        // $mogoSchoolProfile->schoolId        = Auth::user()->id;      
        // $mogoSchoolProfile->schoolName      = request('school_name');
        // $mogoSchoolProfile->schoolBoard     = request('board');
        // $mogoSchoolProfile->schoolAddress1  = request('address1');
        // $mogoSchoolProfile->schoolAddress2  = request('address2');
        // $mogoSchoolProfile->schoolCity      = request('city');
        // $mogoSchoolProfile->schoolState     = $state->state;
        // $mogoSchoolProfile->schoolAbout     = request('about');
        // $mogoSchoolProfile->schoolPincode   = request('pincode');
        // $mogoSchoolProfile->schoolPhone1    = request('phone1');
        // $mogoSchoolProfile->schoolPhone2    = request('phone2');
        // $mogoSchoolProfile->schoolSchoolEmail = request('school_email');
        // $mogoSchoolProfile->courseList = [];
        // $mogoSchoolProfile->save();

        $mongoSchoolProfileId = Auth::user()->id;

        Session::put('mongoSchoolProfileId', $mongoSchoolProfileId);

        return redirect('/school-profile-review');      
    }   

    protected function schoolCourseVerifyView(){
        // echo Auth::user()->school_verify; exit;
        if(!Auth::user()->email_verify){
            return redirect('school/email-verify');
        }else if(!Auth::user()->profile_verify){            
            return redirect('school-profile-verify');
        }else if(!Auth::user()->school_verify){            
            return redirect('school-profile-review');
        }else if(!Auth::user()->course_verify){

            if(!Session::has('mongoSchoolProfileId')){
                // $profileData = mSchoolProfile::where('schoolId',Auth::user()->id)->first();
                 // Session::put('mongoSchoolProfileId', $profileData['_id']);                
            }
            $course = CourseMaster::all();         
            return view('auth.school.course_selection',compact('course'));        
        }else{
            return redirect('/');
        }   
    }

    protected function schoolCourseSelectionVerifyUpdate(Request $request, $id){
        // print_r(request('courseList')); exit;  
        $post = $request->all();      
        $validator = Validator::make($request->all(), [
            '_token' => 'required',
            'courseList' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 404,
                'message' => 'Please select atleast 2 course'
            ]); 
        }

        if(sizeof($post['courseList']) <  2 ){
            return response()->json([
                'code' => 404,
                'message' => 'Please select atleast 2 coursee'
            ]); 
        }

        if (Session::has('mongoSchoolProfileId'))
        {
            if(Session::get('mongoSchoolProfileId') == $id)
            {
                $courseList = CourseMaster::all();                     
                $prefix = $fruitList = '';
                foreach ($courseList as $fruit)
                {
                    $fruitList .= $prefix . '' . $fruit->id . '';
                    $prefix = ', ';
                }
                $arr=explode(",",$fruitList);
                $flag = true;
                foreach ($post['courseList'] as $key => $value) {
                    if(!in_array($key,$arr)){
                        $flag = false;
                    }
                }

                if( $flag == false ){
                    return response()->json([
                        'code' => 404,
                        'message' => 'Something went wrong'
                    ]);
                }            

                // $mongoSchoolProfile = mSchoolProfile::find($id);          
                // $mongoSchoolProfile->courseList = request('courseList');
                // $mongoSchoolProfile->update();   

                //**************** Start Update course verify flag **************//
                $user = User::find(Auth::user()->id);
                $user->course_verify = 1;
                $user->update();
                //****************End Update course verify flag **************//
                    // print_r(request('courseList')); exit;
                $list = request('courseList');
                foreach($list as $key=>$value){                    
                    $LinkCourseSchool = new LinkCourseSchool();
                    $LinkCourseSchool->user_id = Auth::user()->id;
                    $LinkCourseSchool->course_master_id = $key;
                    $LinkCourseSchool->save();
                }

                if($user->course_verify == 1){               
                    return response()->json([
                        'code' => 200,
                        'message' => 'Successfully course updated, you will be redirected on dashboard'
                    ]);
                }
            }else{
                return response()->json([
                    'code' => 404,
                    'message' => 'Something went wrong'
                ]);
            }
        }else{
            Auth::logout();
            return redirect('/login');
        }           
    }      
}
