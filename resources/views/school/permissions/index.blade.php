@extends('layouts.app')

@section('title', '| Permissions')

@section('content')
<section class="content mt-5" > 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
            <div class="card mb-5">
                <div class="card-body">
                    <span class="box-title"><i class="fa fa-key pr-2" aria-hidden="true"></i><strong>Available Permissions</strong></span>
                        <!-- <a href="{{ route('users.index') }}" class="btn btn-primary btn-sm pull-right">Users</a> -->
                    <div class="">  
                        <div class="">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item pl-0">
                                    Permissions                 
                                </li>
                                @foreach ($permissions as $permission)
                                    <li class="list-group-item pl-0">
                                        {{ $permission->name }}                     
                                    </li>
                                @endforeach
                            </ul>  
                            <a href="{{ url()->previous() }}" class="btn btn-sm btn-outline-secondary mt-3 m-0">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


    <!-- 
    <section class="content mt-5" > 
    <div class="col-lg-10 col-lg-offset-1">
        <h1><i class="fa fa-key"></i>Available Permissions
        <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Users</a>
        <a href="{{ route('roles.index') }}" class="btn btn-default pull-right">Roles</a></h1>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Permissions</th>                    
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>                     
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>    
    </div>
    </section> -->
@endsection