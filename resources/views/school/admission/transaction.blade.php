@extends('layouts.app')

@section('content')
<section class="content">                                                     
    <div class="">
        <div id="msg"></div>
        <div class="mbdtable">
            <div class="mdc-top-app-bar__title pb-0">Filters</div>
            <section class="content" style="display: inline-flex;">
                <div class="form-group mr-3">
                    <input type="text" name="search" id="search" class="form-control mbd-label" placeholder="Admission Reference">          
                </div>
                <div class="form-group mr-3">
                    <select name="" id="" class="form-control mbd-select">
                        <option value=""> Course Name </option>
                        @foreach ( $courseDetails as $value )
                        <option value="{{ $value->id }}">{{ $value->course_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mr-3">
                    <select name="" id="" class="form-control mbd-select">
                        <option value="">Status</option>
                        <option value="">Active</option>
                        <option value="">Inactive</option>
                        <option value="">Close</option>
                    </select>
                </div>
                <div class="form-group mr-3">
                    <input type="text" name="" class="form-control mbd-label" placeholder="Date">                
                </div>
            </section>
            <div class="main-panel">
              <section class="content">
                  <div class="mainbar__title">Admission Transaction</div>
              </section>
          </div>
          total<h3 id="total_records"></h3>
          <table id="admissionTransactionTable" class="table cardmbd table-borderless"></table>
      </div>
  </div>
</section>


<script>



    var BS_COLUMNS  = [];
    function indexFormatter( value, row, index ){

        return index+1;
    }
    function nameFormatter( value, row, index ){
      return row.firstname + " " + row.lastname;
  }

  function statusFormatter( value, row, index ){

      return row.status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
  }
  function detailsFormatter( value, row, index ){
    var url = '{{ url("admission/transaction/:id/view") }}';

    url = url.replace(':id', row.id);

    return "<a href="+url+" class='btn btn-outline-warning btn-md mr-3 m-0 weight-bold'>View</a>";
}

BS_COLUMNS.unshift(
{
    field       : 'id',
    title       : 'Sr.No',
    align       : 'center' ,
    formatter   : indexFormatter
},
{
    field       : 'addmission_refrence',
    title       : 'Admission Refrence',
    align       : 'center' 
},
{         
    field       : 'course_name',
    title       : 'Course Name',
    align       : 'center' 
},
{
    field       : 'firstname',
    title       : 'Name',
    align       : 'center',
    formatter   : nameFormatter
},
{
    field       : 'status',
    title       : 'Status',
    align       : 'center',
    formatter 	: statusFormatter
},
{
    field       : 'created_at',
    title       : 'Admission Date/Time',
    align       : 'center'
},
{
    field       : '',
    title       : 'Action',
    align       : 'center',
    formatter   : detailsFormatter
}
);

var BS = {
    url             : '{{ url("admission/transactionapi") }}',
    columns         : BS_COLUMNS,
    pagination      : true,
    search          : true,        
    sidePagination  : 'client',
    PageRefresh     : true,
    pageNumber      : 1,
    pageSize        : 10,
    showRefresh     : true
};

var t= $("#admissionTransactionTable").bootstrapTable(BS);
</script>
<script>
    
    $(document).ready(funtion(){

        fetch_customer_data();

        function fetch_customer_data( query = '' )
        {
            $.ajax({
                url: "{{ route('live_search.action') }}",
                method: 'GET',
                data: {query:query},
                dataType:'json'
                success:function(data)
                {
                    $('tbody').html(data.table_data);
                    $('#total_records').text(data.table_data);
                }
            })
        }

        $(document).on('keyup', '#search', function(){
            var query = $(this).val();
            fetch_customer_data(query);
        });
    });
</script>
@endsection