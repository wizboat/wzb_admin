<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CourseMaster;
use App\Mongo\mongoSchoolProfile;
use Session;
use Auth;
use App\Model\State;

class HomeController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test(){
        return view('merit_list');
    }

    public function printTest(){
        return view('print_merit_list');
    }

    public function index()
    {
        if(!Auth::user()->email_verify)
        {
            return view('auth.school.email_verify');
        }
        else if(!Auth::user()->payment_verify)
        {
            return view('auth.school.payment_verify');
        }
        else if(!Auth::user()->profile_verify)
        {
            $state = State::all();
            return view('auth.school.profile_verify',compact('state'));
        }
        else if(!Auth::user()->course_verify)
        {            
            $course = CourseMaster::all();     
            
            $mogoSchoolProfile = mongoSchoolProfile::where('schoolId', Auth::user()->id)->first();   

            if(isset($mogoSchoolProfile->_id)){
                Session::put('mongoSchoolProfileId', $mogoSchoolProfile->_id);
            }else{
                Auth::logout();
                return redirect('/login');
            }            
            return view('auth.school.course_selection',compact('course'));

        }else{
            return view('home');
        }       
    }
}
