@extends('layouts.app')

@section('content')

<section class="content">                                                     
  <div class="">
    <div class="mbdtable">
      <div class="main-panel">
        <section class="content">
          <div class="admission_inner mb-5">
            <div class="box">
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="admission" data-toggle="tab" href="#admission-details" role="tab" aria-controls="admission-details"
                  aria-selected="true">Admission Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="payment" data-toggle="tab" href="#payment-details" role="tab" aria-controls="payment-details"
                  aria-selected="false">Payment Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="personal" data-toggle="tab" href="#personal-details" role="tab" aria-controls="personal-details"
                  aria-selected="false">Personal Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="document" data-toggle="tab" href="#document-details" role="tab" aria-controls="document-details"
                  aria-selected="false">Document Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="admission-steps" data-toggle="tab" href="#admission-steps-details" role="tab" aria-controls="admission-steps-details"
                  aria-selected="false">Admission Steps Details</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="academic" data-toggle="tab" href="#academic-details" role="tab" aria-controls="academic-details"
                  aria-selected="false">Academic Details</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="admission-details" role="tabpanel" aria-labelledby="admission-details">
                  <div class="mainbar__title sub_mainbar">Admission Details</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>Addmission Refrence #</th>
                          <th>Course</th>
                          <th>Status</th>
                          <th>Admission Date/Time</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->addmission_refrence}}</td>
                          <td>{{ $data[0]->course_name}}</td>
                          <td><span class="label label-success">{{ $data[0]->status}}</span></td>
                          <td>{{ $data[0]->created_at}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane fade" id="payment-details" role="tabpanel" aria-labelledby="payment-details">
                  <div class="mainbar__title sub_mainbar">Payment Details</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>Transaction Id</th>
                          <th>Status</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->course_name}}</td>
                          <td> 
                            <h6  class="course_status"><span class="badge badge-success">Active</span></h6>
                          </td>
                          <td>5500</td>
                        </tr>
                      </tbody>
                    </table> 
                  </div>
                </div>

                <div class="tab-pane fade" id="personal-details" role="tabpanel" aria-labelledby="personal-details">
                  <div class="mainbar__title sub_mainbar">Personal Details</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Middle Name</th>
                          <th>Last Name</th>
                          <th>Gender</th>
                          <th>Father Name</th>
                          <th>Mother Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->firstname}}</td>
                          <td>{{ $data[0]->middlename}}</td>
                          <td>{{ $data[0]->lastname}}</td>
                          <td>{{ $data[0]->gender}}</td>
                          <td>{{ $data[0]->fathername}}</td>
                          <td>{{ $data[0]->mothername}}</td>
                        </tr>
                      </tbody>
                      <thead>
                        <tr>
                          <th>Date Of Birth</th>
                          <th>Email Id</th>
                          <th>Mobile</th>
                          <th>Other Contact</th>
                          <th colspan="2">Category</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->dob}}</td>
                          <td>{{ $data[0]->email_id}}</td>
                          <td>{{ $data[0]->mobile}}</td>
                          <td>{{ $data[0]->other_mobile}}</td>
                          <td colspan="2">{{ $data[0]->category}}</td>
                        </tr>
                      </tbody>
                    </table> 
                  </div>
                  <div class="mainbar__title sub_mainbar">Postal Address</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>Address Line 1</th>
                          <th colspan="3">Address Line 2</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->address_line_1}}</td>
                          <td colspan="3">{{ $data[0]->address_line_2}}</td>
                        </tr>
                      </tbody>
                      <thead>
                        <tr>
                          <th>State</th>
                          <th>Locality/Town</th>
                          <th>Pincode</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->state}}</td>
                          <td>{{ $data[0]->locality}}</td>
                          <td>{{ $data[0]->pincode}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="mainbar__title sub_mainbar">Permanent Address</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>Permanent address line 1</th>
                          <th colspan="3">permanent address line 2</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->permanent_address_line_1}}</td>
                          <td colspan="3">{{ $data[0]->permanent_address_line_2}}</td>
                        </tr>
                      </tbody>
                      <thead>
                        <tr>
                          <th>State</th>
                          <th>Locality/Town</th>
                          <th>Pincode</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data[0]->permanent_state}}</td>
                          <td>{{ $data[0]->permanent_town}}</td>
                          <td>{{ $data[0]->permanent_pincode}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane fade" id="document-details" role="tabpanel" aria-labelledby="document-details">
                  <div id="msg"></div>
                  <div class="mbdtable">
                    <div class="main-panel">
                      <section class="content">
                        <div class="mainbar__title sub_mainbar">Document Details</div>
                      </section>
                    </div>
                    <table class="table cardmbd table-borderless table-bordered table-hover text-center valign-middle">
                      <thead>
                        <tr>
                          <th width="80">Sr. No.</th>
                          <th>Images</th>
                          <th>Status</th>
                          <th>Permission</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td><img src="{{ url('assets/images/Hs-art-design.jpg') }}" width="100" /></td>
                          <td>Active</td>
                          <td>
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Approve </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View</a></td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td><img src="{{ url('assets/images/Hs-art-design.jpg') }}" width="100" /></td>
                          <td>Active</td>
                          <td>
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Approve </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View</a></td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td><img src="{{ url('assets/images/Hs-art-design.jpg') }}" width="100" /></td>
                          <td>Active</td>
                          <td>
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Approve </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View</a></td>
                        </tr>
                      </tbody>
                    </table> 
                  </div>
                </div>
                <div class="tab-pane fade" id="admission-steps-details" role="tabpanel" aria-labelledby="admission-steps-details">
                  <div class="mainbar__title sub_mainbar">Admission Steps Details</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <tbody>
                        <tr>
                          <td><b>Admisson Date</b></td>
                          <td colspan="2">02/03/2020</td>
                          <td><span class="badge badge-success">Success</span></td>
                        </tr>
                        <tr>
                          <td><b>Merit List</b></td>
                          <td>02/03/2020</td>
                          <td width="300">
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Accept </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><span class="badge badge-success">Success</span></td>
                        </tr>
                        <tr>
                          <td><b>Document Submit</b></td>
                          <td>02/03/2020</td>
                          <td width="300">
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Accept </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><span class="badge badge-success">Success</span></td>
                        </tr>
                        <tr>
                          <td><b>Document Approved</b></td>
                          <td>02/03/2020</td>
                          <td width="300">
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Accept </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><span class="badge badge-success">Success</span></td>
                        </tr>
                        <tr>
                          <td><b>Admission Completed</b></td>
                          <td>25/03/2020</td>
                          <td width="300">
                            <button class="btn btn-outline-success mr-3 m-0 btnTable"><i class="fa fa-check"></i> Accept </button>
                            <button class="btn btn-outline-danger btnTable"><i class="fa fa-times"></i> Reject</button>
                          </td>
                          <td><span class="badge badge-success">Success</span></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane fade" id="academic-details" role="tabpanel" aria-labelledby="academic-details">
                  <div class="mainbar__title sub_mainbar">Academic Details</div>
                  <div class="table-responsive">
                    <table class="table cardmbd table-borderless">
                      <thead>
                        <tr>
                          <th>Examination</th>
                          <th>School/College</th>
                          <th>Board/University</th>
                          <th>Year Passing</th>
                          <th>Marks Type</th>
                          <th>Marks</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($AcademicDetails as $value)
                        <tr>
                          <td>{{ $value['examination'] }}</td>
                          <td>{{ $value['school_college'] }}</td>
                          <td>{{ $value['board_university'] }}</td>
                          <td>{{ $value['year_passing'] }}</td>
                          <td>{{ $value['marks_type'] }}</td>
                          <td>{{ $value['marks'] }}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-6">
                      <div class="academic_img">
                        <h4 class="mainbar__title sub_mainbar">1) Mark Sheet IX</h4>
                        <img src="{{ url('assets/images/marksheet.jpg') }}" class="img-fluid cardmbd" style="object-fit: cover;">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="academic_img">
                        <h4 class="mainbar__title sub_mainbar">2) Mark Sheet X</h4>
                        <img src="{{ url('assets/images/marksheet.jpg') }}" class="img-fluid cardmbd" style="object-fit: cover;">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <a href="confirm.html" class="btn btn-success primaryModal">Approve</a>
                      <a href="confirm.html" class="btn btn-danger ml-2 primaryModal">Reject</a>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          </div>
        </section>
      </div>
    </div>
  </div>
</section>
@endsection