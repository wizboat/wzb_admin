<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SubUser;
use App\Model\User;
use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class UserController extends Controller {

    public function __construct() {

        // $this->middleware(['auth', 'isAdmin','CheckUserStatus']); //isAdmin middleware lets only users with a //specific permission permission to access these resources       
    }
    
    public function index() {        
        $users = User::orderBy('id', 'desc')->paginate(10); 
        $role = Role::all();
        $p = Permission::all();       
        // $permissions = $user->permissions;
        $usersdata = SubUser::with('userdata')->where('school_id',Session::get('school_id'))->get()->toArray();

        return view('school.users.index',['users'=>$users,'permissions'=>$p,'roles'=>$role,'usersdata'=>$usersdata]);
    }
   
    public function create() {        
        $roles = Role::get();
        return view('school.users.create', ['roles'=>$roles]);
    }
   
    public function store(Request $request) 
    {
        $post = $request->all();
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:4|confirmed',
            'roles'=>'required|array'
        ]);      

        $post['user_role'] = 'SU';
        $post['user_status'] = 2;        
        $user = User::create($post);    
        $user->user_hash = bcrypt($user->id);     
        $user->save();

        //---------For Sub User data store
        $subUser['school_id'] = 149;  
        $subUser['master_user_id'] = Auth::user()->id;  
        $subUser['user_id'] = $user->id;
        SubUser::create($subUser);

        $roles = $request['roles']; //Retrieving the roles field
        //Checking if a role was selected
        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();            
                $user->syncRoles($role_r); //Assigning role to user
            }
        }

        $data = [
                    'email' =>  'sksuthar09@gmail.com',
                    'password' => 'ddd',
                    'link'=> url("/payment-online/"),
                    'school_name' => 'sinhgad'
                ];
                $email = $request->email;
                

       
        return redirect()->route('users.index')
            ->with('success_message','User successfully added.');
    }
    
    public function show($id) {
        return redirect('users'); 
    }

    public function edit($id) {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles

        return view('school.users.edit', compact('user', 'roles')); //pass user and roles data to view
    }
    
    public function update(Request $request, $id) {
        $user = User::findOrFail($id); //Get role specified by id
        
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            // 'password'=>'required|min:6|confirmed'
        ]);
        $input = $request->only(['name', 'email']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')->with('success_message','User successfully edited.');
    }

    public function statusUser(Request $request){
        $post = $request->all();
        $validator = \Validator::make($post, ['_token'=>'required','user_hash'=>'required']);

        if ($validator->fails()) {
            return response()->json([
                    'status' => '202',
                    'message' => $validator->messages()
            ]);
        }

        $user = User::where('user_hash',$post['user_hash'])->first();            
        if(!empty($user)){
            $user->user_status = ($user->user_status==1) ? 2 : 1;
            $user->save();    
        }           
        
        if(isset($user->id)){
            return response()->json([
                  'status' => 'true',
                  'code'=>'200',
                  'message' =>'successfully updated'
               ]);      
        }else{
            return response()->json([
                  'status' => 'false',
                  'code'=>'202',
                  'message' =>'Something went wrong1'
               ]);  
        }
        
    }
    public function destroy($id) {
        //Find a user with a given id and delete
        $user = User::findOrFail($id); 
        $user->delete();

        return redirect()->route('users.index')
            ->with('flash_message',
             'User successfully deleted.');
    }
}