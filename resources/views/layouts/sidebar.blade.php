<aside class="mdc-drawer mdc-drawer--dismissible mdc-drawer--open">
  <div class="mdc-drawer__header">
    <a href="{{ url('/') }}" class="brand-logo">
      <img src="{{ url('assets/images/logo.png') }} " class="img-fluid" alt="logo" width="140">
    </a>
  </div>
  <div class="mdc-drawer__content">
    <div class="user-info">
      <p class="name">Krishna Sharma</p>
      <p class="email">krishnasharma@gmail.com</p>
    </div>
    <div class="mdc-list-group">
      <nav class="mdc-list mdc-drawer-menu">
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('/') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">home</i>
            Dashboard
          </a>
        </div>
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('profile') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">person</i>
            Profile
          </a>
        </div>
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('course') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">collections_bookmark</i>
            Course
          </a>
        </div>
        <!-- <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('enrolments') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">account_box</i>
            Enrolments
          </a>
        </div> -->
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('users') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">supervisor_account</i>
            Roles
          </a>
        </div>
        <div class="mdc-list-item mdc-drawer-item">
          <a class="mdc-drawer-link" href="{{ url('faculty') }}">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">pie_chart_outlined</i>
            Faculty
          </a>
        </div>
        <div class="mdc-list-item mdc-drawer-item">
          <button class="drpbtn mdc-expansion-panel-link" data-toggle="expansionPanel" data-target="payment">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">account_balance
            </i>
            Payments
            <i class="mdc-drawer-arrow material-icons">chevron_right</i>
          </button>
          <div class="mdc-expansion-panel" id="payment">
            <nav class="mdc-list mdc-drawer-submenu">
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('payments/ad') }}">
                  UPI
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('bank') }}">
                  Bank
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('payment/confirm_payment') }}">
                  Confirm Transaction
                </a>
              </div>
            </nav>
          </div>
        </div>
        <div class="mdc-list-item mdc-drawer-item">
          <button class="drpbtn mdc-expansion-panel-link" data-toggle="expansionPanel" data-target="admission">
            <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">book</i>
            Admission
            <i class="mdc-drawer-arrow material-icons">chevron_right</i>
          </button>
          <div class="mdc-expansion-panel" id="admission">
            <nav class="mdc-list mdc-drawer-submenu">
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('admission/create') }}">
                  Admission Create
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('admission/status') }}">
                  Admission Report
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('admission/transaction') }}">
                  Transaction
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('admission/merit-list') }}">
                  Merit List
                </a>
              </div>
              <div class="mdc-list-item mdc-drawer-item">
                <a class="mdc-drawer-link" href="{{ url('admission/documents') }}">
                 Admission Doc
               </a>
             </div>
             <div class="mdc-list-item mdc-drawer-item">
              <a class="mdc-drawer-link" href="{{ url('admission/confirm') }}">
               Confirm Admission
             </a>
           </div>
         </nav>
       </div>
     </div>
     <div class="mdc-list-item mdc-drawer-item">
      <a class="mdc-drawer-link" href="{{ url('gallery') }}">
        <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">insert_photo</i>
        Gallery
      </a>
    </div>
    <div class="mdc-list-item mdc-drawer-item">
      <a class="mdc-drawer-link" href="{{ url('settings') }}">
        <i class="material-icons mdc-list-item__start-detail mdc-drawer-item-icon" aria-hidden="true">settings_applications</i>
        Setting
      </a>
    </div>
  </nav>
</div>
</div>
</aside>