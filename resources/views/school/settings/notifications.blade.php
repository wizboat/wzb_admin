@extends('school.settings.app')

@section('content')
<style type="text/css">
   .card-header{
      background: #FFF;
   }
</style>
<section class="content">
   <div class="mdc-top-app-bar__title">Notifications</div>
   <div class="">                    
      <div class="edit-general-info">         		                    
         <div id="accordion">
            <div class="card mb-3">
               <div class="card-body" id="headingOne">
                  <h5 class="card-title mb-0">
                     <i class="fa fa-envelope"></i><span class="pl-3">Email<span>
                     <button class="btn btn-link pull-right p-0 m-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-pencil pl-2"></i>
                     </button>
                     </span>  
                  </h5>
               </div>
               <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
               </div>
            </div>
            <div class="card mb-3">
               <div class="card-body" id="headingTwo">
                  <h5 class="card-title mb-0">
                     <i class="fa fa-mobile"></i><span class="pl-3">Mobile Text Message</span>
                     <button class="btn btn-link collapsed pull-right p-0 m-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     <i class="fa fa-pencil pl-2"></i>
                     </button>
                  </h5>
               </div>
               <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                     <span>Text Notifications</span>  
                     <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                        On
                        </label>
                     </div>
                     <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                        Off
                        </label>
                     </div>
                     <br/>
                     <button type="button" class="btn btn-primary btn-sm">Save Changes</button>
                     <button class="btn btn-secondary btn-sm collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     Cancel
                     </button>                           
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-body" id="headingTwo">
                  <h5 class="card-title mb-0">
                     <i class="fa fa-tag"></i><span class="pl-3">Tags</span>
                     <button class="btn btn-link collapsed pull-right p-0 m-0" data-toggle="collapse" data-target="#collapsethree" aria-expanded="false" aria-controls="collapseTwo">
                     <i class="fa fa-pencil pl-2"></i>
                     </button>
                  </h5>
               </div>
               <div id="collapsethree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                     <p>These are notifications for when someone tags you in a comment or post.</p>
                     <h5 class="card-title"><strong>Where you receive these notifications</strong></h5>  
                     <ul class="activity_head">
                        <li class="notification_select_switch float-right">
                           <span>On</span>
                            <input id="switch001" name="switch003" type="checkbox">
                            <label for="switch001" class="label-default"></label>
                        </li>
                        <li class="info-box-text"><i class="fa fa-book" aria-hidden="true"></i></li>
                        <li class="desc_security border-bottom pb-3">
                          <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created a&nbsp;<span class="activity-content">course</span>&nbsp;Master of Computer Application</span></p>
                          <p class="mb-0"><small>September 13, 2018</small></p>
                        </li>
                     </ul>
                    <ul class="activity_head">
                        <li class="notification_select_switch float-right">
                           <span>On</span>
                            <input id="switch002" name="switch003" type="checkbox">
                            <label for="switch002" class="label-default"></label>
                        </li>
                        <li class="info-box-text"><i class="fa fa-book" aria-hidden="true"></i></li>
                        <li class="desc_security border-bottom pb-3">
                          <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created a&nbsp;<span class="activity-content">course</span>&nbsp;Master of Computer Application</span></p>
                          <p class="mb-0"><small>September 13, 2018</small></p>
                        </li>
                     </ul>
                     <ul class="activity_head">
                        <li class="notification_select_switch float-right">
                           <span>On</span>
                            <input id="switch003" name="switch003" type="checkbox">
                            <label for="switch003" class="label-default"></label>
                        </li>
                        <li class="info-box-text"><i class="fa fa-book" aria-hidden="true"></i></li>
                        <li class="desc_security border-bottom pb-3">
                          <p class="desc_activity">info@sinhgadeducation.com<span class="summary">created a&nbsp;<span class="activity-content">course</span>&nbsp;Master of Computer Application</span></p>
                          <p class="mb-0"><small>September 13, 2018</small></p>
                        </li>
                     </ul>
                     <button type="button" class="btn btn-primary btn-sm mr-2 m-0">Save Changes</button>
                     <button class="btn btn-secondary btn-sm m-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     Cancel
                     </button>                           
                  </div>
               </div>
            </div>
         </div>                                                          
      </div>
   </div>
</section> 
@endsection