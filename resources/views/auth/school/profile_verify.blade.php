<!DOCTYPE html>
<html>
<title>Wizboat Login</title>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500&amp;lang=en&amp;display=fallback" nonce="ACUEifZ_4E3itaNgpkugZA">
</head>
<body>
  <!--Navbar -->
  <nav class="mb-1 navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand" href="#"><img src="{{ url('assets/images/logo.png') }}" width="120" class="img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
      aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-user"></i> {{ Auth::user()->name }} </a>

          <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
            <a class="dropdown-item" href="#">Change Password</a>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
  </div>
</div>
</nav>
<!--/.Navbar -->

<!-- School Account login -->
<div class="padtp schoolsconfirmation">
  <div class="">
    <div class="container">

      <div class="row py-5 d-flex justify-content-center">
        <div class="col-md-2 pl-5 pl-md-0 pb-5">

          <!-- Stepper -->
          <div class="steps-form-3">
            <div class="steps-row-3 setup-panel-3 d-flex justify-content-between">
              <div class="steps-step-3">
                <a href="#step-5" type="button" class="btn btn-info btn-circle-3 waves-effect ml-0" data-toggle="tooltip" data-placement="top" title="Email Verification"><img src="{{ url('assets/images/email.png') }}"></a>
                <p class="weight-bold">Email Verification</p>
              </div>
                      <!-- <div class="steps-step-3">
                          <a href="#step-6" type="button" class="btn btn-info btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Basic Information"><img src="{{ url('assets/images/card-payment.png') }}"></a>
                        </div> -->
                        <div class="steps-step-3">
                          <a href="#step-7" type="button" class="btn btn-info btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="School Profile"><img src="{{ url('assets/images/university.png') }}"></a>
                          <p class="weight-bold">School Profile</p>
                        </div>
                        <div class="steps-step-3">
                          <a href="#step-7" type="button" class="btn btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="Profile Verification"><img src="{{ url('assets/images/review.png') }}"></a>
                          <p>Profile Verification</p>
                        </div>
                        <div class="steps-step-3 no-height">
                          <a href="#step-8" type="button" class="btn btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Course Selection"><img src="{{ url('assets/images/books.png') }}"></a>
                          <p>Course Selection</p>
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="col-md-9">

                    <div class="createschoolacc">
                      <h3 class="mainheading">Welcome, <span class="text-info">Sinhgad Institute of Technology</span></h3>
                      <div class="card fill_innersec">
                        <div class="card-body mt-4">
                          <form method="POST" action="{{ url('school-profile-verify/update') }}">
                            @csrf
                            <div class="md-form">                                         
                              <p style="color: #757575;font-weight: 300;font-size: 14px;margin-bottom: -8px;">School name <font color="red">*</font></p>
                              <p class="mbd-label">{{ Auth::user()->name }}</p>
                            </div>
                            <div class="md-form">                            
                              <input id="board" type="text" class="form-control" name="board" value="{{ old('board') }}">
                              @if ($errors->has('board'))
                              <span class="help-block">{{ $errors->first('board') }}</span>
                              @endif   
                              <label for="board">Board or University Name <font color="red">*</font></label>
                            </div>
                            <div class="md-form">                            
                              <textarea class="md-textarea form-control" id="about" name="about">{{ old('about') }}</textarea>          
                              @if ($errors->has('about'))
                              <span class="help-block">{{ $errors->first('about') }}</span>
                              @endif 
                              <label for="about">About <font color="red">*</font></label>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="address1" type="text" class="form-control" name="address1" value="{{ old('address1') }}">
                                  @if ($errors->has('address1'))
                                  <span class="help-block">{{ $errors->first('address1') }}</span>
                                  @endif   
                                  <label for="address1">Address Line 1 <font color="red">*</font></label>
                                </div>
                              </div>
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="address2" type="text" class="form-control" name="address2" value="{{ old('address2') }}">    
                                  <label for="address2">Address Line 2</label>
                                </div>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <div class="md-form">
                                  <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">
                                  @if ($errors->has('city'))
                                  <span class="help-block">{{ $errors->first('city') }}</span>
                                  @endif
                                  <label for="city">City <font color="red">*</font></label>
                                </div>
                              </div>
                              <div class="col">
                                <div class="md-form">
                                  <select class="mbd-select form-control" id="state" name="state">
                                    <option value="">------ Select State ------</option>
                                    @foreach($state as $value)
                                    <option value="{{ $value->id}}">{{ $value->state}}</option>    
                                    @endforeach    
                                  </select> 
                                  @if ($errors->has('state'))
                                  <span class="help-block">{{ $errors->first('state') }}</span>
                                  @endif 
                                  <!-- <label for="state">State <font color="red">*</font></label> -->
                                </div>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="pincode" type="text" class="form-control" name="pincode" value="{{ old('pincode') }}">
                                  @if ($errors->has('pincode'))
                                  <span class="help-block">{{ $errors->first('pincode') }}</span>
                                  @endif 
                                  <label for="pincode">Pin Code <font color="red">*</font></label>
                                </div>
                              </div>
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="phone1" type="text" class="form-control" name="phone1" value="{{ old('phone1') }}">
                                  @if ($errors->has('phone1'))
                                  <span class="help-block">{{ $errors->first('phone1') }}</span>
                                  @endif 
                                  <label for="phone1">Phone 1 <font color="red">*</font></label>
                                </div>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="phone2" type="text" class="form-control" name="phone2" value="{{ old('phone2') }}">  
                                  @if ($errors->has('phone2'))
                                  <span class="help-block">{{ $errors->first('phone2') }}</span>
                                  @endif
                                  <label for="phone2">Phone 2</label>
                                </div>
                              </div>
                              <div class="col">
                                <div class="md-form">
                                  <input id="phone3" type="text" class="form-control" name="phone3" value="{{ old('phone3') }}">     
                                  @if ($errors->has('phone3'))
                                  <span class="help-block">{{ $errors->first('phone3') }}</span>
                                  @endif
                                  <label for="phone3">Phone 3</label>
                                </div>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <div class="md-form">                                    
                                  <input id="school_email" type="text" class="form-control" name="school_email" value="{{ old('school_email') }}">
                                  @if ($errors->has('school_email'))
                                  <span class="help-block">{{ $errors->first('school_email') }}</span>
                                  @endif  
                                  <label for="school_email">School Email <font color="red">*</font></label>
                                </div>
                              </div>
                              <div class="col">
                                <div class="md-form">
                                 <input id="website"  type="text" class="form-control" name="website" value="{{ old('website') }}"> 
                                 <label for="website">Website</label>
                               </div>
                             </div>
                           </div>
                           <div class="form-row float-right">
                            <button class="btn btn-primary primaryModal my-4" type="submit">Confirm & Next</button>
                          </div>                     
                        </form>
                      </div>
                    </div>
                  </div>

                </div>

              </div>

            </div>
          </div>
        </div>
        <script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/mdb.min.js') }}"></script>

      </body>
      </html>