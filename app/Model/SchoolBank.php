<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolBank extends Model
{
    
    protected $table = 'school_bank';
    protected $hidden = [];
    protected $fillable = ['bank_id','school_id','bank_ifsc','bank_branch','account_number','created_by','bank_status'];   

     public function bank(){
        return $this->hasOne('App\Model\Bank','id','bank_id');
    }    
}
