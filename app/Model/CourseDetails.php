<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseDetails extends Model
{
    protected $table = 'course_details';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['course_link_id','seat_intake','fee', 'application_fee','admission_open_date', 'admission_close_date', 'admission_status']; 
    
	public function courseDetails(){
		
         return $this->belongsTo('App\Model\LinkCourseSchool','course_link_id','id');
    }
}