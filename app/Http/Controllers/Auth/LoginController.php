<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

      public function showLoginForm(){

        return view('auth.login');
    }

    public function login(Request $request){
        
        $post = $request->all();        
        
        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt($credentials)) {

            return redirect('/');
        }

        $errors = new MessageBag(['email' => ['Email and/or password invalid.']]);         

        return Redirect::back()->withErrors($errors)->withInput(Input::except('password'));        
    }
}
