<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $table = 'course';
	protected $hidden = [];
	protected $fillable = ['school_id', 'course_type_id', 'course_id','course_hash','course_status'];

	public function courseData(){
		return $this->belongsTo('App\Models\AdmissionOpen');
	}

	public function courseMaster() {
		return $this->belongsTo('App\Models\CourseMaster','course_master_id');
	}
}
