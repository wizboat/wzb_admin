@extends('layouts.app')

@section('title', '| Add Bank')

@section('content')
<div class="main-panel">
<section class="content"> 

    <div class="row">                                                      
        <div class="col-md-5 mx-auto">
            <div class="card mb-5 mt-5">
                <div class="card-body">                             
                    <!-- <div class="mainbar__title">Edit Bank</div> -->
                    <h5 class="card-title" style="font-size: 18px;"><i class="fa fa-university pr-2"></i><strong>Edit Bank</strong></h5>
                    <div class="mt-3">
                        {{ Form::open( array('route' => array('bank.update', $bankData->id), 'method' => 'PUT','files' => true, 'enctype' => 'multipart/form-data')) }}
                            <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                <label>Bank Name</label>
                                <select name="bank_name" class="mbd-select form-control pl-0">
                                    <option value="">--select--</option>
                                    @foreach($bank as $value)
                                    <option value="{{ $value->id }}">{{ $value->bank_name }}</option>
                                    @endforeach
                                </select>                               
                                @if ($errors->has('bank_name'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('bank_name') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form form-group{{ $errors->has('bank_ifsc') ? ' has-error' : '' }}">
                                {{ Form::label('bank_ifsc', 'IFSC Code'  , array('class' => 'active')) }}
                                {{ Form::text('bank_ifsc', "$bankData->bank_ifsc", array('class' => 'form-control')) }}
                                @if ($errors->has('bank_ifsc'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('bank_ifsc') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form form-group{{ $errors->has('bank_branch') ? ' has-error' : '' }}">
                                {{ Form::label('bank_branch', 'Bank Branch'  , array('class' => 'active')) }}
                                {{ Form::text('bank_branch', "$bankData->bank_branch", array('class' => 'form-control')) }}
                                @if ($errors->has('bank_branch'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('bank_branch') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                                {{ Form::label('account_number', 'Account Number' , array('class' => 'active')) }}
                                {{ Form::text('account_number', "$bankData->account_number", array('id' => 'account_number', 'class' => 'form-control')) }}
                                @if ($errors->has('account_number'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('account_number') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                                <select name="bank_status" class="mbd-select form-control"> 
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                            <!-- <button type="submit" name="updateseat" class="btn btn-primary primaryTable" value="Update">Update</button> -->
                            <div class="form-group mb-0">
                                {{ Form::submit('Update', array('class' => 'btn btn-primary primaryModal')) }}
                                <a href="{{ url('/bank')}}" class="btn closebtn">Back</a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script type="text/javascript">
    $(document).ready(function () {    
        $('[name="bank_name"]').val('{{$bankData->bank_id}}');
        $('[name="bank_status"]').val('{{$bankData->bank_status}}');
    });
</script>
@endsection