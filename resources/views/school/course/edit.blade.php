@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div id="msg"></div>
    <section class="content">
        <div class="mb-3">
            <span class="mdc-top-app-bar__title">Edit Class - I</span>
            <a href="{{ url('/course')}}" class="btn-link float-right"><i class="fa fa-arrow-left pr-2"></i>Back</a>
        </div>
        <div id="toast-container" class="md-toast-bottom-right" aria-live="polite" role="alert"  style="display: none;">
            <div class="md-toast md-toast-info">
                <div class="md-toast-message"></div>
            </div>
        </div>
        <div class="card mb-4">
          <div class="card-body">
            <div class="tab1"></div>
            <form name='tab1' id='tab1' action='#' method='post'>
                @csrf
                <div class="flex">
                    <div class="form-group equlwid pr-3">
                        <label for="seat_intake">Seat</label>               
                        <input type='text' name='seat_intake' class='form-control mbd-label' id='seat_intake' value="{{ isset($result->seat_intake) ? $result->seat_intake : '' }}">
                    </div>
                    <div class="form-group equlwid">
                        <label for="fee">Fee</label>                        
                        <input type='text' name='fee' class='form-control mbd-label' id='fee' value="{{ isset($result->fee) ? $result->fee : ''}}">
                    </div>
                    <div class="form-group equlwid">
                        <label for="fee">Application Fee</label>                        
                        <input type='text' name='application_fee' class='form-control mbd-label' id='application_fee' value="{{ isset($result->application_fee) ? $result->application_fee : ''}}">
                    </div>
                    <div class="flex align-items-center">
                        <button type="submit" name="updateseat" class="btn btn-primary primaryTable" value="Update">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>      

    <div class="card mb-3">
        <div class="card-body">
            <div class="select_course_switch course_edit">
                <span class="ml-0 mr-2">Course Status</span>
                <input type="checkbox" id="course_status" name="course_status" data-id="{{$linkId}}" {{ ($linkCourseSchool->course_status=='1') ? 'checked' : '' }}>
                <label class="label-default" for="course_status"></label>    
            </div>
        </div>
    </div>

    <div class="mbdtable">
        <div class="main-panel">
            <section class="content">
                <div id="message-success"></div>
                <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#addEligibilityModal"><i class="fa fa-plus pr-2"></i>Create Eligibility</button>
                <div class="mainbar__title sub_mainbar">Eligibility Criteria</div>
            </section>
        </div>
        <table id="eligibility_table" class="table cardmbd table-borderless"></table>
    </div>

    <div class="mbdtable">
        <div class="main-panel">
            <section class="content">
                <div id="message-success"></div>
                <button class="btn btn-primary primaryTable marTable create_syllabus" data-toggle="modal" data-target="#addSyllabusModal" data-id="{{$linkId}}"><i class="fa fa-plus pr-2"></i>Add More</button>
                <div class="mainbar__title sub_mainbar">Syllabus</div>
            </section>
        </div>
        <table id="syllabus_table" class="table cardmbd table-borderless"></table>
    </div>

</section>      
</div>

<!-- Modal Eligibility -->
<div class="modal fade" id="addEligibilityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="card-body modal-content">
        <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="mdc-top-app-bar__title" id="exampleModalLabel">Add Eligibility</div>
        </div>
        <form id="create_eligibility" name="create_eligibility" method="post">
            @csrf
            <input type="hidden" name="_h" value="{{ $linkId }}">
            <div class="">          
                <button class="btn btn-sm btn-info add_more_button m-0">
                    <i class="fa fa-plus pr-2"></i>Add More</button>
                    <div class="flex removeicon"><input type="text" name="eligibility[]" class="form-control removeicon mbd-label"/><a href="#" class="remove_field text-danger">&times;</a></div>
                    <div class="eligibility_field"></div>
                </div>

                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary primaryModal">Save changes</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Syllabus -->
<div class="modal fade" id="addSyllabusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="card-body modal-content">
        <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="mdc-top-app-bar__title" id="exampleModalLabel">Add Syllabus</div>
        </div>
        <form id="create_syllabus" name="create_syllabus" method="post">
            @csrf
            <input type="hidden" name="__hash" class="__create_syllabus">
            <div class="">
                <button class="btn btn-sm btn-info add_more_button1 m-0">
                    <i class="fa fa-plus pr-2"></i>Add More</button>
                    <div class="flex removeicon">
                        <input type="text" name="syllabus[]" class="form-control removeicon mbd-label"/>
                        <a href="#" class="remove_field text-danger">&times;</a>
                    </div>
                    <div class="syllabus_field"></div>
                </div>
                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary primaryModal">Save changes</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Eligibility-->
<div class="modal fade" id="deletepopup" tabindex="-1" role="dialog" aria-labelledby="deletepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <form id="deleted_eligibility" name="deleted_eligibility" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Delete</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Syllabus-->
<div class="modal fade" id="deletepopup_s" tabindex="-1" role="dialog" aria-labelledby="deletepopup_s" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <form id="deleted_syllabus" name="deleted_syllabus" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                    <input type="hidden" name="__sylbs" class="__sylbs">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Delete</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disabled Eligibility-->
<div class="modal fade" id="disablepopup" tabindex="-1" role="dialog" aria-labelledby="disablepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to disable these records? This process cannot be undone.</p>
            </div>
            <form id="disabled_eligibility" name="disabled_eligibility" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Save Change</button> 
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disabled Syllabus -->
<div class="modal fade" id="disablepopup_s" tabindex="-1" role="dialog" aria-labelledby="disablepopup_s" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to disable these records? This process cannot be undone.</p>
            </div>
            <form id="disabled_syllabus" name="disabled_syllabus" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Save Change</button> 
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

    var BS_COLUMNS  = [];
    function indexFormatter( value, row, index ){
        return index+1;
    }

    function statusFormatter( value, row, index ){
        return row.disabled == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
    }

    function disabledFormatter( value, row, index ){
        var disabledBtn = row.disabled == 1 ? "<button class='btn btn-outline-primary btn-md mr-3 m-0 weight-bold' onclick='disableEligibility("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Disable</button>" : "<button class='btn btn-outline-primary btn-md mr-3 m-0 weight-bold' onclick='disableEligibility("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Enable</button>";

        return "<button class='btn btn-outline-danger btn-md mr-3 m-0 weight-bold' onclick='deleteEligibility("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#deletepopup'>Delete</button>"+ disabledBtn;
    }

    BS_COLUMNS.unshift(
    {
        field       : 'id',
        title       : 'S.No',
        align       : 'center' ,
        formatter   : indexFormatter 
    },{
        field       : 'eligibility',
        title       : 'Eligibility',
        align       : 'center'
    },{
        field       : 'disabled',
        title       : 'Status',
        align       : 'center' ,
        formatter   : statusFormatter 
    },{
        field       : 'disabled',
        title       : 'Action',
        align       : 'center' ,
        formatter   : disabledFormatter
    }
    );

    var BS = {
        url             : '{{ url("course/eligibility/$linkId/list") }}',
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 5,
        showRefresh     : true
    };

    var t= $("#eligibility_table").bootstrapTable(BS);

    var BS_COLUMNS1  = [];

    function indexFormatter2( value, row, index ){

        return index+1;
    }

    function statusFormatter2( value, row, index ){

        return row.disabled == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
    }
    function disabledFormatter2( value, row, index ){    
        var disabledBtn = row.disabled == 1 ? "<button class='btn btn-outline-primary btn-md mr-3 m-0 weight-bold' onclick='disableFun("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup_s'>Disable</button>" : "<button class='btn btn-outline-primary btn-md mr-3 m-0 weight-bold' onclick='disableFun("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup_s'>Enable</button>";

        return "<button class='btn btn-outline-danger btn-md mr-3 m-0 weight-bold' onclick='deleteSyllabus("+row.lnk_id+","+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#deletepopup_s'>Delete</button>"+ disabledBtn;
    }

    BS_COLUMNS1.unshift(
    {
        field       : 'id',
        title       : 'S.No',
        align       : 'center' ,
        formatter   : indexFormatter2
    },{
        field       : 'syllabus_name',
        title       : 'Syllabus',
        align       : 'center'
    },{
        field       : 'disabled',
        title       : 'Status',
        align       : 'center' ,
        formatter   : statusFormatter2
    },{
        field       : 'disabled',
        title       : 'Action',
        align       : 'center' ,
        formatter   : disabledFormatter2 
    }
    );

    var BS1 = {
        url             : '{{ url("course/syllabus/$linkId/list") }}',
        columns         : BS_COLUMNS1,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 5,
        showRefresh     : true
    };

    var t1= $("#syllabus_table").bootstrapTable(BS1);

    $.noConflict();

    //*****Status*****//
    $('#course_status').change(function () {
        var id = $(this).attr('data-id');
        if($(this).prop('checked') == true){
            var formdata = {'data':1,'id':id};
        }else{
            var formdata = {'data':0,'id':id};
        }

        var successFun = function (data){
            var result = JSON.parse(data);  
            console.log(result);                         
            $("#loader").hide();
            $("#toast-container").show();
            $(".md-toast-message").html(result.msg); 
            setTimeout(function() {
               $("#toast-container").css('display','none').fadeOut('slow');
           }, 4000);           
        }

        var url = '{{ url("course/status") }}'; 
        AjaxCall("POST",url,formdata,true,successFun);
    });    


    //********************** Course Details***********************//
    var form = $("[name=tab1]");
    form.validate({
        rules:
        {
          seat_intake:
          {
            required: true,
            number : true,
            maxlength : 3
        },
        fee:
        {
            required: true,
            number : true,   
            maxlength : 6   
        },
        application_fee:
        {
            required: true,
            number : true,      
            maxlength :4    
        }
    },
    messages:
    {
      seat_intake:
      {
        required: "Please enter seat"
    },
    fee:
    {
        required: "Please enter fee"
    },
    application_fee:
    {
        required: "Please enter Application fee"
    }
},
submitHandler: function(form){
    formdata=$("#tab1").serialize();
    var successFun = function (data){                
        var result = JSON.parse(data);                            

        $("#loader").hide();
        $("#msg").html(result.message);               
    }
    var url = '{{ url("course/tab1/update/$linkId") }}'; 
    AjaxCall("POST",url,formdata,true,successFun);
}
});

    var form = $("[name=create_eligibility]");
    form.validate({
        ignore: [],
        rules: {
            'eligibility[]': {
              required: true
          }
      },
      submitHandler: function(form){
        formdata=$("#create_eligibility").serialize();

        var successFun = function (data){

            var result = JSON.parse(data);
            var url = '{{ url("course/eligibility/$linkId/list") }}';

            t.bootstrapTable('refresh', {
                url: url
            }); 

            $("#addEligibilityModal").modal("hide");
            $("#loader").hide();

            $("#msg").html(result.message);
        }

        var url = "{{ url('course/eligibility/eligibilitystore') }}";
        AjaxCall("POST",url,formdata,true,successFun);
    }
});

    var form = $("[name=deleted_eligibility]");
    form.validate({

      submitHandler: function(form){
        formdata=$("#deleted_eligibility").serialize();

        var successFun = function (data){

            var result = JSON.parse(data);
            var url = '{{ url("course/eligibility/$linkId/list") }}';

            t.bootstrapTable('refresh', {
                url: url
            }); 

            $("#deletepopup").modal("hide");
            $("#loader").hide();

            $("#msg").html(result.message);
        }

        var url = "{{ url('course/eligibility/deleted') }}";
        AjaxCall("POST",url,formdata,true,successFun);
    }
});

    var form = $("[name=disabled_eligibility]");
    form.validate({

      submitHandler: function(form){
        formdata=$("#disabled_eligibility").serialize();

        var successFun = function (data){

            var result = JSON.parse(data);
            var url = '{{ url("course/eligibility/$linkId/list") }}';

            t.bootstrapTable('refresh', {
                url: url
            }); 

            $("#disablepopup").modal("hide");
            $("#loader").hide();
            $("#msg").html(result.message);               
        }

        var url = "{{ url('course/eligibility/disabled') }}";
        AjaxCall("POST",url,formdata,true,successFun);
    }
});

   //****************************Syllabus*************************//
   var create_syllabus = $("[name=create_syllabus]");
   create_syllabus.validate({
      ignore: [],
      rules: {
        'syllabus[]': {
          required: true
      }
  },
  submitHandler: function(create_syllabus){
    formdata=$("#create_syllabus").serialize();

    var successFun = function (data){

        var result = JSON.parse(data);
        var url = '{{ url("course/syllabus/$linkId/list") }}';

        t1.bootstrapTable('refresh', {
            url: url
        }); 

        $("#addSyllabusModal").modal("hide");
        $("#loader").hide();
        $("#msg").html(result.message);               
    }

    var url = "{{ url('course/syllabus/store') }}";
    AjaxCall("POST",url,formdata,true,successFun);
}
});

   var form = $("[name=deleted_syllabus]");
   form.validate({

      submitHandler: function(form){
        formdata=$("#deleted_syllabus").serialize();

        var successFun = function (data){

            var result = JSON.parse(data);
            var url = '{{ url("course/syllabus/$linkId/list") }}';

            t1.bootstrapTable('refresh', {
                url: url
            }); 

            $("#deletepopup_s").modal("hide");
            $("#loader").hide();

            $("#msg").html(result.message);
        }

        var url = "{{ url('course/syllabus/deleted') }}";
        AjaxCall("POST",url,formdata,true,successFun);
    }
});

   var form = $("[name=disabled_syllabus]");
   form.validate({

      submitHandler: function(form){
        formdata=$("#disabled_syllabus").serialize();

        var successFun = function (data){

            var result = JSON.parse(data);
            var url = '{{ url("course/syllabus/$linkId/list") }}';

            t1.bootstrapTable('refresh', {
                url: url
            }); 

            $("#disablepopup_s").modal("hide");
            $("#loader").hide();
            $("#msg").html(result.message);               
        }

        var url = "{{ url('course/syllabus/disabled') }}";
        AjaxCall("POST",url,formdata,true,successFun);
    }
});

   $(".create_syllabus").click(function(){    
    $(".__create_syllabus").val($(this).attr("data-id"));        
});

   $(document).ready(function(){
    // Syllabus
    var max_fields_limit = 5;
    var x = 1; 
    $('.add_more_button1').click(function(e){ 
        e.preventDefault();
        if(x < max_fields_limit){
            x++; 
            $('.syllabus_field').append('<div class="flex removeicon"><input type="text" name="syllabus[]" class="form-control mbd-label"/><a href="#" class="remove_field text-danger">&times;</a></div>');
        }
    });  
    $('.syllabus_field').on("click",".remove_field", function(e){ 
        e.preventDefault(); $(this).closest('div').remove(); x--;
    });

    $('.removeicon').on("click",".remove_field", function(e){ 
        e.preventDefault(); $(this).closest('div').remove(); x--;
    });

    //Eligibility
    var max_fields_limit1 = 5;
    var x = 1; 
    $('.add_more_button').click(function(e){ 
        e.preventDefault();
        if(x < max_fields_limit1){
            x++; 
            $('.eligibility_field').append('<div class="flex removeicon"><input type="text" name="eligibility[]" class="form-control mbd-label"/><a href="#" class="remove_field_1 text-danger">&times;</a></div>');
        }
    });  
    $('.eligibility_field').on("click",".remove_field_1", function(e){ 
        e.preventDefault(); $(this).closest('div').remove(); x--;
    });

    $('.removeicon').on("click",".remove_field_1", function(e){ 
        e.preventDefault(); $(this).closest('div').remove(); x--;
    });        
});
</script>
@endsection