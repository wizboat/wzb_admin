@extends('layouts.app')

@section('content')
<div id="msg"></div>
<div class="mbdtable">
	<p>Admission Open Id : {{$courseMaster->admission_open_id}}</p>
	<p>Course Name :- {{$courseMaster->course_name}}</p>
	<div class="main-panel">
		<section class="content">
			<div id="message-success"></div>
			<div class="mainbar__title">Confirm Payment View</div>
		</section>
	</div>
	
	<table id="table" class="table cardmbd table-borderless"></table>
</div>
<script type="text/javascript">

	var BS_COLUMNS  = [];

	function indexFormatter( value, row, index ){
		return index+1;
	}

	function statusFormatter( value, row, index ){

		return row.admission_status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
	}

	function disabledFormatter( value, row, index ){

		var url = "{{ url('confirm_payment/:id/view') }}";
		url = url.replace(':id', row.admission_open_id);
		return "<a href="+url+" class='btn btn-outline-primary btnTable'>View</a>"

	}

	BS_COLUMNS.unshift(
	{
		field       : 'id',
		title       : 'Sr.No',
		align       : 'center' ,
		formatter   : indexFormatter 
	},
	{
		field       : 'txn_id',
		title       : 'TXN ID',
		align       : 'center'
	},
	{
		field       : 'addmission_refrence',
		title       : 'Admission Reference Id',
		align       : 'center'
	},
	{
		field       : 'firstname,lastname',
		title       : 'Name',
		align       : 'center'
	},
	{
		field       : 'Payment',
		title       : 'Payment',
		align       : 'center'
	},
	{
		field       : 'admission_status',
		title       : 'Payment Status',
		align       : 'center' ,
		formatter   : statusFormatter 
	}
	);

	var BS = {
		url             : "{{ url('payment/confirm_payment/$admissionID/view?i=0') }}",
		columns         : BS_COLUMNS,
		pagination      : true,
		search          : true,        
		sidePagination  : 'client',
		PageRefresh     : true,
		pageNumber      : 1,
		pageSize        : 10,
		showRefresh     : true
	};

	var t= $("#table").bootstrapTable(BS);

</script>

@endsection