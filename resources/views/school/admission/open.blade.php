@extends('layouts.app')
@section('content')
<section class="content">   
<div id="msg"></div>   
	<div class="mb-3">
		<span class="mdc-top-app-bar__title">Create admission for - {{ ucfirst($courseDetails->course_name) }}</span>
		<a href="{{ url('/admission/create')}}" class="btn-link float-right"><i class="fa fa-arrow-left pr-2"></i>Back</a>
	</div>
	<div class="card mb-4">
		<div class="card-body">
			<div class="tab1"></div>
			<form name='admission_open' id='admission_open' action='#' method='post'>
				@csrf
				<input type="hidden" name="e_s" value="{{ rand() }}">
				<input type="hidden" name="__h" value="{{ $linkId }}">
				<div class="flex">
					<div class="form-group equlwid pr-3">
						<label for="admission_start_date">Admission Start Date</label>				
						<input class="form-control mbd-label" name="admission_start_date" type="text" placeholder="Start Date" id="admission_start_date"/>
					</div>
					<div class="form-group equlwid">
						<label for="admission_close_date">Admission Close Date</label>						
						<input class="form-control mbd-label" name="admission_close_date" type="text" placeholder="Close Date" id="admission_close_date"/>
					</div>                 
				</div>
				<div class="flex">
					<div class="form-group equlwid pr-3">
						<label for="merit_calculation">Merit list calculation id required?(Merit list calculation in based upon percentage)</label>
						<input type="checkbox" name="merit_calculation" id="merit_calculation">
					</div>
					<div class="form-group equlwid">
						<label for="merit_date">Merit List Date</label>				
						<input class="form-control mbd-label" name="merit_date" type="text" placeholder="Merit List Date" id="merit_date"/>
					</div>
				</div>				
				<div class="">
					<label for="fee">Upload required document online or offline (report to school with original document & Fee related instructions)</label>
					<div class="flex">
						<div class="form-group equlwid pr-3">
							<label for="doc_start_date">Start Date</label>				
							<input class="form-control mbd-label" name="doc_start_date" type="text" placeholder="Start Date" id="doc_start_date"/>
						</div>
						<div class="form-group equlwid">
							<label for="doc_end_date">End Date</label>						
							<input class="form-control mbd-label" name="doc_end_date" type="text" placeholder="End Date" id="doc_end_date"/>
						</div>                 
					</div>
					<div class="form-group equlwid d-flex">
						<label for="admission_confirm_date">Confirm date</label>						
						<input class="form-control mbd-label" name="admission_confirm_date" type="text" placeholder="Session Date" id="admission_confirm_date" style="width: 15%;margin: 0 15px;">						
					</div> 
					<div class="form-group equlwid d-flex">
						<label for="session_start_date">School batch session start date</label>						
						<input class="form-control mbd-label" name="session_start_date" type="text" placeholder="Session Date" id="session_start_date" style="width: 15%;margin: 0 15px;">			
					</div>   					
				</div>
				<button type="submit" id="submitadmission" class="btn btn-primary primaryModal">Save Change</button>
			</form>
		</div>
	</div>	
</section>
<script type="text/javascript">
	$.noConflict();
	$("#admission_start_date").datepicker({
		todayBtn:  1,
		format: 'dd-mm-yyyy',
		startDate: new Date(),
		autoclose: true,
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		// $("#admission_close_date").val('');
		$('#admission_close_date').datepicker('setStartDate', minDate);
	});

	$("#admission_close_date").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
	
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		$("#admission_start_date").val('');
		$('#admission_start_date').datepicker('setEndDate', minDate);
		$('#merit_date').val('');
		$('#doc_start_date').val('');
		$('#doc_end_date').val('');
		$('#session_start_date').val('');
		$('#merit_date').datepicker('setStartDate', minDate);
	});

	// ----Merit List
	$("#merit_date").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
		
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		$('#doc_start_date').val('');
		$('#doc_end_date').val('');
		$('#session_start_date').val('');
		$('#admission_confirm_date').val('');
		$('#doc_start_date').datepicker('setStartDate', minDate);		
	});

	// ----Document Date
	$("#doc_start_date").datepicker({
		todayBtn:  1,
		format: 'dd-mm-yyyy',		
		autoclose: true,
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		$('#doc_end_date').datepicker('setStartDate', minDate);
		$('#session_start_date').datepicker('setStartDate', minDate);
		$('#doc_end_date').val('');
		$('#admission_confirm_date').val('');
		$('#session_start_date').val('');
	});

	$("#doc_end_date").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,		
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		$('#admission_confirm_date').datepicker('setStartDate', minDate);
		$('#admission_confirm_date').val('');
		$('#session_start_date').val('');
	});

	$("#admission_confirm_date").datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true,
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
		$('#session_start_date').datepicker('setStartDate', minDate);
		$('#session_start_date').val('');
	});

	// ---School List---
	$("#session_start_date").datepicker({
		todayBtn:  1,
		format: 'dd-mm-yyyy',		
		autoclose: true,
	}).on('changeDate', function (selected) {
		var minDate = new Date(selected.date.valueOf());
	});

var form = $("[name=admission_open]");
form.validate({
	rules: {
		admission_start_date: {
			required: true
		},admission_close_date: {
			required: true
		},merit_date: {
			required: true
		},session_start_date: {
			required: true
		},            
	},
	messages: {
		admission_start_date: {
			required: "Please select start date",
		},admission_close_date: {
			required: "Please select close date",
		},merit_date: {
			required: "Please select close date",
		},session_start_date: {
			required: "Please select close date",
		}
	},
	submitHandler: function(form){
		formdata=$("#admission_open").serialize();
		var successFun = function (data){
			var result = JSON.parse(data);                
			if(result.status==false){
				$("#msg").html(result.message);
				return false;
			}
			window.location = 'admission/create';
			
			// $("#openAdmission").modal("hide");
			// $("#loader").hide();
			// $("#msg").html(result.message);
		}
		var url = "/admission/open/update";
		AjaxCall("POST",url,formdata,true,successFun);
	}
});
</script>
@endsection