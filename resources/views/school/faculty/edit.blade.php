@extends('layouts.app')

@section('title', '| Add User')

@section('content')
<section class="content"> 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
           <div class="card mb-5 mt-5">
                <div class="card-header p-3">                                   
                    <span class="box-title"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Add Faculty</span>                             
                </div>                
                <div class="card-body p-0 ">  
                    <div class="col-lg-12 mt-3 mb-3">                      
                        {{ Form::open( array('route' => array('faculty.update', $faculty->id), 'method' => 'PUT','files' => true, 'enctype' => 'multipart/form-data')) }}
                            <div class="form-group{{ $errors->has('faculty_name') ? ' has-error' : '' }}">    
                                <label>Faculty Name:</label>                           
                                <input type="text" name="faculty_name" value="{{ $faculty->faculty_name}}" class="form-control">
                                @if ($errors->has('faculty_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('faculty_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <center><img src="{{ url('assets/images/faculty/'.$faculty->faculty_image)}}" width="200" height="200"></center>
                            </div>
                            <div class="custom-file{{ $errors->has('faculty_image') ? ' has-error' : '' }}">
                                {{ Form::label('faculty_image', 'Choose image',array('class'=>'custom-file-label')) }}
                                {{ Form::file('faculty_image',array('class' => 'custom-file-input')) }}
                                @if ($errors->has('faculty_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('faculty_image') }}</strong>
                                    </span>
                                @endif
                            </div>                       
                            <div class="form-group mt-2{{ $errors->has('faculty_degree') ? ' has-error' : '' }}">
                                <label>Faculty Degree:</label>
                                <input type="text" name="faculty_degree" value="{{ $faculty->faculty_degree}}" class="form-control">
                                @if ($errors->has('faculty_degree'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('faculty_degree') }}</strong>
                                    </span>
                                @endif
                            </div>                           
                             <div class="form-group pb-2">
                                {{ Form::submit('Update', array('class' => 'btn btn-primary btn-sm')) }}
                                <a href="{{ url('/faculty')}}" class="btn btn-secondary btn-sm">Back</a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection