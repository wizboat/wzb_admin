@extends('layouts.app')

@section('title', '| Users')

@section('content')
<div class="main-panel"><!-- 
   <section class="content">
      @if(Session::get('success_message'))
         <div class="alert alert-success" role="alert"><strong>Success!</strong>&nbsp;&nbsp;{{Session::get('success_message')}}</div>
      @endif 
      @if(Session::get('error_message'))
         <div class="alert alert-danger" role="alert">{{Session::get('success_message')}}</div>
         @endif  -->
         <!-- <div class="mdc-top-app-bar__title">Banks Details</div> -->
         <!-- <a href="{{ url('/payments/ad') }}" class="btn-link float-right"><i class="fa fa-arrow-left pr-2"></i>Back to payments</a>                      -->
         
       </section>
       <section class="content">   
         <div id="msg"></div>
         <div class="mbdtable">
          <div class="main-panel">
            <section class="content">
              <div id="message-success"></div>
              <!-- <span class="pull-right pb-3"><a href="{{ url('bank/create')}}" class="btn btn-primary btn-sm m-0"><i class="fa fa-plus pr-2"></i> Add Bank</a></span> -->
              <a href="{{ url('bank/create')}}">
               <button class="btn btn-primary primaryTable marTable"><i class="fa fa-plus pr-2"></i>Add Bank</button>
             </a>
             <div class="mainbar__title">Banks Details </div>
           </section>
         </div>
         <table id="bank_table" class="table cardmbd table-borderless"></table>
       </div>
     </section>

   </div>


   <script type="text/javascript">

    var BS_COLUMNS  = [];

    function indexFormatter( value, row, index ){

      return index+1;
    }

    function statusFormatter( value, row, index ){

      return row.bank_status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
    }

    function editFormatter( value, row, index ){
     
     var url = "{{ url('bank/:id/edit') }}";
     url = url.replace(':id', row.id);

     return "<a href="+url+" class='btn btn-outline-primary btnTable'>Edit</a>";
   }

   BS_COLUMNS.unshift(
   {
    field       : 'id',
    title       : 'Sr.No',
    align       : 'center' ,
    formatter   : indexFormatter 
  },{
    field       : 'account_number',
    title       : 'Account Number',
    align       : 'center'
  },
  {
    field       : 'bank_name',
    title       : 'Bank Name',
    align       : 'center'
  },
  {
    field       : 'bank_ifsc',
    title       : 'IFSC Code',
    align       : 'center'
  },
  {
    field       : 'bank_branch',
    title       : 'Branch',
    align       : 'center'
  },
  {
    field       : 'bank_status',
    title       : 'Status',
    align       : 'center' ,
    formatter   : statusFormatter 
  },
  {
    field       : '',
    title       : 'Action',
    align       : 'center' ,
    formatter   : editFormatter 
  }
  );

   var BS = {
    url             : "{{ url('bank/list') }}",
    columns         : BS_COLUMNS,
    pagination      : true,
    search          : true,        
    sidePagination  : 'client',
    PageRefresh     : true,
    pageNumber      : 1,
    pageSize        : 10,
    showRefresh     : true
  };

  var t = $("#bank_table").bootstrapTable(BS);

  
</script>
@endsection