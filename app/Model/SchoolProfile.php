<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolProfile extends Model
{
    protected $table = 'school_profile';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = [ 'user_id', 'school_name', 'school_hash','school_logo','year_founded','school_banner','school_email','board','address1','address2','address3','state','city','pincode','phone1','phone2','phone3','website'];

    public function user(){
         return $this->belongsTo('App\Model\User','user_id');
    }

    public function stateName(){
         return $this->belongsTo('App\Model\State','state');
    }
}
