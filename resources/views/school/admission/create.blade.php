@extends('layouts.app')
@section('content')

<section class="content">   
   <div id="msg"></div>
  <div class="mbdtable">
      <div class="main-panel">
          <section class="content">
              <div id="message-success"></div>
                  <!-- <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus pr-2"></i>Add Faculty</button> -->
              <div class="mainbar__title">Create Admission </div>
          </section>
      </div>
      <table id="admission_create" class="table cardmbd table-borderless"></table>
  </div>
</section>


<!-- Admission Open -->
<div class="modal fade" id="openAdmission" tabindex="-1" role="dialog" aria-labelledby="openAdmission" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <h5 class="mdc-top-app-bar__title" id="exampleModalLabel">Open Addmission</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <form id="admission_open" name="admission_open" method="post">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="__h" class="__hash">
                    <div class="row">
                    <div class="col-sm-6 px-0">
                        <div class="form-group my-0">
                            <input class="form-control mbd-label" name="start_date" type="text" placeholder="Start Date" id="startdate"/>
                            <div class="admissionDate"><i class="fa fa-calendar iconDate"></i></div>
                        </div>
                    </div>
                    <div class="col-sm-6 pr-0">
                        <div class="form-group my-0">
                            <input class="form-control mbd-label" name="close_date" type="text" placeholder="Close Date" id="enddate"/>
                            <div class="admissionDate"><i class="fa fa-calendar iconDate"></i></div>
                        </div>
                    </div>
                </div> 
                </div>
                <div class="text-right mt-3"> 
                    <button type="submit" id="submitadmission" class="btn btn-primary primaryModal">Save Change</button> 
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Admission -->
<div class="modal fade" id="editAdmission" tabindex="-1" role="dialog" aria-labelledby="editAdmission" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <h5 class="mdc-top-app-bar__title" id="exampleModalLabel">Edit Addmission</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <form id="admission_edit" name="admission_edit" method="post">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="__h" class="__h">
                    <div class="row">
                    <div class="col-sm-6 px-0">
                        <div class="form-group my-0">
                            <input class="form-control mbd-label" name="start_date" type="text" placeholder="Start Date" id="start_date"/>
                            <div class="admissionDate"><i class="fa fa-calendar iconDate"></i></div>
                        </div>
                    </div>
                    <div class="col-sm-6 pr-0">
                        <div class="form-group my-0">
                            <input class="form-control mbd-label" name="close_date" type="text" placeholder="Close Date" id="close_date"/>
                            <div class="admissionDate"><i class="fa fa-calendar iconDate"></i></div>
                        </div>
                    </div>
                </div> 
                </div>
                <div class="text-right mt-3"> 
                    <button type="submit" id="submit_admission" class="btn btn-primary primaryModal">Save Change</button> 
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

      var BS_COLUMNS  = [];

      function indexFormatter( value, row, index ){
        return index+1;
      }

      function courseFormatter( value, row, index ){
        return row.course_status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
      }

      function admissionFormatter( value, row, index ){  
         if(row.admission_open_date != '' && row.admission_close_date != ''){
            if(row.admission_status == 1 ){            
               return '<span class="badge badge-success">Active</span>';
            }else if(row.admission_status == 2){
                  return '<span class="badge badge-warning">In-Active</span><br><div style="font-size: xx-small;"><b>Open on '+row.admission_open_date+'</b></div>';
            }else{
                  return '<span class="badge badge-danger">Closed</span>';
            } 
         }else{
            return '-';
         }
      }

     function openFormatter( value, row, index ){      
         if(row.admission_open_date != '' && row.admission_close_date != ''){
            return row.admission_open_date;
         }else{
            return '-';
         }
      }

      function closeFormatter( value, row, index ){      
         if(row.admission_open_date != '' && row.admission_close_date != ''){
            return row.admission_close_date;           
         }else{
            return '-';
         }
      }

      function buttonFormatter( value, row, index ){                   
         var url = "{{ url('admission/create/:id/detail') }}";
         url = url.replace(':id', row.linkid);

         var url1 = "{{ url('admission/create/:linkid') }}";
         url1 = url1.replace(':linkid', row.linkid);
         
         if(row.course_status==1){
            return "<a href="+url1+" class='btn btn-link btnTable'  style='text-decoration:underline;color: #2748ec; font-weight: 400;'>Open Admission</a><br/> <a href="+url+" class='btn btn-link btnTable'  style='text-decoration:underline;color: #2748ec; font-weight: 400;'>Previous Details</a>";
         }else{
            return "<a href="+url+" class='btn btn-link btnTable' style='text-decoration:underline;color: #2748ec; font-weight: 400;'>Previous Details</a>";            
         }
      }

    BS_COLUMNS.unshift(
        {
            field       : 'id',
            title       : 'Sr.No',
            align       : 'center' ,
            formatter   : indexFormatter 
        },{
            field       : 'course_name',
            title       : 'Course Name',
            align       : 'center'
        },
        {
            field       : 'course_status',
            title       : 'Course Status',
            align       : 'center',
            formatter   : courseFormatter
        },
        {
            field       : 'admission_open_date',
            title       : 'Admission Open Date',
            align       : 'center',
            formatter   : openFormatter
        },
        {
            field       : 'admission_close_date',
            title       : 'Admission Close Date',
            align       : 'center',
            formatter   : closeFormatter
        },
        {
            field       : 'admission_status',
            title       : 'Admission Status',
            align       : 'center',
            formatter   : admissionFormatter
        },
        {
            field       : '',
            title       : 'Action',
            align       : 'center' ,
            formatter   : buttonFormatter 
        }
    );

    var BS = {
        url             : "{{ url('admission/open/list') }}",
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 10,
        showRefresh     : true
    };

    var t = $("#admission_create").bootstrapTable(BS);

   $.noConflict();
  
    var form = $("[name=admission_open]");
    form.validate({
         rules: {
            start_date: {
              required: true
            },
            close_date: {
              required: true
            }
          },
          // Specify validation error messages
          messages: {
            start_date: {
              required: "Please select start date",
            },
            close_date: {
              required: "Please select close date",
            }
            
          },
         submitHandler: function(form){
            formdata=$("#admission_open").serialize();

            var successFun = function (data){
                
                var result = JSON.parse(data);                
                if(result.status==false){
                  $("#msg").html(result.message);
                }

                var url = "{{ url('admission/open/list') }}";

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#openAdmission").modal("hide");
                $("#loader").hide();

                $("#msg").html(result.message);
            }

            var url = "{{ url('admission/open/update') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
    });

    $("#startdate").datepicker({
      todayBtn:  1,
      format: 'dd-mm-yyyy',
      startDate:new Date(),
      autoclose: true,
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#enddate').datepicker('setStartDate', minDate);
      });
    
    $("#enddate").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#startdate').datepicker('setEndDate', minDate);
      });


      // Edit Adnission
      var form = $("[name=admission_edit]");
          form.validate({
               rules: {
                  start_date: {
                    required: true
                  },
                  close_date: {
                    required: true
                  }
                },
                // Specify validation error messages
                messages: {
                  start_date: {
                    required: "Please select start date",
                  },
                  close_date: {
                    required: "Please select close date",
                  }
                  
                },
               submitHandler: function(form){
                  formdata=$("#admission_edit").serialize();

                  var successFun = function (data){
                      
                      var result = JSON.parse(data);                
                      if(result.status==false){
                        $("#msg").html(result.message);
                      }

                      var url = "{{ url('admission/open/list') }}";

                      t.bootstrapTable('refresh', {
                          url: url
                      }); 

                      $("#editAdmission").modal("hide");
                      $("#loader").hide();

                      $("#msg").html(result.message);
                  }

                  var url = "{{ url('admission/edit/update') }}";
                  AjaxCall("POST",url,formdata,true,successFun);
              }
          });

            function editAdmission(id){

               var formdata = {'__h':id};

               var successFun = function (data){
                         
                 var result = JSON.parse(data);
                 console.log(result);
                 if(result.status==true){
                     $("#start_date").val(result.data.admission_open_date);
                     $(".__h").val(result.data.id);
                     $("#close_date").val(result.data.admission_close_date);  
                     $("#loader").hide();          
                     $("#editAdmission").modal("show");
                 }
                 
               }
               var errorFun = function(data){}

               var url = '{{ url("admission/edit") }}';
               AjaxCall("GET",url,formdata,true,successFun,errorFun);

            }

      
      $("#start_date").datepicker({
         todayBtn:  1,
         format: 'dd-mm-yyyy',
         startDate:new Date(),
         autoclose: true,
         }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#close_date').datepicker('setStartDate', minDate);
      });
    
      $("#close_date").datepicker({
         format: 'dd-mm-yyyy',
         autoclose: true,
         }).on('changeDate', function (selected) {
               var minDate = new Date(selected.date.valueOf());
               $('#start_date').datepicker('setEndDate', minDate);
      });

</script>
@endsection
