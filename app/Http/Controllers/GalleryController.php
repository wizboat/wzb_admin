<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use Hash;
use DB;
use Illuminate\Http\Request;
use App\Model\SchoolGallery;


class GalleryController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
    	$schoolGallery = SchoolGallery::get();
    	return view('school.gallery.gallery',compact('schoolGallery'));
    }

     public function upload(Request $request){
    	
    	$post = $request->all();

        // $validator = \Validator::make($post, ['upload_file'=>'mimes:jpeg,jpg,png|required']);
        // if ($validator->fails())
        // {
              // Redirect or return json to frontend with a helpful message to inform the user 
              // that the provided file was not an adequate type
             // return redirect()->back()->withErrors($validator);  
        // } 

    	$filename = '';

    	for($i=0;$i<count($_FILES["upload_file"]["name"]);$i++){

			$uploadfile=$_FILES["upload_file"]["tmp_name"][$i];
			$folder=public_path()."/assets/images/school_gallery/";
			move_uploaded_file($_FILES["upload_file"]["tmp_name"][$i], "$folder".$_FILES["upload_file"]["name"][$i]);

			$result = new SchoolGallery();        
			$result->school_id  = Auth::user()->id;        
			$result->image_path  = "/assets/images/school_gallery/".$_FILES["upload_file"]["name"][$i];        
			$result->save();
		}

        Session::flash('success_message', 'Gallery images successfully uploaded');
        return redirect('gallery');
    }

    public function deleteGallery(Request $request){

        $post = $request->all();    
      
        $validator = \Validator::make($post, ['__hash'=>'required']);

        if ($validator->fails()) {
            
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);     
        }

        $result = SchoolGallery::where('id', $post['__hash'])->where('school_id',Auth::user()->id)->first();

        if($result){
            $result->deleted  = '0';
            $result->save();
        }
    
        if(isset($result->id)){
            $message = '<div class="alert alert-success" role="alert"> Deleted Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);       
    }

    public function disableGallery(Request $request){

        $post = $request->all();

        $result = SchoolGallery::where('id', $post['__hash'])->where('school_id',Auth::user()->id)->first();

        if(isset($result)){
            if($result->disabled == '1'){

                $result->disabled  = '0';
            }else{
                $result->disabled  = '1';
            }

            $result->save();    
        }else{
            return response()->json(['status' => false, 'message' => '<div class="alert alert-success" role="alert"> Something went wrong</div>']);
        }
        
        if($result->id){
            $message = '<div class="alert alert-success" role="alert"> Disabled Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);
    }


    public function show(){

        $schoolGallery = SchoolGallery::where('school_id', Auth::user()->id)->where('deleted','1')->get()->toArray();          
        return response()->json($schoolGallery);
    }
}