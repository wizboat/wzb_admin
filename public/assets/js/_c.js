function AjaxCall(method,url,data,async,successFun) {

    var successCallback = function (data) {
        successFun(data);
    }

    var xhr = $.ajax({
        method  : method,
        url     : url,
        data    : data,
        async   : async,
        dataType: "text",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success : successCallback,
        beforeSend: function() {
            $("#loader").show();
        },
    });
   
    return xhr;
}

function setId(hash){
    $(".__hash").val(hash);
}


function deleteFun(hash){
    $(".__hash").val(hash);
}

function deleteGallary(hash){
    $(".__hash").val(hash);
}

function deleteEligibility(hash){
    $(".__hash").val(hash);
}

function disableFun(hash){
    $(".__hash").val(hash);
}

function disableGallary(hash){
    $(".__hash").val(hash);
}

function disableEligibility(hash){
    $(".__hash").val(hash);
}

function deleteSyllabus(hash,syllabusId){
    $(".__hash").val(hash);
    $(".__sylbs").val(syllabusId);
}