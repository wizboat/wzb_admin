<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

</body>
</html>
<table border="0" cellpadding="0" cellspacing="0" style="width: 800px; margin: 25px auto;padding: 25px; padding-bottom: 63px;">
	<tr style="width: 70%; display: block; margin: auto;">
		<td style="display: block; background-color: transparent;padding-bottom: 15px; box-shadow: 0 0 10px gainsboro;">
			<img src="http://test.wizboat.com/images/logo1.png" style="width: 140px; display: block; margin: 0 auto;" alt="Logo">
			<p style="text-align: center; margin: 15px; margin-bottom: 0; font-size: 18px;font-weight: 500;">
				{{ $data['name'] }}
			<p>
		</td>
	</tr>
	<tr style="width: 70%; display: block; margin: auto;">
		<td style="display: block; padding: 25px 50px; background-color: #fff; border-radius: 6px; border: 1px solid #c3c3c3;">
			<img src="https://wizboat.com/public/assets/images/email.png" width="160px" style="width: 120px;margin: 15px auto; display: block;" alt="Message">
			<h5 style="font-size: 20px; text-align: center; margin: 0;">Here is your One Time Password</h5>
			<p style="text-align: center; font-size: 17px;">to validate your email address</p>
			<h4 style="font-size: 48px; margin: 0; text-align: center; letter-spacing: 5px;">{{ $data['otp'] }}</h4>
			<p style="font-size: 16px; text-align: center; color: #ff3434;">valid for 10 minutes only</p>
		</td>
	</tr>
	<tr style="width: 70%; display: block; margin: auto;text-align: center;">
		<td style="display: block;">
			<ul style="padding: 10px 0px; list-style: none">
				<li style="display: inline-block; padding-right: 10px; margin: 0">FAQs</li> |
				<li style="display: inline-block; padding: 0 10px; margin: 0">Terms & Conditions</li> |
				<li style="display: inline-block; padding: 0 10px; margin: 0">Contact Us</li>
			</ul>
		</td>
	</tr>
</table>


