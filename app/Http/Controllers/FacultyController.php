<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use App\Model\Faculty;
use Session;
use Auth;

class FacultyController extends Controller
{
    public function __construct(){
      	$this->middleware('auth');
    }

    public function index(){
    	$faculty = Faculty::select('id')->get();
   	    return view('school.faculty.index',compact('faculty'));	
    }

    public function show(){
        $faculty = Faculty::where('school_id',Auth::user()->id)->where('deleted','1')->get()->toArray();
        return response()->json($faculty);
    }

   	public function create(){
   		return view('school.faculty.create');	
   	}

   	public function store(Request $request){

   		$post = $request->all();	        

   		$validator = \Validator::make($post, ['faculty_name'=>'required','faculty_degree'=>'required']);

   		if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);        
        }
        
   		$filename = '';

   		if(!empty($request->file('faculty_image'))){

            $image_url = $request->file('faculty_image');
            $destinationPath =  public_path().'/assets/images/faculty/';
            $filename = $image_url->getClientOriginalName();
            $filename = rand().date('His').'.'.'png';
            $image_url->move($destinationPath, $filename);
   		}
   		
        $faculty = new Faculty();
        $faculty->school_id 	 = Auth::user()->id;        
        $faculty->faculty_name 	 = $post['faculty_name'];
        $faculty->faculty_image  = !empty($filename) ? '/assets/images/faculty/'.$filename : 'default.jpg';
        $faculty->faculty_degree = $post['faculty_degree'];
        $faculty->save();

        Session::flash('success_message', 'Faculty has been created successfully');
        return redirect('faculty');
   }
   
    public function edit(Request $request,$id){
   	    $faculty = Faculty::where('school_id',Session::get('school_id'))->where('id',$id)->first();
        if(isset($faculty)){
            return view('school.faculty.edit',compact('faculty')); 
        }else{
            return redirect('faculty');
        }		
   }

	public function update(Request $request,$id){
   	$post = $request->all();	
   	$validator = \Validator::make($post, ['_token'=>'required','faculty_name'=>'required','faculty_degree'=>'required']);
   	if ($validator->fails()) {
   		return redirect()->back()->withErrors($validator);        
   	}
   	$filename = '';
   	if(!empty($request->file('faculty_image'))){
   		$image_url = $request->file('faculty_image');
   		$destinationPath =  public_path().'/assets/images/faculty/';
   		$filename = $image_url->getClientOriginalName();
   		$filename = rand().date('His').'.'.'png';
   		$image_url->move($destinationPath, $filename);
   	}
   	
       $faculty = Faculty::where('school_id',Session::get('school_id'))->where('id',$id)->first();
       $faculty->school_id 	 = Session::get('school_id');
       $faculty->created_by 	 = Auth::user()->id;
       $faculty->faculty_name 	 = $post['faculty_name'];
       if(!empty($filename)){
       	$faculty->faculty_image  = $filename;	
       }
       $faculty->faculty_degree = $post['faculty_degree'];
       $faculty->save();

    	Session::flash('success_message', 'Faculty has been updated successfully');
    	return redirect('faculty');
   }

   public function deleteGallery(Request $request){

        $post = $request->all();    
      
        $validator = \Validator::make($post, ['__hash'=>'required']);

        if ($validator->fails()) {
            
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);     
        }

        $result = Faculty::where('id', $post['__hash'])->where('school_id',Auth::user()->id)->first();

        if($result){
            $result->deleted  = '0';
            $result->save();
        }
    
        if(isset($result->id)){
            $message = '<div class="alert alert-success" role="alert"> Deleted Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);       
    }

    public function disableGallery(Request $request){

        $post = $request->all();

        $result = Faculty::where('id', $post['__hash'])->where('school_id',Auth::user()->id)->first();

        if($result){
            if($result->disabled == '1'){

                $result->disabled  = '0';
            }else{
                $result->disabled  = '1';
            }
            $result->save();
        }else{
            return response()->json(['status'=>false, 'message'=>'<div class="alert alert-danger" role="alert"> <strong>Warning !</strong> Something went wrong</div>']);
        }
        
        if($result->id){
            $message = '<div class="alert alert-success" role="alert"> <strong>Success !</strong> Disabled Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-danger" role="alert"><strong>Warning !</strong> Something went wrong</div>';
            $status = false;
        }

        return response()->json(['status'=>$status,'message'=>$message]);
    }
}
