<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
   	protected $table = 'bank';
    protected $hidden = [];
    protected $fillable = ['bank_name'];
}
