@extends('layouts.app')

@section('content')

<style type="text/css">
    .admisssiion_inner {
        padding-top: 10px;
        padding-left: 24px;
        text-align: center;
    }
    .admisssiion_inner p {
        margin-bottom: 0;
        line-height: 24px;
        font-size: 16px;
    }
    .merit_main_title {
        text-align: center;
        font-weight: 600;
        font-size: 20px;
    }
    .admisssion_detail {
        margin: 50px auto;
        padding-right: 0;
        color: #000;
    }
    .admisssion_detail_inner {
        background-color: #fff;
        border-radius: 10px;
        border: 1px solid #ddd;
        padding: 45px 50px 45px;
        box-shadow: 0 10px 25px #ddd;
    }
    .table {
        color: #000;
    }
    span.left_imgs {
        text-align: center;
        display: block;
        width: 100%;
    }
    .list-img {
        width: 120px;
        height: 120px;
    }
    table.table th, table.table td {
        padding: 0.75rem;
        font-size: 15px;
    }
    table.table td {
        font-weight: 400;
    }
</style>
<section class="padtp">
    <div class="container">
        <div class="admisssion_detail">
            <div class="admisssion_detail_inner">
                <div class="admisnstd_blobk mb-4">
                    <span class="left_imgs">
                        <img src="http://localhost:8003/assets/images/logo-school.png" class="list-img">
                    </span>                               
                    <div class="admisssiion_inner">                      
                        <h3 class="school-name mb-1">Ryan International School</h3>
                        <p>Near Yazoo park, Carter Road, Dahisar, 21-401303. Ph. 85522222</p>
                        <p>e-mail : sksuthar09@gmail.com</p>
                        <p>url : www.dssss.com</p>
                    </div>
                </div>
                <div class="admission_inner_card">                      
                    <h5 class="merit_main_title">Merit List :</h5> 
                    <div class="left_merit_list my-3">
                        <span class="pr-3"><strong>Course Name :</strong> Class - I</span> 
                        <span><strong>Open Seat :</strong> 30</span>
                        <span class="float-right">
                            <button class="btn btn-primary primaryTable mr-2 d-inline">Download</button>
                            <button class="btn btn-primary primaryTable d-inline" onclick="printJS('printJS-form', 'html')">Print</button>
                        </span>
                    </div>   
                    <table class="table table-bordered table-hover text-center" width="100%">
                        <thead>
                            <tr>
                                <th width="80">Sr. No.</th>
                                <th>Admission ID</th>
                                <th>Application Name</th>
                                <th>Percentage</th>
                                <th>Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>256563656</td>
                                <td>Rahim</td>
                                <td>84.16%</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>65986598</td>
                                <td>Shyam</td>
                                <td>91.56%</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>986598989</td>
                                <td>Krishna</td>
                                <td>81.91%</td>
                                <td>2</td>
                            </tr>
                        </tbody>
                    </table>                       
                </div>
                <div class="text-right">
                    <button class="btn btn-primary primaryTable mr-2 d-inline">Download</button>
                    <button class="btn btn-primary primaryTable d-inline" onclick="printJS('printJS-form', 'html')">Print</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection