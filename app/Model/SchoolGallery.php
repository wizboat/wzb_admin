<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolGallery extends Model        
{
    protected $table = 'school_gallery'; 
    protected $hidden = [];
    public $timestamps = true; 
    protected $fillable = [ 'school_id', 'image_path','disabled','deleted'];
}
