@extends('layouts.app')
@section('content')

<section class="content">   
    <div id="msg"></div>                
    <div class="mbdtable">
      <div class="main-panel">
         <a class="btn btn-primary primaryTable marTable" href="{{ url('admission/create') }}">Back</a>
         <section class="content">
            <div id="message-success"></div>
            <div class="mainbar__title">Detail Admission for - {{ ucfirst($courseDetails->course_name) }} </div>
         </section>
      </div>
      <table id="admission_detail" class="table cardmbd table-borderless"></table>
  </div>
</section>


<script>
   var BS_COLUMNS  = [];
   function indexFormatter( value, row, index ){
      return index+1;
   }

   function admissionStatusFormatter( value, row, index ){     
      if(row.admission_open_date != '' && row.admission_close_date != ''){
         if(row.admission_status == 1 ){            
            return '<span class="badge badge-success">Active</span>';
         }else if(row.admission_status == 2){
            return '<span class="badge badge-warning">In-Active</span><br><div style="font-size: xx-small;"><b>Open on '+row.admission_open_date+'</b></div>';
         }else{
            return '<span class="badge badge-danger">Closed</span>';
         } 
      }else{
         return '-';
      }
   }

   BS_COLUMNS.unshift(
     {
         field       : 'id',
         title       : 'Sr.No',
         align       : 'center' ,
         formatter   : indexFormatter 
      },{
         field       : 'admission_open_date',
         title       : 'Admission Open Date',
         align       : 'center',
      },{
         field       : 'admission_close_date',
         title       : 'Admission Close Date',
         align       : 'center',
      },{
         field       : 'seat_intake',
         title       : 'Seat Intake',
         align       : 'center'
      },{
         field       : 'fee',
         title       : 'Fee',
         align       : 'center',
      },{
         field       : 'merit_date',
         title       : 'Merit Date',
         align       : 'center'
      },{
         field       : 'doc_start_date',
         title       : 'Doc Start Date',
         align       : 'center',
      },{
         field       : 'doc_end_date',
         title       : 'Doc Close Date',
         align       : 'center',
      },{
         field       : 'session_start_date',
         title       : 'Session Start Date',
         align       : 'center',
      },{
         field       : 'admission_status',
         title       : 'Admission Status',
         align       : 'center',
         formatter   : admissionStatusFormatter 
      }
   );

    var BS = {
        url             : '{{ url("admission/detail/list/$linkId") }}',
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 10,
        showRefresh     : true
    };

    var t = $("#admission_detail").bootstrapTable(BS);
</script>
@endsection