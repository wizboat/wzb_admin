@extends('layouts.app')

@section('content')


<section class="content">                                                     
    <div class="">
        <div id="msg"></div>
		<div class="mbdtable">
		    <div class="main-panel">
		        <section class="content">
		            <!-- <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus pr-2"></i>Add Gallery</button> -->
		            <div class="mainbar__title">Enrollment List</div>
		        </section>
		    </div>
		    <table id="erollListTable" class="table cardmbd table-borderless"></table>
		</div>
    </div>
</section>

<!-- Delete Eligibility-->
<div class="modal fade" id="deletepopup" tabindex="-1" role="dialog" aria-labelledby="deletepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <form id="deleted_enroll" name="deleted_enroll" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Delete</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>



<script>
var BS_COLUMNS  = [];

    function indexFormatter( value, row, index ){

        return index+1;
    }

    function editFormatter( value, row, index ){
        
         var url = '{{ url("enrolments/$id/:id/edit") }}';
         
         url = url.replace(':id', row.id);

         
         return "<a href="+url+" class='btn btn-outline-warning btn-md mr-3 m-0 weight-bold'>Edit</a> <button class='btn btn-outline-danger btn-md mr-3 m-0 weight-bold' onclick='deleteFun("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#deletepopup'>Delete</button>";

    }

    BS_COLUMNS.unshift(
        {
            field       : 'id',
            title       : 'Sr.No',
            align       : 'center' ,
            formatter   : indexFormatter 
        },{
            field       : 'enrollment_number',
            title       : 'Enrollment No.',
            align       : 'center' 
        },
        {
            field       : 'dob',
            title       : 'Date of Birth',
            align       : 'center' 
        },
        {
            field       : '',
            title       : 'Action',
            align       : 'center',
            formatter   : editFormatter 
        }
    );

    var BS = {
        url             : '{{ url("enrolments/$id/listapi") }}',
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 10,
        showRefresh     : true
    };

    var t= $("#erollListTable").bootstrapTable(BS);
    $.noConflict();

   var form = $("[name=deleted_enroll]");
   form.validate({

      submitHandler: function(form){
            formdata=$("#deleted_enroll").serialize();

            var successFun = function (data){
                
                var result = JSON.parse(data);
                var url = '{{ url("enrolments/$id/listapi") }}';

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#deletepopup").modal("hide");
                $("#loader").hide();
                $("#msg").html(result.message);
            }

            var url = "{{ url('enrolments/deleted') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
   });

</script>

@endsection