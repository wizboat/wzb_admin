<!DOCTYPE html>
<html>
<title>Login</title>
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500&amp;lang=en&amp;display=fallback" nonce="ACUEifZ_4E3itaNgpkugZA">
</head>
<body>
<div class="loginmain">
    <div class="container">
        <div class="login_sec">
            <img class="login_img img-fluid" src="assets/images/logo-school-account.png" alt="logo" width="120">
            <div class="card login_innersec">
                <div class="card-body">
                    <h5 class="login_header">School Login</h5>
                     <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="md-form">
                            <label for="email">E-mail</label>
                            <input id="email" type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                            <label for="password">Password</label>
                            <input id="password" type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="remember">
                                <label class="form-check-label" for="remember">Remember me</label>
                            </div>
                            <div>
                                <a href="{{ url('/') }}">Forgot password?</a>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block my-4 waves-effect z-depth-0" type="submit">Sign in</button>
                        <p class="mb-0">New to Wizboat ? <a href="{{ url('register') }}">Register</a></p>                                                    
                    </form>
                </div>
            </div>
            <p class="copyright_regis">Copyright © 2019 All rights reserved | wizboat.com </p>
        </div>
    </div>
</div>
<script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('assets/js/mdb.min.js') }}"></script>
</body>
</html>