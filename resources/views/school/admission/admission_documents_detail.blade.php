@extends('layouts.app')
@section('content')
<style type="text/css">
.fade:not(.show) {
    opacity: 1;
}
.modal-backdrop.in { 
    opacity: 0.4;
}
.bootbox .modal-dialog{
	margin-top: 135px;
}
</style>
<section class="content">                                                     
   <div id="msg"></div>
	<div class="mbdtable">
	    <div class="main-panel">
	        <section class="content">
	        	<h2>Admission Documents Confirmation List</h2>
	            <h5>Admission Open ID: {{ $admssionOpenId}}</h5>
	            <h5>Course : {{ ucfirst($course->course_name) }}</h5>

	            <section class="content" style="    display: inline-flex;">
		            <div class="form-group">
		                <label>Admission Refrence</label>
		                <input type="text" name="" class="form-control">                
		            </div>
		            <div class="form-group">
		                <label>Status</label>
			            	<select class="form-control">
			            		<option>confirm</option>
			            		<option>cancelled</option>
			            		<option>processiong</option>
			            	</select>              
		            </div>
		           
		            </section>
	           
	            <table class="table cardmbd table-borderless table-bordered table-hover">
	            	<thead>
	            		<th>S.No.</th>
	            		<th>Admission Reference ID</th>
	            		<th>Rank</th>
	            		<th>Applicant Name</th>
	            		<th>Contact No</th>
	            		<th>Status</th>
	            		@if(isset($value['admission_transaction_details']) && ($admissionDocuments[0]['admission_open_details']['doc_start_date'] < date('d-m-Y') && $admissionDocuments[0]['admission_open_details']['doc_end_date'] > date('d-m-Y')))
	            		<th></th>
	            		@endif
	            	</thead>
	            	<tbody>
	            		@foreach($admissionDocuments as $key=>$value)
		            		@if(isset($value['admission_transaction_details']))	            		
			            		<tr>
			            			<td>{{ ++$key }}</td>
			            			<td>{{ $value['admission_transaction_details']['addmission_refrence'] }}</td>
			            			<td>{{ $value['rank']  }}</td>
			            			<td>{{ $value['admission_transaction_details']['firstname'] }}</td>
			            			<td>{{ $value['admission_transaction_details']['mobile'] }}</td>
			            			<td>
			            				@if($value['admission_transaction_details']['admission_doc_submit']==1)
			            					<span class="badge badge-success">Verified</span>
			            				@elseif($value['admission_transaction_details']['admission_doc_submit']==2)
			            					<span class="badge badge-danger">Cancelled</span>
			            				@else
			            					<span class="badge badge-warning">Processing</span>         				
			            				@endif			            				
			            			</td>
			            			@if(isset($value['admission_transaction_details']) && ($admissionDocuments[0]['admission_open_details']['doc_start_date'] < date('d-m-Y') && $admissionDocuments[0]['admission_open_details']['doc_end_date'] > date('d-m-Y')))
			            			<td>
			            				<button class="btn btn-primary btnTable status" data-id="{{ $value['admission_transaction_details']['id'] }}" data-tp='con'>Verify</button> 
			            				<button class="btn btn-danger btnTable status"  data-id="{{ $value['admission_transaction_details']['id'] }}" data-tp='can'>Reject</button>
			            			</td>
			            			@endif
			            		</tr>
		            		@endif
	            		@endforeach  

	            		@if(count($admissionDocuments) == 0)
	            				<tr>
	            					<td colspan="5" class="text-center">Data not found..</td>
	            				</tr>
	            		@endif         		
	            	</tbody>
	            </table>
	        </section>
	    </div>
	</div>
</section>
<script type="text/javascript" src="https://makitweb.com/demo/deleteajax_confirm/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://makitweb.com/demo/deleteajax_confirm/bootbox.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  	$('.status').click(function(){
    	var el = this;
    	var id = $(this).data('id');
    	var type = $(this).data('tp');
    	bootbox.confirm("Do you really want to cancelled admission ?", function(result) {
   		if(result){
   			var successFun = function (data){
   				// location.reload();
        			$("#loader").hide();     
        			bootbox.alert('Record not deleted.');       
    			}
    			var data =  {
	        		'_token': "{{ csrf_token() }}",
	        		'id': id,
	        		'tp': type
	       	};
    			var url = "{{ url('admission/documents/cancelled') }}";
    			AjaxCall("POST",url,data,true,successFun);
    		}
   	});
  	});
});	
</script>
@endsection