<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionOpen extends Model
{
    protected $table = 'admission_open';
    protected $hidden = [];
    protected $fillable = ['course_id', 'start_date', 'close_date','admission_status','admission_comment','extend_date'];

    public function courseData(){
        return $this->belongsTo('App\Model\Course','course_id');
    }   
}
