<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Hash;
use DB;
use App\Model\User;
use App\Model\Course;
use App\Model\Syllabus;
use App\Model\CourseMaster;
use App\Model\SchoolProfile;
use App\Model\AdmissionOpen;
use App\Model\AdmissionOpenDetails;
use App\Model\linkCourseSchool;
use App\Model\CourseSyllabus;
use App\Model\CourseDetails;
use App\Model\CourseEligibility;
use App\Model\AdmissionTransactionDetails;
use App\Model\AcademicDetails;
use App\Model\MeritList;
use App\Model\AdmissionConfirm;


class AdmissionOpenController extends Controller
{
   public function __construct(){
      $this->middleware('auth');
   }

   public function create(){
      return view('school.admission.create');
   }

   public function admissionStatus(){
      return view('school.admission.status');
   }

   public function transaction(Request $request){
      return view('school.admission.transaction');
   }

   public function admissionCreateList(){

      $userId = Auth::user()->id;
      $courseMaster  = CourseMaster::wherehas('course',function($q) use ($userId){
         $q->where('user_id', $userId);                       
      })->with('course.courseDetails')->get()->toArray();            
      $data = [];
      if(count($courseMaster)>0){
         foreach ($courseMaster as $key => $value) {
            $data[$key]['course_name']             = $value['course_name'];
            $data[$key]['course_status']           = isset($value['course'][0]) ? $value['course'][0]['course_status'] : '';
            $data[$key]['linkid']                 = isset($value['course'][0]) ? $value['course'][0]['id'] : '';
            $data[$key]['admission_open_date']     = isset($value['course'][0]['course_details'][0]) ? $value['course'][0]['course_details'][0]['admission_open_date'] : '';
            $admissionStatus = 0;

            if(isset($value['course'][0]['course_details'][0])){
               $dateTimestamp1 = strtotime($value['course'][0]['course_details'][0]['admission_close_date']); 
               $dateTimestamp2 = strtotime(date("d-m-Y"));

               if($dateTimestamp1 > $dateTimestamp2){
                  $admissionStatus = 1;
               }
            }
            $data[$key]['admission_close_date']    = isset($value['course'][0]['course_details'][0]) ? $value['course'][0]['course_details'][0]['admission_close_date'] : '';
            $data[$key]['admission_status']        = isset($value['course'][0]['course_details'][0]) ? $value['course'][0]['course_details'][0]['admission_status'] : '';
         }                      
      }
      return response()->json($data);
   }

   public function admissionOpen($linkId){

      $courseDetails = DB::table('link_course_school')
      ->select('course_master.course_name','link_course_school.course_status','tbl_admission_open_details.admission_open_date','tbl_admission_open_details.admission_close_date','tbl_admission_open_details.merit_calculation','tbl_admission_open_details.merit_date','tbl_admission_open_details.doc_start_date','tbl_admission_open_details.doc_end_date','tbl_admission_open_details.session_start_date','tbl_admission_open_details.admission_status','tbl_admission_open_details.admission_confirm_date')
      ->leftJoin('course_master','course_master.id','=','link_course_school.course_master_id')
      ->leftJoin('tbl_admission_open_details','tbl_admission_open_details.course_link_id','=','link_course_school.id')
      ->where('link_course_school.id',$linkId)
      ->where('link_course_school.user_id',Auth::user()->id)
      ->whereOr('tbl_admission_open_details.admission_status','1')
      ->orderBy('tbl_admission_open_details.id','desc')
      ->first();
                           // dd($courseDetails->course_status);
      if($courseDetails->course_status == 0){
         print_r('course are disabled please enabled'); 
         exit;
      }  

      if(!isset($courseDetails->admission_open_date)){         
         return view('school.admission.open',compact('courseDetails','linkId'));
      }else{
         return view('school.admission.edit',compact('courseDetails','linkId'));
      }
   }

   public function admissionOpenUpdate(Request $request){

      $post = $request->all();

      $validator = \Validator::make($post, [ '__h'=>'required',
         'admission_start_date'=>'required|date_format:d-m-Y',
         'admission_close_date'=>'required|date_format:d-m-Y',                                             
         'merit_date'=>'required|date_format:d-m-Y',
         'doc_start_date'=>'required|date_format:d-m-Y',
         'doc_end_date'=>'required|date_format:d-m-Y',
         'session_start_date'=>'required|date_format:d-m-Y'
      ]);
      if ($validator->fails()) {
         return response()->json(['status'=>false,'message'=>'<div class="alert alert-danger" role="alert">Something went wrong</div>']);
      }

      if(strtotime($post['admission_start_date']) > strtotime($post['admission_close_date'])){
         return response()->json(['status'=>false,'message'=>'Something went wrong']);
      }

      if((strtotime($post['admission_close_date']) > strtotime($post['merit_date'])) || (strtotime($post['admission_close_date']) > strtotime($post['merit_date'])) ){
         return response()->json(['status'=>false,'message'=>'Something went wrong']);
      }

      if((strtotime($post['merit_date']) > strtotime($post['doc_start_date'])) || (strtotime($post['merit_date']) > strtotime($post['doc_end_date'])) ){
         return response()->json(['status'=>false,'message'=>'Something went wrong']);
      }

      $userId = Auth::user()->id;

      $linkData = DB::table('link_course_school')->where(['user_id'=>$userId,'id'=>$post['__h']])->first();      
      
      if(isset($linkData)){

         $result = CourseDetails::where('course_link_id',$post['__h'])->first();

         if(isset($result)) {

            $result->admission_open_date  =  $post['admission_start_date'];
            $result->admission_open_date  =  $post['admission_start_date'];
            $result->admission_status     =  strtotime($post['admission_start_date']) > strtotime(date('d-m-Y')) ? '2' : '1';
            $result->save();

            $resultAdmission = AdmissionOpenDetails::where([['course_link_id', '=', $post['__h']],['admission_status', '=', '1']])->orderBy('id','desc')->first();

            if(!$resultAdmission){
               $resultAdmission = new AdmissionOpenDetails();
            }

            $resultAdmission->course_link_id       = isset($result->course_link_id) ? $result->course_link_id : '';
            $resultAdmission->seat_intake          = isset($result->seat_intake) ? $result->seat_intake : '';
            $resultAdmission->fee                  = isset($result->fee) ? $result->fee : '';
            $resultAdmission->admission_open_date  = $post['admission_start_date'];
            $resultAdmission->admission_close_date = $post['admission_close_date'];
            $resultAdmission->admission_confirm_date = $post['admission_confirm_date'];
            $resultAdmission->merit_calculation    = ($post['merit_calculation']=='checked') ? '1' : '0';
            $resultAdmission->merit_date           = $post['merit_date'];
            $resultAdmission->doc_start_date       = $post['doc_start_date'];
            $resultAdmission->doc_end_date         = $post['doc_end_date'];
            $resultAdmission->session_start_date   = $post['session_start_date'];
            $resultAdmission->admission_comment    = isset($post['admission_comment']) ? $post['admission_comment'] : '';
            
            $resultAdmission->admission_status     =  strtotime($post['admission_start_date']) > strtotime(date('d-m-Y')) ? '2' : '1';
            $resultAdmission->save();

         }else{
            return response()->json(['status'=>false,'message'=>'Something went wrongg']);
         }
      }else{
         return response()->json(['status'=>false,'message'=>'Something went wrong']);
      }
      
      if(isset($result->id) && isset($resultAdmission->id)) {
         return response()->json(['status'=>true,'message'=>'<div class="alert alert-success" role="alert">Successfully create addmission</div>']);
      }else{
         return response()->json(['status'=>false,'message'=>'Something went wrong']);
      }
   }

   public function admissionDetail(Request $request, $linkId){

     $courseDetails = DB::table('link_course_school')
     ->select('course_master.course_name') 
     ->leftJoin('course_master','course_master.id','=','link_course_school.course_master_id')    
     ->where('link_course_school.id',$linkId)
     ->where('link_course_school.user_id',Auth::user()->id)                                               
     ->first();                           
     return view('school.admission.detail',compact('linkId','courseDetails'));
  }

  public function admissionDetailList(Request $request,$linkId){

   $result = DB::table('link_course_school')
   ->select('tbl_admission_open_details.fee','tbl_admission_open_details.seat_intake','tbl_admission_open_details.admission_open_date','tbl_admission_open_details.admission_close_date','tbl_admission_open_details.merit_date','tbl_admission_open_details.doc_start_date','tbl_admission_open_details.doc_end_date','tbl_admission_open_details.session_start_date','tbl_admission_open_details.admission_status')     
   ->join('tbl_admission_open_details','tbl_admission_open_details.course_link_id','=','link_course_school.id')
   ->where('link_course_school.id',$linkId)
   ->where('link_course_school.user_id',Auth::user()->id)
   ->orderBy('tbl_admission_open_details.id','desc')
   ->get();

   return response()->json($result);
}

public function transactionDetails(Request $request, $id){

   $post = $request->all();
   $AcademicDetails = AcademicDetails::select()->get()->toArray();
   $data = DB::table('tbl_admission_transacition_details as transaction')
   ->join('tbl_admission_open_details', 'tbl_admission_open_details.id', '=', 'transaction.tbl_admission_open_details_id')
   ->join('link_course_school', 'tbl_admission_open_details.course_link_id', '=', 'link_course_school.id')
   ->join('course_master', 'link_course_school.course_master_id', '=', 'course_master.id')
   ->select('transaction.id as id', 'transaction.firstname as firstname' ,'transaction.middlename as middlename','transaction.email_id as email_id','course_master.course_name as course_name', 'transaction.lastname as lastname', 'transaction.date_of_birth as dob', 'transaction.gender as gender', 'transaction.mothername as mothername', 'transaction.fathername as fathername', 'transaction.other_mobile as other_mobile', 'transaction.mobile as mobile', 'transaction.category as category', 'transaction.address_line_1 as address_line_1','transaction.address_line_2 as address_line_2', 'transaction.state as state', 'transaction.district as district', 'transaction.locality as locality', 'transaction.pincode as pincode','transaction.permanent_address_line_1 as permanent_address_line_1', 'transaction.permanent_address_line_2 as permanent_address_line_2', 'transaction.permanent_district as permanent_district', 'transaction.permanent_town as permanent_town','transaction.permanent_state as permanent_state', 'transaction.permanent_pincode as permanent_pincode','course_master.course_name as course_name', 'transaction.addmission_refrence as addmission_refrence', 'transaction.status as status', 'transaction.created_at as created_at')
   ->where('link_course_school.user_id',Auth::user()->id)
   ->where('transaction.id',$id)
   ->get()->toArray();
                   // dd($data);
   if($data){
      return view('school.admission.view',compact('data','AcademicDetails'));
   }  else{
      return redirect('admission/transaction');
   }           
}

public function transactionapi(Request $request){
   
   $data = DB::table('tbl_admission_transacition_details as transaction')
   ->leftJoin('tbl_admission_open_details', 'tbl_admission_open_details.id', '=', 'transaction.tbl_admission_open_details_id')
   ->leftJoin('link_course_school', 'tbl_admission_open_details.course_link_id', '=', 'link_course_school.id')
   ->leftJoin('course_master', 'link_course_school.course_master_id', '=', 'course_master.id')
   ->select('transaction.id as id', 'transaction.firstname as firstname', 'transaction.lastname as lastname','course_master.course_name as course_name', 'transaction.addmission_refrence as addmission_refrence', 'transaction.status as status', 'transaction.created_at as created_at')
   ->where('link_course_school.user_id',Auth::user()->id)
   ->get()->toArray();

   return response()->json($data);
}

public function academicDetails(Request $request){
   $data = AcademicDetails::select()->get()->toArray();
   return response()->json($data);
}

public function meritList(Request $request){

   $post = $request->all();
   $userId = Auth::user()->id;
   $sessionId = session()->getId();
   $admissionOpenData = '';
      //--------------------------------Show Course Master List-------------------//
   $courseMaster  = CourseMaster::select('id','course_name','course_status')->wherehas('course',function($q) use ($userId){
      $q->where('user_id', $userId);                       
   })->get()->toArray();     
   
      //--------- [c_i]   -> Course Id
      //--------- [a_o_i] -> Admission Open Id
      //--------- [s_i]   -> Session Id

      //--------------------------Generate Merit List Data----------------------------//
   if(isset($post['c_i']) && isset($post['a_o_i']) && isset($post['s_i']) )
   {         
      if($sessionId == $post['s_i'])
      {
         $schoolProfile  =  SchoolProfile::with('stateName')->where('user_id',$userId)->first();
         
         $courseDetails  =  DB::table('tbl_admission_open_details')
         ->select('tbl_admission_open_details.merit_calculation','tbl_admission_open_details.seat_intake','course_master.course_name')
         ->join('link_course_school','link_course_school.id','=','tbl_admission_open_details.course_link_id')
         ->join('course_master','course_master.id','=','link_course_school.course_master_id')
         ->where('link_course_school.user_id',$userId)
         ->where('tbl_admission_open_details.id',$post['a_o_i'])
         ->first();

         $meritListDetails = DB::table('tbl_admission_transacition_details')
         ->select('tbl_admission_transacition_details.addmission_refrence','tbl_admission_transacition_details.firstname','tbl_admission_transacition_details.lastname','tbl_admission_transacition_details.academic_percentage')
         ->leftJoin('tbl_meritlist','tbl_meritlist.tbl_admission_transacition_details_id', '=' ,'tbl_admission_transacition_details.id')
         ->where('tbl_meritlist.tbl_admission_open_details_id',$post['a_o_i'])
         ->get()->toArray();
                                       // dd($meritListDetails);
                                       // exit;
         return view('school.admission.merit_list_students',compact('schoolProfile','courseDetails','meritListDetails'));
      }
   }



      //--------------------------Show Admission Open Id's List-----------------------//
   if(isset($post['c_i'])){

      $admissionOpenData = DB::table('tbl_admission_open_details')
      ->select('tbl_admission_open_details.id as admission_open_id','tbl_admission_open_details.admission_status')
      ->join('link_course_school','link_course_school.id','=','tbl_admission_open_details.course_link_id')
      ->where('link_course_school.user_id',Auth::user()->id)
      ->where('link_course_school.course_master_id',$post['c_i'])
      ->get()->toArray();
   }

   $courseId = isset($post['c_i']) ? $post['c_i'] : '';
   return view('school.admission.merit_list',compact('courseMaster','admissionOpenData','courseId','sessionId'));
}

public function admissionDocuments(Request $request){

   $post = $request->all();
   $userId = Auth::user()->id;
   $sessionId = session()->getId();
   $courseId = isset($post['c_i']) ? $post['c_i'] : '';
   $admissionOpenData = '';
      //--------------------------------Show Course Master List-------------------//
   $courseMaster  = CourseMaster::select('id','course_name','course_status')->wherehas('course',function($q) use ($userId){
      $q->where('user_id', $userId);                       
   })->get()->toArray();     
   
      //--------------------------Generate Merit List Data----------------------------//
   if(isset($post['c_i']) && isset($post['a_o_i']) && isset($post['s_i']) )
   {         
      if($sessionId == $post['s_i'])
      {
         $admssionOpenId = $post['a_o_i'];
         $admissionDocuments = MeritList::with(['admissionTransactionDetails','admissionOpenDetails'])        
         ->whereHas('admissionOpenDetails.courseLinkDetails',function($q) use($userId){
            $q->where('user_id', '=', $userId);
         })                                           
         ->where('tbl_admission_open_details_id',$admssionOpenId)            
         ->get()->toArray();

         $course = CourseMaster::find($post['c_i']);
         return view('school.admission.admission_documents_detail',compact('admissionDocuments','admssionOpenId','course'));
      }
   }



      //--------------------------Show Admission Open Id's List-----------------------//
   if(isset($post['c_i'])){

      $admissionOpenData = DB::table('tbl_admission_open_details')
      ->select('tbl_admission_open_details.id as admission_open_id','tbl_admission_open_details.admission_status')
      ->join('link_course_school','link_course_school.id','=','tbl_admission_open_details.course_link_id')
      ->where('link_course_school.user_id',Auth::user()->id)
      ->where('link_course_school.course_master_id',$post['c_i'])
      ->get()->toArray();
   }

   return view('school.admission.admission_documents',compact('courseMaster','admissionOpenData','courseId','sessionId'));

}   

public function admissionDocumentsCancelledStatus(Request $request){

   $post = $request->all();

   $validator = \Validator::make($post, ['id'=>'required','tp'=>'required']);

   if ($validator->fails()) {
      return response()->json(['status'=>false,'message'=>'<div class="alert alert-danger" role="alert">Something went wrong</div>']);
   }

   if(!in_array($post['tp'], ['can','con'])){
      return response()->json(['status'=>false,'message'=>'<div class="alert alert-danger" role="alert">Something went wrong</div>']);
   }

   $userID = Auth::user()->id;
   $result = AdmissionTransactionDetails::whereHas('transaction.courseLinkDetails',function($q) use($userID){
      $q->where('user_id', '=', $userID);
   })   
   ->where('id',$post['id'])
   ->first();
      //----Status 2 Reject----
      //----Status 1 Confirm---

   if($result){         
      $result->academic_doc_submit = $post['tp']=='can' ? '2' : '1';
      $result->admission_doc_submit = $post['tp']=='can' ? '2' : '1';
      $result->save();

      return response()->json([                    
         'status' => true,                                        
         'message' => 'Admission Successfully Updated!'
      ]);
   }else{
      return response()->json([                    
         'status' => false,                                        
         'message' => 'Something went wrong'
      ]);
   }
}

public function admissionConfirm(Request $request){
   $post = $request->all();
   $userId = Auth::user()->id;
   $admissionID = isset($post['id']) ? $post['id'] : '';
      //----------------------------Admission Open List API------------------------//
   if(isset($post['ap'])){
      $result = AdmissionOpenDetails::with('courseLinkDetails')
      ->whereHas('courseLinkDetails',function($q) use($userId){
         $q->where('user_id', '=', $userId);
      })
      ->where('admission_confirm_date','<',date('d-m-Y'))
      ->orderBy('id','desc')
      ->get();

      if(count($result)>0){
         $arrayData = [];
         foreach ($result as $key => $value) {
            $arrayData[] = [  'admission_open_id'=>$value->id,
            'course_name'=>ucfirst($value->courseLinkDetails->course->course_name),
            'admission_confirm_date'=>$value->admission_confirm_date
         ];
      }
      return response()->json($arrayData);
   }else{
      return response()->json($result);
   }                      
}                                 

return view('school.admission.confirm');

}


public function admissionConfirmDetail(Request $request){
   $post = $request->all();
   $userId = Auth::user()->id;

       //----------------------------Confirm Admission List API------------------------//
   if(isset($post['id'])) {
      $result = AdmissionConfirm::with(['admissionTransaction','admissionOpen'])
      ->whereHas('admissionOpen.courseLinkDetails',function($q) use($userId){
         $q->where('user_id', '=', $userId);
      })
      ->get();

      if(count($result)>0){
         $arrayData = [];
         foreach ($result as $key => $value) {               
            $arrayData[] = [  'addmission_refrence'=>$value->admissionTransaction->addmission_refrence,
            'name'=>ucfirst($value->admissionTransaction->firstname)                                    
         ];               
      }
      return response()->json($arrayData);
   }else{
      return response()->json($result);
   }             
}
$AdmissionID = isset($post['aid']) ? $post['aid'] : '';
return view('school.admission.confirm_details',compact('AdmissionID'));
}

   //---------------Cron Function------------------------------//

public function generateMeritList(){

      // $admissionOpenData = DB::table('tbl_admission_open_details')
      //                            ->select('tbl_admission_open_details.*')
      //                            ->join('link_course_school','link_course_school.id','=','tbl_admission_open_details.course_link_id')                                                 
      //                            ->get();

      // $userId = Auth::user()->id;

      // $courseMaster  = CourseMaster::wherehas('course',function($q) use ($userId){
      //                                           $q->where('user_id', $userId);                       
      //                                        })->with('course.courseDetails')->get()->toArray();   

   $result = AdmissionOpenDetails::with('transaction')->where('merit_date',date('d-m-Y'))->get()->toArray();        

      //--------------------Need to this Data into Redis Also-----------//
   if(count($result) > 0){
      $finalData = [];
      foreach ($result as $key1 => $value1) {       
         $academicPercentage = [];        
            // dd($value1);
         if(isset($value1['transaction']) && count($value1['transaction']) > 0) {    
            foreach ($value1['transaction'] as $key2 => $value2) {
               $data[$value2['id']] = $value2;
               $academicPercentage[$value2['id']] = $value2['academic_percentage'];
            }

            if(isset($value1['transaction']) && $value1['transaction'] ==1 ){
               arsort($academicPercentage);
            }
                  // print_r($academicPercentage); exit;
            $i=1;
            foreach ($academicPercentage as $key => $value) {

               $finalData[] = [  'tbl_admission_open_details_id' => $value1['id'],
               'tbl_admission_transacition_details_id'=> $key,
               'rank'=>$i++
            ];
         }               
      }            
   }

   DB::table('tbl_meritlist')->insert($finalData);

   print_r($finalData);
   print_r($academicPercentage);
   exit;
}      
}

public function generateConfirmList(){

   $text = '';
   $result = AdmissionOpenDetails::with('transaction')->whereHas('transaction',function($q) use($text){
      $q->where('admission_doc_submit', '=', 1);
   })   
   ->where('doc_end_date',date('d-m-Y'))
   ->get()->toArray();
   
   if(count($result)>0) {         
      $data = [];
      foreach ($result as $key => $value) {
         foreach ($value['transaction'] as $key => $value) {
            $data[] = ['tbl_admission_transacition_details_id'=>$value['id'],
            'tbl_admission_open_details_id'=>$value['tbl_admission_open_details_id']
         ];
      }
      AdmissionConfirm::insert($data);                    
   }
}else{
   
}
}

public function generatePaymentSettled(){

   $circularDate = ['1','6','11','16','21','26'];

   if(in_array(date('d'), $circularDate)){

      echo 'yesy';
      
   }
}
}