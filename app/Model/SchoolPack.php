<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolPack extends Model
{
   	protected $table = 'school_pack';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['user_id', 'pack_id','transaction_id','start_date', 'end_date','status'];
}
