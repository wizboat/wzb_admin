<?php
namespace App\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Cache;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    use HasRoles;

    protected $fillable = [
        'user_name', 'name','email', 'mobile_no','password','user_hash','user_role','email_verify','otp','user_status'
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function schoolprofile(){
        return $this->hasOne('App\Model\SchoolProfile','user_id', 'id');
    }
   
    public function userdata(){
        return $this->hasOne('App\Model\SubUser','user_id', 'id');
    }

    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
}
