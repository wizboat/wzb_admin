<?php   
namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\CourseMaster;
use App\Model\Enrollment;

use App\Model\EnrollmentImport;
use Maatwebsite\Excel\Facades\Excel;


class EnrolmentController extends Controller
{
 public function __construct(){
  $this->middleware('auth');
}

public function index() {      
  return view('school.enrolment.index');         
}

public function enrollmentList(Request $request){
  $userId = Auth::user()->id;
  $result = CourseMaster::wherehas('course',function($q) use ($userId){$q->where('user_id', $userId);                                                    
})->with('course')->get()->toArray();  

      // $result = Enrollment::get()->toArray();
  return response()->json($result);
}

public function list(Request $request,$id){     
  return view('school.enrolment.list',compact('id')); 
}

public function listapi(Request $request,$id){
 
  $result = Enrollment::where('deleted','1')->get()->toArray();
  return response()->json($result);
}

public function upload(Request $request,$id){
  return view('school.enrolment.upload');     
}

public function uploadCSV(Request $request){
  
  Excel::import(new EnrollmentImport,request()->file('file'));
  
  return back()->with('success','Enrollments uploaded successfully!');;
}

public function listEdit(Request $request,$id,$idd){
  
  $enrollment = Enrollment::where('id',$idd)->first();

  return view('school.enrolment.edit',compact('enrollment'));     

}

public function update(Request $request){

  $post = $request->all();

  $redirect = url()->previous();

  $result = Enrollment::where('id',$post['enid'])->first();

  $result->enrollment_number = $post['enrollment_number'];
  $result->dob = $post['dob'];
  $result->save();

  return redirect("$redirect")->with('success', ['Enrollment update successfully!']);   
}

public function enrollmentDelete(Request $request){

  $post = $request->all();    
  
  $validator = \Validator::make($post, ['__hash'=>'required']);

  if ($validator->fails()) {
    
    $status = false;
    return response()->json(['status' => $status, 'message' => $validator]);     
  }


  $result = Enrollment::where('id', $post['__hash'])->first();

  if($result){
    $result->deleted  = '0';
    $result->save();
  }
  
  if($result->id){
    $message = '<div class="alert alert-success" role="alert"> Deleted Successfully!!!</div>';
    $status = true;
  }
  else{
    $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
    $status = false;            
  }

  return response()->json(['status' => $status, 'message' => $message]);       
}


}
