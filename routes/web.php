<?php

Route::get('/', function () {
	return view('welcome');
});

Route::get('getip',function(){
	$ch = curl_init ();
      // set URL and other appropriate options
	curl_setopt ($ch, CURLOPT_URL, "http://ipecho.net/plain");
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

      // grab URL and pass it to the browser

	$ip = curl_exec ($ch);
	echo "The public ip for this server is: $ip";
      // close cURL resource, and free up system resources
	curl_close ($ch);

	$ip= \Request::ip();
	 // exit;
  	//   $ip= "103.195.248.111";
	$data = \Location::get($ip);
	dd($data);
});

Route::get('/merit', 'AdmissionOpenController@generateMeritList');		
Route::get('/confirm_admission', 'AdmissionOpenController@generateConfirmList');		
Route::get('/payment_settle', 'AdmissionOpenController@generatePaymentSettled');		

Auth::routes();

Route::get('test', 'HomeController@test');
Route::get('print_test', 'HomeController@printTest');
Route::get('admission_document', 'AdmissionOpenController@admissionDocument');
Route::get('admission_open_id', 'AdmissionOpenController@admissionOpenId');
//************* Step 1 Email Verify ***************//
Route::group(array('prefix' => 'school/email-verify/'), function() {		
	Route::get('/', 'Auth\SchoolStepsController@emailVerifyView');	
	Route::get('/re-send-email', 'Auth\SchoolStepsController@resendEmail');		
	Route::post('account/update', 'Auth\SchoolStepsController@emailVerifyUpdate');
});

//************* Step 2 Account Payment Verify ***************//
Route::group(array('prefix' => 'school-profile-review/'), function() {		
	Route::get('/', 'Auth\SchoolStepsController@reviewVerifyView');	
});

//************* Step 3 Profile Verify ***************//
Route::group(array('prefix' => 'school-profile-verify/'), function() {		
	Route::get('/', 'Auth\SchoolStepsController@schoolProfileVerifyView');	
	Route::post('update', 'Auth\SchoolStepsController@schoolProfileVerifyUpdate');
	//Route::get('account/verify', 'SchoolProfileController@schoolEmailVerifyFirst');	
	//Route::get('account/verify/hq', 'SchoolProfileController@schoolEmailVerifySecond');	
});

//************* Step 4 Course Selections ***************//
Route::group(array('prefix' => 'course-selections/'), function() {		
	Route::get('/', 'Auth\SchoolStepsController@schoolCourseVerifyView');	
	Route::post('update/{schoolprofileid}', 'Auth\SchoolStepsController@schoolCourseSelectionVerifyUpdate');
});

Route::get('/home', 'DashboardController@dashboard');
Route::get('/', 'DashboardController@dashboard');

Route::group(array('prefix' => 'account/'), function() {
	Route::get('verify', 'SchoolProfileController@schoolEmailVerifyFirst');	
	Route::get('verify/hq', 'SchoolProfileController@schoolEmailVerifySecond');	
	Route::post('verify/otp', 'SchoolProfileController@schoolEmailVerifyUpdate');
});

Route::group(array('prefix' => 'school/'), function() {
	Route::post('account/auth/profile', 'SchoolProfileController@step1Update');
	Route::get('account/profile', 'Auth\AuthSchoolController@step1');	
	Route::get('account/review', 'Auth\AuthSchoolController@step2');	
	Route::get('account/payment', 'Auth\AuthSchoolController@step3');	
	Route::post('account/payment/pay', 'PaymentController@schoolFirst');	
	Route::get('review-confirmation', 'SchoolProfileController@reviewConfirmation');
	//Route::get('account/verify/hq', 'SchoolProfileController@schoolEmailVerifySecond');	
});

Route::group(array('prefix' => 'payment/'), function() {
	Route::get('confirm_payment', 'PaymentController@confirmPayment');
	Route::get('confirm_payment/{admissionId}/view', 'PaymentController@confirmPaymentView');
});

Route::group(array('prefix' => 'course/'), function() {
	Route::get('{course_hash}/details', 'CourseController@courseView');
	Route::get('eligibility/{linkId}/list', 'CourseController@eligibilityList');
	Route::post('eligibility/deleted', 'CourseController@eligibilityDelete');
	Route::post('eligibility/disabled', 'CourseController@eligibilityDisable');
	Route::post('eligibility/eligibilitystore', 'CourseController@eligibilityStore');

	Route::get('syllabus/{linkId}/list', 'CourseController@syllabusList');
	Route::post('syllabus/store', 'CourseController@syllabusStore');
	Route::post('syllabus/update', 'CourseController@syllabusUpdate');
	Route::post('syllabus/deleted', 'CourseController@syllabusDelete');
	
	Route::post('syllabus/disabled', 'CourseController@syllabusDisable');
	Route::post('tab1/update/{linkId}', 'CourseController@courseDetailsUpdate');
	Route::post('tab2/update/{linkId}', 'CourseController@eligibilityUpdate');
	Route::post('syllabus/update/{linkId}', 'CourseController@syllabusUpdate');
	Route::post('status', 'CourseController@status');
}); 

Route::group(array('prefix' => 'admission/'), function() {
	Route::get('status', 'AdmissionOpenController@admissionStatus');
	Route::get('transaction', 'AdmissionOpenController@transaction');

	Route::get('live_search/action', 'AdmissionOpenController@action')->name('live_search.action');

	Route::get('transactionapi', 'AdmissionOpenController@transactionapi');
	Route::get('transaction/{id}/view', 'AdmissionOpenController@transactionDetails');	
	Route::get('transaction/academicdetails', 'AdmissionOpenController@academicDetails');	
	Route::get('create/{linkId}', 'AdmissionOpenController@admissionOpen'); 
	Route::get('open/list', 'AdmissionOpenController@admissionCreateList'); 	
	Route::get('create/{id}/detail', 'AdmissionOpenController@admissionDetail');
	Route::get('detail/list/{id}', 'AdmissionOpenController@admissionDetailList');
	Route::post('open/update', 'AdmissionOpenController@admissionOpenUpdate'); 	
	Route::get('merit-list','AdmissionOpenController@meritList');
	Route::get('documents','AdmissionOpenController@admissionDocuments');
	Route::get('confirm','AdmissionOpenController@admissionConfirm');
	Route::get('confirm/d','AdmissionOpenController@admissionConfirmDetail');
	Route::post('documents/confirm','AdmissionOpenController@admissionDocumentsConformStatus');
	Route::post('documents/cancelled','AdmissionOpenController@admissionDocumentsCancelledStatus');
}); 

Route::group(array('prefix' => 'users/'), function() {
	Route::post('status', 'UserController@statusUser');
});

Route::group(array('prefix' => 'faculty/'), function() {
	Route::post('status', 'FacultyController@statusFaculty');
	Route::post('list', 'FacultyController@show');
	Route::post('deleted', 'FacultyController@deleteGallery');
	Route::post('disabled', 'FacultyController@disableGallery');
});

Route::group(array('prefix' => 'settings/'), function() {
	Route::get('security', 'SettingController@securityIndex');
	Route::get('privacy', 'SettingController@privacyIndex');
	Route::get('notifications', 'SettingController@notifications');
	Route::post('general/edit', 'SettingController@generalSettingEdit');
	Route::post('general/update', 'SettingController@generalSettingUpdate');
	Route::get('changePassword','HomeController@showChangePasswordForm');
	Route::post('changePassword','HomeController@changePassword')->name('changePassword');    
});

Route::group(array('prefix' => 'profile/'), function() {
	Route::post('update', 'ProfileController@update');	 	 
	Route::post('up/banner', 'ProfileController@schoolBannerImage');	 	 
	Route::post('up/logo', 'ProfileController@schooLogoImage');	 	 
}); 

Route::group(array('prefix' => 'enrolments/'), function() {
	Route::get('update', 'EnrolmentController@update');	 
	Route::get('enrollmentList', 'EnrolmentController@enrollmentList');	 
	Route::get('{courseid}/list', 'EnrolmentController@list');	 
	Route::get('{courseid}/listapi', 'EnrolmentController@listapi');	 
	Route::get('{courseid}/upload', 'EnrolmentController@upload');
	Route::get('{courseid}/{id}/edit', 'EnrolmentController@listEdit');
	Route::post('deleted', 'EnrolmentController@enrollmentDelete');	 
}); 

Route::group(array('prefix' => 'gallery/'), function() {
	Route::post('list', 'GalleryController@show');	 
	Route::post('upload', 'GalleryController@upload');	 
	Route::post('deleted', 'GalleryController@deleteGallery');	 
	Route::post('disabled', 'GalleryController@disableGallery');	 
}); 

Route::group(array('prefix' => 'enquiry/'), function() {
	Route::get('list', 'EnquiryController@show');	 
}); 

Route::group(array('prefix' => 'bank/'), function() {
	Route::get('list', 'BankController@list');	 
}); 

Route::resource('course', 'CourseController');
Route::resource('admission', 'AdmissionOpenController');
Route::resource('users', 'UserController');
Route::resource('enrolments', 'EnrolmentController');
Route::resource('roles', 'RoleController');
Route::resource('faculty', 'FacultyController');
Route::resource('permissions', 'PermissionController');
Route::resource('payments/ad', 'PaymentController');
Route::resource('bank', 'BankController');
Route::resource('all-activity', 'ActivityController');
Route::resource('settings', 'SettingController');
Route::resource('gallery', 'GalleryController');
Route::resource('profile', 'ProfileController');

Route::post('schoolDetailUpdate/update', 'SchoolController@schoolDetailUpdate')->name('home');
