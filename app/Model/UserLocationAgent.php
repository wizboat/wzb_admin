<?php

namespace App\Model;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class UserLocationAgent extends Model
{
    protected $connection = 'mongodb';
    protected $collection = "user_location_agent";
}
