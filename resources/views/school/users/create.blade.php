@extends('layouts.app')

@section('title', '| Add User')

@section('content')
<section class="content"> 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
            <div class="card mb-5">
                <div class="card-body">
                    <span class="card-title"><i class="fa fa-user-plus pr-2" aria-hidden="true"></i><strong>Add User</strong></span>                             
                    <div class="">  
                        <div class="">
                            {{ Form::open(array('url' => 'users')) }}
                                <div class="md-form form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {{ Form::label('name', 'Name *') }}
                                    {{ Form::text('name', '', array('class' => 'form-control mb-0')) }}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="md-form form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    {{ Form::label('email', 'Email *') }}
                                    {{ Form::email('email', '', array('class' => 'form-control mb-0')) }}
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <h5><b>Give Role *</b></h5>
                                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                                    @foreach ($roles as $role)
                                        {{ Form::checkbox('roles[]',  $role->id ) }}
                                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                                    @endforeach
                                    @if ($errors->has('roles'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('roles') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="md-form form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {{ Form::label('password', 'Password *') }}
                                    {{ Form::password('password', array('class' => 'form-control mb-0')) }}
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="md-form form-group mt-0">
                                    {{ Form::label('password', 'Confirm Password *') }}
                                    {{ Form::password('password_confirmation', array('class' => 'form-control mb-0')) }}
                                </div>
                                 <div class="md-form form-group m-0">
                                    {{ Form::submit('Add', array('class' => 'btn btn-primary btn-sm mr-2 m-0')) }}
                                    <a href="{{ url('/users')}}" class="btn btn-outline-secondary btn-sm m-0">Back</a>
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection