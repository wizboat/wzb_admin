<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Wizboat-Online Admission Platform</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" href="{{ url('assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets/vendors/jvectormap/jquery-jvectormap.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/demo/dashboard_style.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/dashboard-styles.css') }}">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
  <!-- <link rel="stylesheet" href="{{ url('assets/css/style.css') }}"> -->
  <body>

    <script src="{{ url('assets/js/preloader.js') }}"></script>

    <div class="body-wrapper">
      <!-- -- Sidebar --  -->
      @include('school.settings.sidebar')

      <div class="main-wrapper mdc-drawer-app-content">
        <!-- -- Header --  -->
        @include('layouts.header')
        <!-- -- Content -- -->
        <div class="page-wrapper mdc-toolbar-fixed-adjust">
          <main class="content-wrapper">
            @yield('content')
          </main>
      </div>
        <!-- -- Footer --  -->
        @include('layouts.footer')
      </div>
    </div>
  </body>
</html>
