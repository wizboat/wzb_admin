<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionTransactionDetails extends Model
{
    protected $table = 'tbl_admission_transacition_details';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['txn_id','addmission_refrence','tbl_admission_open_details_id','user_id','firstname','middlename','lastname','email','date_of_birth','gender','mothername','fathername','other_mobile','mobile','category','address_line_1','address_line_2','state','locality','pincode','permanent_address_line_1','permanent_address_line_2','permanent_town','permanent_state','permanent_pincode','status','academic_percentage','academic_doc_submit','admission_doc_submit','admission_confirm'];   

    public function transaction(){
        return $this->belongsTo('App\Model\AdmissionOpenDetails', 'tbl_admission_open_details_id', 'id');
    }
}
