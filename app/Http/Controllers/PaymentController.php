<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SchoolBank;
use App\Model\User;
use App\Model\PaymentTransaction;
use App\AdmissionTransactionDetails;
use App\AdmissionPaymentTransaction;
use App\AdmissionOpenDetails;
use App\Model\SchoolPack;
use App\Model\courseMaster;
use Session;
use Auth;
use Hash;
use DB;

class PaymentController extends Controller
{
  public function __construct(){
   $this->middleware('auth');
 }

 public function index(){    	
   $bankData = SchoolBank::with('bank')->paginate(3);   		
   return view('school.payments.index',['bankData'=>$bankData]);   		
 }

 public function paymentRegistrationSchool(Request $request){
   $post = $request->all();    
   $userData = User::where('id', Auth::user()->id)->first();
   $userData->user_status = getenv('STATUS_11');  
   $userData->save();

   if(isset($userData->id)){
    $tempSchoolData = TempSchoolProfile::where('user_id',$userData->id)->first();
    $schoolProfileData = SchoolProfile::create([
     'user_id'=>$userData->id,
     'school_name'=>$tempSchoolData->school_name,    			    			    			
     'school_email'=>$tempSchoolData->school_email,
     'board'=>$tempSchoolData->board,
     'address1'=>$tempSchoolData->address1,
     'address2'=>$tempSchoolData->address2,
     'state'=>$tempSchoolData->state,
     'city'=>$tempSchoolData->city,
     'pincode'=>$tempSchoolData->pincode,
     'phone1'=>$tempSchoolData->phone1,
     'phone2'=>$tempSchoolData->phone2,
     'phone3'=>$tempSchoolData->phone3,
     'website'=>$tempSchoolData->website				
   ]);

    $schoolProfileData->school_hash = bcrypt($schoolProfileData->id);
    $schoolProfileData->save();    		
    		//Session::flash('success_message', 'Faculty has been created successfully');
    return redirect('/');
  }
}

public function schoolFirst(Request $request){
  $post = $request->all();
  $validator = \Validator::make($post, ['_token'=>'required','pt'=>'required']);

  if ($validator->fails()) {
    return redirect()->back()->withErrors($validator);        
  }else{ 
    if (Hash::check('schoolActivation', $post['pt'])){
      $paymentData = new PaymentTransaction();
      $paymentData->user_id = Auth::user()->id;
      $paymentData->description = 'Account activation';
      $paymentData->transaction_status = 'processing';
      $paymentData->transaction_type = 'schoolActivation';
      $paymentData->amount = 5999;
      $paymentData->save();

      $schoolPack = new SchoolPack();
      $schoolPack->user_id = Auth::user()->id; 
      $schoolPack->start_date = date('d-m-Y');
      $schoolPack->end_date = date('d-m-Y',strtotime('+1 years'));
      $schoolPack->status = 1;
      $schoolPack->save();
      return redirect('/');                
    }else{
     return redirect()->back(); 
   }
 }
 print_r($post);
}

public function confirmPayment(Request $request){

  $post = $request->all();

  // $courseMaster = DB::table('course_master')
  // ->select('id','course_name')
  // ->get()->toArray();

  if(isset($post['i'])){
    $confirmPayment = DB::table('tbl_admission_open_details')

    ->join('link_course_school','link_course_school.id', '=' , 'tbl_admission_open_details.course_link_id')

    ->join('course_master','course_master.id', '=' , 'link_course_school.course_master_id')

    ->select('tbl_admission_open_details.id as admission_open_id','course_master.course_name as course_name')
    ->get()->toArray();
    return response()->json($confirmPayment);
  }
  // print_r($courseMaster); 
  // exit;
  return view('school.payments.confirm_payment',compact('courseMaster'));

}
public function confirmPaymentView(Request $request,$admissionID){

  $post = $request->all();

  $courseMaster = DB::table('tbl_admission_open_details')
  ->join('link_course_school','link_course_school.id', '=' , 'tbl_admission_open_details.course_link_id')
  ->join('course_master','course_master.id', '=' , 'link_course_school.course_master_id')
  ->select('tbl_admission_open_details.id as admission_open_id','course_master.course_name as course_name')
  ->get()->first();

  if(isset($post['i'])){
    $confirmPaymentView = DB::table('tbl_admission_transacition_details')
    ->select('txn_id','addmission_refrence','firstname','lastname')
    ->get()->toArray();
    return response()->json($confirmPaymentView);
  }

  // print_r($courseMaster); exit;
  return view('school.payments.view',compact('courseMaster','admissionID'));

}
}
