<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\SchoolProfile;
use App\Model\State;

class ProfileController extends Controller
{
   public function __construct(){
      $this->middleware('auth');
   }

   public function index(){
      $schoolId = Auth::user()->id;
      $state = State::all();
      $schoolProfile = SchoolProfile::with('stateName')->where('user_id',$schoolId)->first();
      return view('school.profile.index',compact("schoolProfile","state"));   		
   }

   public function update(Request $request){

      $post = $request->all();

      // TabAbout Us 
      if(isset($post['tab']) && $post['tab']=='about_us'){

         $validator = \Validator::make($post, ['aboutus'=>'required']);

         if ($validator->fails()) {
            return response()->json(['status'=>false,'msg'=>'<div class="alert alert-danger" role="alert">Something went wrong</div>']);
         }

         $result = SchoolProfile::where('user_id',Auth::user()->id)->first();
         if($result){
            $result->about = $post['aboutus'];
            $result->save();
            return response()->json(['status'=>true,'msg'=>'Successfully Updated About Us']);
         }else{
            return response()->json(['status'=>false,'msg'=>'Something went wrong']);
         }
      }

      if(isset($post['tab']) && $post['tab']=='details'){

         // print_r($post); exit;
         $validator = \Validator::make($post, ['school_name'=>'required',
            'board'=>'required',
            'school_year_founded'=>'required',
            'address1'=>'required',
            'address2'=>'required',
            'city'=>'required',
            'state'=>'required',
            'pincode'=>'required',
            'phone1'=>'required',
            'school_email'=>'required']);

         if ($validator->fails()) {
            return response()->json(['status'=>false,'msg'=>'<div class="alert alert-danger" role="alert">Something went wrong</div>']);
         }

         $result = SchoolProfile::where('user_id',Auth::user()->id)->first();
         if($result){
            $result->school_name = $post['school_name'];
            $result->board = $post['board'];
            $result->year_founded = $post['school_year_founded'];
            $result->address1 = $post['address1'];
            $result->address2 = $post['address2'];
            $result->city = $post['city'];
            $result->state = $post['state'];
            $result->pincode = $post['pincode'];
            $result->phone1 = $post['phone1'];
            $result->phone2 = $post['phone2'];
            $result->phone3 = $post['phone3'];
            $result->school_email = $post['school_email'];
            $result->website = $post['website'];
            $result->save();

            $data = ['school_name'=>$result->school_name,
                     'school_board'=>$result->board,
                     'school_year_founded'=>$result->year_founded,
                     'address1'=>$result->address1,
                     'address2'=>$result->address2,
                     'school_city'=>$result->city,
                     'state'=>$result->stateName->state,
                     'school_pincode'=>$result->pincode,
                     'phone1'=>$result->phone1,
                     'phone2'=>$result->phone2,
                     'phone3'=>$result->phone3,
                     'school_email'=>$result->school_email,
                     'school_website'=>$result->website];
            return response()->json(['status'=>true,'msg'=>'Successfully Profile Details','data'=>$data]);
         }else{
            return response()->json(['status'=>false,'msg'=>'Something went wrong']);
         }
      }

      print_r($post);
   }

   public function schoolBannerImage(Request $request){

        $post = $request->all();
        $filename = '';
        if(isset($post['image'])){
            $uploadDir = public_path().'/assets/images/school_banner/';
            $image_parts = explode(";base64,", $post['image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);

            $file1 =  uniqid() . '.png';
            $filename = $uploadDir . $file1;
            file_put_contents($filename, $image_base64);
        }
        
        $result = SchoolProfile::where('user_id',Auth::user()->id)->first();        
        $result->school_banner  = !empty($filename) ? '/assets/images/school_banner/'.$file1 : '/assets/images/default.jpg';   
        $result->save();
        
        return response()->json(['code' => 200, 'msg'=>'School Banner Updated Successfully','data' => url($result->school_banner) ]);        
    }

    public function schooLogoImage(Request $request){

        $post = $request->all();
        $filename = '';
        if(isset($post['image'])){
            $uploadDir = public_path().'/assets/images/school_logo/';
            $image_parts = explode(";base64,", $post['image']);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);

            $s = Auth::user()->id;
            $s1 = time();
            $file1 =   $s.$s1 .'.png';
            $filename = $uploadDir . $file1;
            file_put_contents($filename, $image_base64);
        }
        // echo $file1; exit;
        $result = SchoolProfile::where('user_id',Auth::user()->id)->first();        
        $result->school_logo  = !empty($filename) ? '/assets/images/school_logo/'.$file1 : '/assets/images/default.jpg';   
        $result->save();
        
        return response()->json(['code' => 200, 'msg'=>'School Logo Updated Successfully','data' => url($result->school_logo) ]);        
    }
}
