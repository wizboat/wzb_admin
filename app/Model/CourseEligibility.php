<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseEligibility extends Model
{
    protected $table = 'course_eligibility';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['course_link_id','eligibility','deleted','disabled'];
}