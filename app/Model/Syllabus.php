<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model 
{
    protected $table = 'course_syllabus';
    protected $hidden = [];
    protected $fillable = ['link_course_school_id','syllabus_name','syllabus_hash','disabled','deleted'];
}
