<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionConfirm extends Model
{
    protected $table = 'tbl_confirm_admission';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['tbl_admission_transacition_details_id','tbl_admission_open_details_id'];    

    public function admissionTransaction(){
        return $this->belongsTo('App\Model\AdmissionTransactionDetails', 'tbl_admission_transacition_details_id', 'id');
    }

	public function admissionOpen(){
        return $this->belongsTo('App\Model\AdmissionOpenDetails', 'tbl_admission_open_details_id', 'id');
    }    
}
