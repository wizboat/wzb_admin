@extends('school.settings.app')
@section('content')
<section class="content">             
   <div class="mdc-top-app-bar__title">Security & Login</div>
   <div class="">                    
      <div class="edit-general-info">
        <div class="card">
         <div class="card-body">
            <h5 class="card-title pb-2  border-bottom"><strong> Where You're Logged In</strong></h5>
            <ul class="security_head">
               <li class="info-box-text"><i class="fa fa-desktop" aria-hidden="true"></i></li>
               <li class="desc_security">
                  <p class="desc_innersec">Windows PC · Bhayandar, India</p>
                  <p class="mb-0"><small>Chrome <font color="green">&emsp; Active</font></small></p>
               </li>
            </ul>
         </div>
      </div>
      <div class="card mt-3">
         <div class="card-body">
            <h5 class="card-title pb-2  border-bottom"><strong> Login </strong></h5>
            <ul class="security_head">
               <a href="{{ url('/settings/changePassword') }}" class="btn btn-outline-primary editBtn mt-2 pull-right">Edit</a>
               <li class="info-box-text"><i class="fa fa-key"></i></li>
               <li class="desc_security">
                  <p class="desc_innersec">Change Password</p>
                  <p class="mb-0"><small>It's a good idea to use a strong password that you're not using elsewhere</small></p>
               </li>
            </ul>
         </div>
      </div>
         <!-- <div class="card mt-3">
            <div class="card-header">
               Privacy
            </div>
            <ul class="list-group list-group-flush">
               <li class="list-group-item"> <b>Find People and Contact You</b></li>
               <li class="list-group-item"><h6><small>Every people can lookup your profile.</small></h6></li>
               <li class="list-group-item"><h6><small>Every people can find your email address and phone numner with address.</small></h6></li>
               <li class="list-group-item"><h6><small>Every student can take admission from any where.</small></h6></li>
            </ul>
         </div>  -->                         

      </div>
   </section> 
   <script type="text/javascript">
      $(document).ready(function()
      {

      });
   </script>
   @endsection