@extends('layouts.app')

@section('content')

<style type="text/css">
    .table {
        color: #000;
    }
    table.table th, table.table td {
        padding: 18px 0.75rem;
        font-size: 15px;
    }
    table.table td {
        font-weight: 400;
    }
</style>
<section class="padtp">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Admission Documents</a></li>
            <li class="breadcrumb-item active" aria-current="page">Admission Open ID</li>
        </ol>
    </nav>
    <div class="mdc-top-app-bar__title">Admission Open ID : 25653536</div>
    <div class="admisssion_detail">
        <div class="">
            <div class="document_inner_card">
                <table id="myTable" class="table cardmbd table-borderless table-bordered table-hover text-center">
                    <thead>
                        <tr>
                            <th width="80">Sr. No.</th>
                            <th>Admission ID</th>
                            <th>Applicant Name</th>
                            <th>Academic Details Verify</th>
                            <th>Document Upload / Submitted</th>
                            <th>Document Verify</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>256563656</td>
                            <td>Surya</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td data-toggle="collapse" data-target="#accordion" class="clickable"><button href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View / Verify Documents</button></td>
                        </tr>
                        <tr>
                            <td colspan="7" class="hiddenRow p-0">
                                <div id="accordion" class="row collapse">
                                    <div class="col-md-4 p-3">
                                        <img src="{{ url('assets/images/adhar-card.jpg') }}" class="img-fluid">
                                        <h6>Addhar Card</h6>
                                    </div>
                                    <div class="col-md-4 p-3">
                                        <img src="{{ url('assets/images/adhar-card.jpg') }}" class="img-fluid">
                                        <h6>Addhar Card</h6>
                                    </div>
                                    <div class="col-md-4 p-3">
                                        <img src="{{ url('assets/images/adhar-card.jpg') }}" class="img-fluid">
                                        <h6>Addhar Card</h6>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>256563656</td>
                            <td>Surya</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View / Verify Documents</a></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>256563656</td>
                            <td>Surya</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td>Yes / No</td>
                            <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View / Verify Documents</a></td>
                        </tr>
                    </tbody>
                </table>                       
            </div>
        </div>
    </div>
</section>
@endsection