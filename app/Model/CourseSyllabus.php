<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseSyllabus extends Model
{
    protected $table = 'course_syllabus';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['course_link_id', 'syllabus_name', 'syllabus_status'];

    public function courseSyllabus(){
		
         return $this->belongsTo('App\Model\LinkCourseSchool','course_link_id','id');
    }
}
