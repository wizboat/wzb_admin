<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LinkCourseSchool extends Model
{
    protected $table = 'link_course_school';
    protected $hidden = [];
    protected $fillable = ['user_id','course_master_id','course_status'];

    public function course(){

         return $this->belongsTo('App\Model\CourseMaster','course_master_id');
    }

    public function courseDetails(){
        
        return $this->hasMany('App\Model\CourseDetails','course_link_id', 'id');
    }
}