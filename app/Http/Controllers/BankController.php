<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Bank;
use App\Model\SchoolBank;
use Session;
use Auth;
use DB;

class BankController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public function index(){      
    return view('school.bank.index');
  }

  public function list(){
    
    $result =  DB::table('school_bank')
    ->select('school_bank.id as id', 'bank.bank_name as bank_name','school_bank.bank_ifsc as bank_ifsc','school_bank.bank_branch as bank_branch','school_bank.account_number as account_number' ,'school_bank.bank_status as bank_status' )
    ->join('bank','bank.id','=','school_bank.bank_id')
    ->where('school_bank.school_id',Auth::user()->id)
    ->get()->toArray();
    
    return response()->json($result);
  }    

  public function create(){
    $bank = Bank::all();
    return view('school.bank.create',['bank'=>$bank]);
  }

  public function store(Request $request){
    
    $post = $request->all();
    $validator = \Validator::make($post, ['_token'=>'required','bank_name'=>'required','bank_ifsc'=>'required','bank_branch'=>'required','account_number'=>'required']);
    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator);        
    }

    $bank = Bank::where('id',$post['bank_name'])->first();
    if(!empty($bank)){
      $schoolBank = new SchoolBank();
      $schoolBank->bank_id = $post['bank_name'];
      $schoolBank->school_id = Auth::user()->id;
      $schoolBank->bank_ifsc = $post['bank_ifsc'];
      $schoolBank->bank_branch = $post['bank_branch'];
      $schoolBank->account_number = $post['account_number'];
      $schoolBank->created_by = Auth::user()->id;
      $schoolBank->save();

      if(isset($schoolBank->id)){
        Session::flash('success_message', 'Bank created successfully');
        return redirect('bank');
      }else{
        Session::flash('error_message', 'Something went wrong');
        return redirect('bank');
      }
    }else{
      Session::flash('error_message', 'Something went wrong');
      return redirect('bank');
    }       
  }

  public function edit(Request $request,$id){
    $post = $request->all();
    $bank = Bank::all();
    $bankData = SchoolBank::where('id',$id)->first();
    if(isset($bankData)){
      return view('school.bank.edit',['bank'=>$bank,'bankData'=>$bankData]);  
    }else{
      return redirect('bank');
    }       
  }

  public function update(Request $request,$id){
    $post = $request->all();          
    $validator = \Validator::make($post, ['_token'=>'required','bank_name'=>'required','bank_ifsc'=>'required','bank_branch'=>'required','account_number'=>'required','bank_status'=>'required']);
    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator);        
    }

    $bank = Bank::where('id',$post['bank_name'])->first();
    if(!empty($bank))
    {
      $schoolBank = SchoolBank::where('id',$id)->first();
      if(isset($schoolBank) && ($post['bank_status']==1  || $post['bank_status']==0))
      {
        $schoolBank->bank_id = $post['bank_name'];
        $schoolBank->school_id = Auth::user()->id;
        $schoolBank->bank_ifsc = $post['bank_ifsc'];
        $schoolBank->bank_branch = $post['bank_branch'];
        $schoolBank->account_number = $post['account_number'];            
        $schoolBank->bank_status = $post['bank_status'];
        $schoolBank->save();

        if(isset($schoolBank->id)){
          Session::flash('success_message', 'Bank updated successfully');
          return redirect('bank');
        }else{
          Session::flash('error_message', 'Something went wrong');
          return redirect('bank');
        }
      }else{
        Session::flash('error_message', 'Something went wrong');
        return redirect('bank');
      }         
    }else{
      Session::flash('error_message', 'Something went wrong');
      return redirect('bank');
    }
  }
}
