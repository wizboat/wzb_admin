@extends('school.settings.app')

@section('content')
<section class="content">           
  <div class="heading_main">
    <div class="pull-right">
      <button type="button" class="btn btn-outline-primary editBtn">Edit
      </button>
    </div>
    <div class="mdc-top-app-bar__title">General Settings</div>
  </div>
  <div class="card">
    <div class="card-body">
      <ul class="genset_main pt-0">
        <li class="genset_heading">School Name </li>
        <li class="genset_desc"> {{$result->schoolprofile->school_name}}</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Board/University </li>
        <li class="genset_desc">{{$result->user_name}}</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Board/University </li>
        <li class="genset_desc">Pune University</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Address </li>
        <li class="genset_desc">250, Mumbai-Pune Express way,Kothrud,Pune 521252</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">City </li>
        <li class="genset_desc">Pune</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">State </li>
        <li class="genset_desc">Maharashtra</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Year founded </li>
        <li class="genset_desc">2012</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Website </li>
        <li class="genset_desc">http://www.sinhgadedu.edu</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Phone </li>
        <li class="genset_desc">022-565233,  022-25652336,  022-5255265</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">Email id </li>
        <li class="genset_desc">info@sinhgad.edu</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">School Name </li>
        <li class="genset_desc">Sinhgad Institute Of Management and Technology (Pune)</li>
      </ul>
      <ul class="genset_main">
        <li class="genset_heading">School Name </li>
        <li class="genset_desc">Sinhgad Institute Of Management and Technology (Pune)</li>
      </ul>
    </div>
  </div>
</section> 
<script type="text/javascript">
  $(document).ready(function()
  {
   $(".general-elem-btn").click(function(event){  
    event.stopPropagation();
    var token = '{{ csrf_token() }}';   							
    $.ajax({
     type: "POST",
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     data:{'_token':token,'_c2':'3'},
     url: "{{ url('settings/general/edit') }}",					
     success: function (result){    
      console.log(result);
      if(result.code==200){
       $(".edit-general-info").html(result.html);   
       $(".g-e-b").html('');   
					// window.location.reload(4000);
				}else{
					$(".model-danger").show();
					// window.location.reload(4000);                                          
				}
			}
		});				
  });

   $(".row").click(function(event){    
    event.stopPropagation();     
    var form = $("[name=general__i]");
    form.validate({
      submitHandler: function()
      {
        formdata=$("#general__i").serialize();
        $.ajax({
         type: "POST",
         headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ url('settings/general/update') }}",
        data: formdata,
        success: function (result){    
          if(result.code==200){                     
           $(".edit-general-info").html(result.html);                                                                                 
         }else{
           $(".model-danger").show();
                     // window.location.reload(4000);                                          
                   }
                 }
               });
      } 
    });
  });
 });
</script>
@endsection