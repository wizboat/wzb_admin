<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
   	protected $table = 'payment_transaction';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['user_id', 'ref_number', 'user_type' ,'description','transaction_status','transaction_type','amount'];
}
