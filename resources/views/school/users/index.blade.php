@extends('layouts.app')

@section('title', '| Users')

@section('content')
<section class="content">                                                     
        <div class="">
            <div class="mdc-top-app-bar__title">Users List</div>
            @if(Session::get('success_message'))
                <div class="alert alert-success" role="alert"><strong>Success!</strong>&nbsp;&nbsp;{{Session::get('success_message')}}</div>
            @endif 
            @if(Session::get('error_message'))
                <div class="alert alert-success" role="alert">{{Session::get('success_message')}}</div>
            @endif 
            <div class="card mb-5">
               <div class="card-body"><!-- 
                    <div class="">                   
                    <i class="fa fa-users" aria-hidden="true"></i>
                     <span class="card-title"><strong>Users List</strong></span>                             
                    </div> -->
                    <div class="card-body p-0">  
                        <div class="">
                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm pull-left"><i class="fa fa-plus pr-2"></i>Add User</a>
                            <a href="{{ route('roles.index') }}" class="btn btn-default btn-sm pull-right"><i class="fa fa-users pr-2"></i>Roles</a>
                            <a href="{{ route('permissions.index') }}" class="btn btn-warning btn-sm mr-2 pull-right"><i class="fa fa-shield pr-2"></i>Permissions</a>                        
                            <div class="table-responsive">                            
                                <table class="table table-bordered table-hover mt-3">
                        
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Date/Time Added</th>
                                            <th>User Roles</th>
                                            <th>Operations</th>
                                        </tr>
                                    </thead>
                                    <tbody>         
                                        @if(!empty($usersdata))
                                        @foreach ($usersdata as $value)
                                            @foreach ($users as $user)
                                                @if($value['userdata']['id']==$user->id)
                                                <tr>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                                    <td><span class="badge badge-info ml-2">{{  $user->roles()->pluck('name')->implode(' , ') }}</span></td>
                                                    <td style="width: 157px;">
                                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-outline-grey btn-sm pull-left mr-2 m-0"><i class="fa fa-pencil"></i></a>

                                                    <!-- {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id] ]) !!}
                                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm pull-right']) !!}
                                                    {!! Form::close() !!} -->
                                                    @if($user->user_status==1)
                                                      <button type="button" class="btn btn-outline-danger  btn-sm user-status m-0" data-toggle="modal" data-user-hash="{{$user->user_hash}}" data-user-name="{{ $user->email }}" data-target="#statusUserModal"> <i class="fa fa-times" aria-hidden="true"></i></button>
                                                    @else

                                                      <button type="button" class="btn btn-outline-success  btn-sm user-status m-0" data-user-hash="{{$user->user_hash}}" data-user-name="{{ $user->email }}" data-toggle="modal" data-target="#statusUserModal">  <i class="fa fa-check" aria-hidden="true"></i>  </button>
                                                      @endif

                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5">
                                                    <center>
                                                        <div class="alert alert-warning mt-4" role="alert"><strong>No User created yet!</strong></div>
                                                    </center>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>    
</section>
<div class="modal" id="statusUserModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>                  
            <form name="userUpdateForm" id="userUpdateForm" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="alert model-message" role="alert" style="display: none;">     
                </div>            
                    <label>Are you sure want to change status of user&nbsp;<strong><span class="user-name">&nbsp;</span></strong>?</label>                     
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_hash" class="user-hash">                    
                </div>            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm user-status-btn">Submit</button>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $(".user-status").click(function(){        
        var userHash = $(this).data("user-hash");
        var userName = $(this).data("user-name");
        $(".user-hash").val(userHash);     
        $(".user-name").text(userName);     
   });  

   $(".user-status-btn").click(function(){           
      var form = $("[name=userUpdateForm]");
      form.validate({
        ignore: "",        
        submitHandler: function()
        {
            formdata=$("#userUpdateForm").serialize();
            $.ajax({
               type: "POST",
               url: "{{ url('users/status') }}",
               data: formdata,
               success: function (result){    
                  if(result.code=='200'){                     
                    $(".model-message").show();            
                    $(".model-message").text(result.message);                                      
                    $(".model-message").addClass('alert-success');                                      
                     window.location.reload(4000);
                  }else{
                    $(".model-message").show();
                    $(".model-message").text(result.message);                                      
                    $(".model-message").addClass('alert-danger'); 
                    window.location.reload(4000);                                      
                  }
               }
            });
         } 
      });
   }); 
});
</script>
@endsection