@extends('layouts.app')

@section('title', '| Edit User')

@section('content')
<section class="content"> 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
           <div class="card my-5">
                <div class="card-header p-3">                   
                
                 <span class="card-title"><i class="fa fa-user-plus pr-2" aria-hidden="true"></i>{{$user->name}}</span>                             
                </div>
                <div class="card-body p-0 ">  
                    <div class="col-lg-12 mt-3 mb-3">
                        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}
                            <div class="md-form form-group">
                                {{ Form::label('name', 'Name') }}
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="md-form form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::email('email', null, array('class' => 'form-control')) }}
                            </div>
                            <h5><b>Give Role</b></h5>
                            <div class='form-group'>
                                @foreach ($roles as $role)
                                    {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                                    {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                                @endforeach
                            </div>
                            <!-- <div class="form-group">
                                {{ Form::label('password', 'Password') }}<br>
                                {{ Form::password('password', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('password', 'Confirm Password') }}<br>
                                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                            </div> -->
                            <div class="form-group pb-2">
                                {{ Form::submit('Update', array('class' => 'btn btn-primary btn-sm mr-2 m-0')) }}
                                <a href="{{ url('/users')}}" class="btn btn-outline-secondary btn-sm m-0">Back</a>
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- 
<section class="content mt-5" > 
<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
    <hr>

    

</div>
</section> -->
@endsection