@extends('layouts.app')

@section('content')
<div class="main-panel">
    <section class="content"> 
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="mdc-top-app-bar__title">Courses</div>
                <!-- <h5 class="card-title">Courses</h5> -->
            </div>
            <div class="alert-message"></div>
            @foreach( $courseMaster as $key=>$value )
            <div class="col-lg-4 col-md-6 col-sm-6 mb-4">
                <div class="card">
                    <div class="coursec card-body">
                        @if($value['course'][0]['course_status'] == '1')
                        <h6  class="course_status float-right" id="courseStatus{{ $value['course'][0]['id'] }}"><span class="badge badge-success">Active</span></h6>
                        @else
                        <h6 class="course_status float-right" id="courseStatus{{ $value['course'][0]['id'] }}"><span class="badge badge-danger">Deactive</span></h6>
                        @endif
                        <h5 class="card-title mb-3"><strong> {{ ucfirst($value['course_name']) }} </strong></h5>                   
                        <div style="border-bottom: 1px solid #e2e2e2; padding: 10px 0px;">
                            <span >Seat Intake</span>
                            <span class="pull-right">Fee</span>
                        </div>

                        <div class="mb-3" style="border-bottom: 1px solid #e2e2e2; padding: 10px 0px;">
                            <span class="seatView{{$value['id']}}">{{ isset($value['course'][0]['course_details'][0]) ? $value['course'][0]['course_details'][0]['seat_intake'] : 'NiL' }}</span>
                            <span class="pull-right  feeView{{$value['id']}}"><i class="fa fa-inr" aria-hidden="true"></i>{{ isset($value['course'][0]['course_details'][0]) ? $value['course'][0]['course_details'][0]['fee'] : 'NiL' }}</span>
                        </div>
                        @php ($id = $value['course'][0]['id'] )

                        <a href='{{ url("/course/$id/details") }}' class="btn btn-primary primaryTable float-right">View Details</a>

                        <!-- <button type="button" class="btn btn-sm btn-outline-secondary float-right mr-3 editButton" data-toggle="modal" data-target="#courseEdit" data-link="{{ base64_encode(base64_encode(base64_encode( $value['course'][0]['id'] ))) }}"  data-hash="{{ base64_encode(base64_encode(base64_encode( $value['id'] ))) }}" data-courseName="{{ ucfirst($value['course_name']) }}">Edit</button>   -->

                        <a href='{{ url("/course/$id/edit") }}' class="btn btn-outline-primary editBtn">Edit</a>

                    </div>
                </div>
            </div>
            @endforeach                                                       
        </div>   
    </section>
</div>
@endsection
