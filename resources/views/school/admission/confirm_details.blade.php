@extends('layouts.app')

@section('content')
<section class="content">                                                     
   <div id="msg"></div>
	<div class="mbdtable">
		<div class="main-panel">
		   <section class="content">
		     	<h2>Confirm Admission List</h2>
		     	 <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter">Print</button>           
		   </section>
		</div>
		<table id="confirm_list" class="table cardmbd table-borderless"></table>
	</div>
</section>
<script type="text/javascript">
	var BS_COLUMNS  = [];

	function indexFormatter( value, row, index ){
	  	return index+1;
	}

	function buttonFormatter( value, row, index ){		
	 	return "<a href="+window.location.href+"?id="+row.admission_open_id+" class='btn btn-outline-primary btnTable delete-gallery'>View List</a>";
	}

	BS_COLUMNS.unshift(
	  	{
	      field       : 'id',
	      title       : 'S.No',
	      align       : 'center' ,
	      formatter   : indexFormatter 
	  	},{
	      field       : 'addmission_refrence',
	      title       : 'Addmission ReferenceID',
	      align       : 'center'
	  	},{
	      field       : 'name',
	      title       : 'Student Name',
	      align       : 'center'
	  	}
	);

	var BS = {
		url             : '{{ url("admission/confirm/d?id=$AdmissionID") }}',
		columns         : BS_COLUMNS,
		pagination      : true,
		search          : true,        
		sidePagination  : 'client',
		PageRefresh     : true,
		pageNumber      : 1,
		pageSize        : 10,
		showRefresh     : true
	};

	var t = $("#confirm_list").bootstrapTable(BS);
</script>
@endsection