<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'faculty';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['school_id','created_by','faculty_name','faculty_image','faculty_degree','deleted','disabled'];    
}
