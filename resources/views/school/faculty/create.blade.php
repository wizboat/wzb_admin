@extends('layouts.app')

@section('title', '| Add User')

@section('content')
<section class="content"> 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
           <div class="card mb-5 mt-5">
                <div class="card-body">  
                    <span class="card-title">
                        <i class="fa fa-user-plus pr-2" aria-hidden="true"></i>
                        <strong>Add Faculty</strong>
                    </span>    
                    <div class="">
                        {{ Form::open(array('url' => 'faculty','files' => true, 'enctype' => 'multipart/form-data')) }}

                            <div class=" md-form form-group{{ $errors->has('faculty_name') ? ' has-error' : '' }}">
                                {{ Form::label('faculty_name', 'Faculty Name') }}
                                {{ Form::text('faculty_name', '', array('class' => 'form-control')) }}
                                @if ($errors->has('faculty_name'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('faculty_name') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="custom-file{{ $errors->has('faculty_image') ? ' has-error' : '' }}">
                                {{ Form::label('faculty_image', 'Choose image',array('class'=>'custom-file-label browserfile')) }}
                                {{ Form::file('faculty_image',array('class' => 'custom-file-input')) }}
                                @if ($errors->has('faculty_image'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('faculty_image') }}</label>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form form-group {{ $errors->has('faculty_degree') ? ' has-error' : '' }}">
                                {{ Form::label('faculty_degree', 'Faculty Degree') }}
                                {{ Form::text('faculty_degree','' ,array('class' => 'form-control')) }}
                                @if ($errors->has('faculty_degree'))
                                    <span class="help-block">
                                        <label>{{ $errors->first('faculty_degree') }}</label>
                                    </span>
                                @endif
                            </div>  
                            <div class="form-group">
                                {{ Form::submit('Add', array('class' => 'btn btn-primary btn-sm mr-2 m-0')) }}
                                <a href="{{ url('/faculty')}}" class="btn btn-secondary btn-sm m-0">Back</a>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection