<?php
  
namespace App\Model;
  
use Auth;  
use App\Model\User;
use App\Model\Enrollment;

use Maatwebsite\Excel\Concerns\ToModel;
  
class EnrollmentImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $userId = Auth::user()->id;

        return new Enrollment([
            'school_id'     => $userId,
            'enrollment_number' => $row[0], 
            'dob' => $row[1],
        ]);
    }
}