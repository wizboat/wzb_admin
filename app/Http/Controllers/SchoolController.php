<?php   
namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\TempSchoolProfile;
use App\Model\User;
use Session;
use DB;

class SchoolController extends Controller
{
   public function __construct(){

   	$this->middleware('auth');
   }
   
   public function index(){


      $userId = Auth::user()->id;  

      $schoolProfile = DB::table('temp_school_profile')
         ->join('users', 'temp_school_profile.user_id', '=', 'users.id')
         ->where('users.id',$userId)
         ->first();       

      return view('temp_school_profile',compact('schoolProfile'));
    }

   public function pendingSchoolIndex(){

      $userId = Auth::user()->id;
      
      $q = TempSchoolprofile::query();

      $q->whereHas('user', function($query) use ($userId) {
         $query->where('id', $userId);
         $query->where('user_status', getenv('STATUS_11'));
      });
      $q->with('state');

      $schoolData = $q->get()->toArray();

      return view('school.school_registration.index',compact('schoolData'));
   }

   public function pendingSchoolView(Request $request,$id){

      $userId = Auth::user()->id;
      
      $schoolData = TempSchoolProfile::with('state')->where('id',$id)->first()->toArray();
      return view('school.school_registration.view',compact('schoolData'));
   }

   public function activeSchoolIndex(){
      $userId = Auth::user()->id;
      $q = TempSchoolprofile::query();
      $q->whereHas('user', function($query) use ($userId) {
         $query->where('id', $userId);
         $query->where('user_status', getenv('STATUS_22'));
      });
      $q->with('state');
      $schoolData = $q->get()->toArray();
      return view('school.school_active.index',compact('schoolData'));
   }

   public function activeSchoolView(Request $request,$id){
      $userId = Auth::user()->id;
      $schoolData = TempSchoolProfile::with('state')->where('id',$id)->first()->toArray();
      return view('school.school_active.view',compact('schoolData'));
   }

   public function schoolApprove(Request $request){

      $userId = Auth::user()->id;

      $post = $request->all();     

      $data = User::where('id',$post['user_id'])->first();        

      $data->user_status = getenv('STATUS_22');

      $data->save();

      return redirect('pending-school')->with('success','School approved successfully')   ;

   }

}
