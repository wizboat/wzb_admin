@extends('layouts.app')

<!-- @section('title', '| Users') -->

@section('content')
<div class="main-panel">
  <section class="content"> 
    <div class="paymentmain_sec">
      @if(Session::get('success_message'))
        <div class="alert alert-success" role="alert"><strong>Success!</strong>&nbsp;&nbsp;{{Session::get('success_message')}}</div>
      @endif 
      @if(Session::get('error_message'))
        <div class="alert alert-danger" role="alert">{{Session::get('error_message')}}</div>
      @endif 
        <div class="mdc-top-app-bar__title">Payments</div>
          <div class="paymentinner">
            <div class="mb-3">     
              <div class="card p-4" style="display: block;">                                 
                <span class="box-title">Total earnings (2019)</span>       
                <div class="box-title pull-right">
                  <h4><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;0.00</h4>
                </div>
              </div>
            </div>

            <div class="mb-3">     
              <div class="card p-4">                                                             
                <h5 class="proftable_title mb-3">Transactions</h5>
                <table class="table table-bordered table-hover table-responsive-lg">
                  <thead>
                    <tr>
                      <th scope="col">Reference Number</th>
                      <th scope="col">Status</th>
                      <th scope="col">Amount (<i class="fa fa-inr" aria-hidden="true"></i>)</th>
                      <th scope="col">Description</th>
                      <th scope="col">Type</th>     
                      <th scope="col">Transcation Date</th>     
                      <th></th>                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td scope="row">BRY89997TYU</td>
                      <td><span class="badge badge-success">Active</span></td>
                      <td>5265.00</td>
                      <td>Application Kit</td>
                      <td>Debit</td>
                      <td>02-11-2018</td>
                      <td><a href="{{ url('/') }}" class="btn btn-outline-primary btn-sm">View</a></td>
                    </tr>                                             
                  </tbody>
                </table>                       
              </div>
            </div>

            <div class="">     
              <div class="card p-4" style="display: block;">   
                <span class="proftable_title mt-2">Banks details</span>
                <span class="pull-right mb-3"><a href="{{ url('bank/create')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus pr-2"></i> Add Bank</a></span>
                <table class="table table-bordered table-hover table-responsive-lg">
                  <thead>
                    <tr>
                    <th scope="col">Sr.No</th>
                    <th scope="col">Accoount Number</th>
                    <th scope="col">Bank Name</th>
                    <th scope="col">IFSC Code</th>
                    <th scope="col">Branch</th>     
                    <th scope="col">Status</th>       
                    <th></th>                    
                    </tr>
                  </thead>
                <tbody>
                @foreach($bankData as $key=>$value)   
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $value['account_number'] }}</td>
                    <td>{{ $value->bank->bank_name }}</td>
                    <td>{{ $value['bank_ifsc'] }}</td>
                    <td>{{ $value['bank_branch'] }}</td>
                    <td>
                    @if($value['bank_status']==1)
                    <span class="badge badge-success">Active</span>
                    @else
                    <span class="badge badge-danger">Inactive</span>
                    @endif
                    </td>
                    <td><a href="{{ route('bank.edit',$value['id'] ) }}" class="btn btn-outline-primary btn-sm">Edit</a></td>
                  </tr>
                @endforeach                          
                </tbody>
                </table>       

              {{ $bankData->links() }}       
              </div>
            </div>
          </div>           
    </div>
  </section>
</div>
@endsection