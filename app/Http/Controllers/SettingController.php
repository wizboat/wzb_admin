<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Auth;
use Jenssegers\Agent\Agent;
use App\Model\UserLocationAgent;
use DB;
use Session;

class SettingController extends Controller
{
    public function __construct(){
      	$this->middleware('auth');
    }

    public function index(){    
    	$userId = Auth::user()->id;
    	$result = User::with('schoolprofile')->where('id',$userId)->first();  	
   		return view('school.settings.account',compact('result'));   		
    }

    public function generalSettingEdit(Request $request)
    {
    	$post = $request->all();
    	$userId = Auth::user()->id;
    	$result = User::with('schoolprofile')->where('id',$userId)->first();
 		$html = "<form id='general__i' name='general__i' enctype='multipart/form-data'>  
	    			<table class='table border-top-0'>						  
						<tbody>
							<tr>
								<th style='width: 164px;''>School Name</th>
								<td align='left'>
									<input type='text' class='form-control' name='school_name' value='".$result->schoolprofile->school_name."'>
								</td>
							</tr>
							<tr>
								<th scope='row'>User Name</th>
								<td><input type='text' class='form-control' name='user_name' value='".$result->user_name."'></td>
							</tr>
							<tr>
								<th scope='row'>Mobile Number</th>
								<td><input type='text' name='mobile' class='form-control'></td>
							</tr>
							<tr>							
								<td colspan='2'>
									<button type='submit' class='btn btn-primary btn-sm fff_ww'>Change Update</button>
									<button class='btn btn btn-outline-secondary btn-sm'>cancel</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>";

    	 return response()->json([
                    'code'=>200,
                    'status' => true,                    
                    'html'=>$html
        ]); 
    	
    }

    public function generalSettingUpdate(Request $request)
    {
    	$post = $request->all();
    	$userId = Auth::user()->id;

    	$userData = User::find($userId);

    	$user = $userData->schoolprofile()
		  ->update([
		      'school_name'=> $request->school_name
		  ]);

		 $userDetail = User::where('id', $userId)
		  ->update([
		      'user_name'=> $request->user_name
		  ]);  
    	
    	$html = "<table class='table border-top-0'>
					<tbody>
						<tr>
							<th>School Name</th>
							<td align='left'>{$post['school_name']}</td>
						</tr>
						<tr>
							<th scope='row'>User Name</th>
							<td>{$post['user_name']}</td>
						</tr>							
					</tbody>
				</table>";

		return response()->json([
                    'code'=>200,
                    'status' => true,                    
                    'html'=>$html
        ]);			    	
    }

    public function securityIndex()
    {
    	// echo session()->getId(); exit;
    	
    	// $users = User::all();

    	// $userLocationAgentData = UserLocationAgent::where('user_id',Auth::user()->id)->orderBy('_id','desc')->limit(10)->get();

    	// $ch = curl_init ();

     //  // set URL and other appropriate options
     //  curl_setopt ($ch, CURLOPT_URL, "http://ipecho.net/plain");
     //  curl_setopt ($ch, CURLOPT_HEADER, 0);
     //  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

     //  // grab URL and pass it to the browser

     //  $ip = curl_exec ($ch);
     //  // echo "The public ip for this server is: $ip";
     //  // close cURL resource, and free up system resources
     //  curl_close ($ch);
      
    	// print_r($userLocationAgentData);
    	// return view('school.settings.security_index',compact('userLocationAgentData','users'));
        return view('school.settings.security_index');
    	
    }

    public function privacyIndex(){
    	return view('school.settings.privacy');
    }

    public function notifications(){
    	return view('school.settings.notifications');	
    }
}
