@extends('layouts.app')

@section('title', '| Roles')

@section('content')
<section class="content"> 
    <div class="row">                                                      
        <div class="offset-3 col-md-5">
            <div class="card my-5">
                <div class="card-body">
                    <div class="card-title mb-2"><i class="fa fa-key pr-2" aria-hidden="true"></i><strong>Roles</strong></div>  
                    <div class="">  
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th>Permissions</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($roles as $role)
                                        <tr>
                                            <td>{{ $role->name }}</td>
                                            <td>{{ str_replace(array('[',']', '"'),' ', $role->permissions()->pluck('name')) }}</td>                             
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <a href="{{ url()->previous() }}" class="btn btn-sm btn-outline-secondary m-0">Back</a> 
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
<!-- 

<section class="content mt-5" > 
<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-key"></i>&nbsp;Roles
    <a href="{{ route('users.index') }}" class="btn btn-primary btn-sm pull-right">Users</a>
    <a href="{{ route('permissions.index') }}" class="btn btn-primary btn-sm pull-right">Permissions</a></h1>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                <tr>
                    <td>{{ $role->name }}</td>
                    <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td>
                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>

</div>
</section> -->
@endsection