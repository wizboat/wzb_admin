@extends('layouts.app')

@section('content')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
 tinymce.init({
   selector: '#aboutusId'
 });
</script>
<style type="text/css">
	.overlay::before {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 71px;
    opacity: 0.5;
    background: #000;
    content: "";
    -webkit-transition: all 0.4s ease;
    -moz-transition: all 0.4s ease;
    transition: all 0.4s ease;
}
.form-group label {
	position: inherit;
	/*color: red;*/
}
</style>
<div class="main-panel">
<section class="content">
  <div class="profilesec_inner">
  		<div id="toast-container" class="md-toast-bottom-right" aria-live="polite" role="alert"  style="display: none;">
         <div class="md-toast md-toast-info">
             <div class="md-toast-message"></div>
         </div>
     </div>                   
		<div class="card mb-3">
			<div class="prof_name">              
				<div class="prof_img">
					<div class="imgicon" data-toggle="modal" data-target="#bannerUpdate">
						<i class="material-icons imgset">camera_alt</i>            
					</div>
					<img class="pic-cropper__target-image" id="ember1409-target-image" src="{{ url($schoolProfile->school_banner) }}" alt="Profile Banner" draggable="false">  
				</div>
				<div class="card-body">
					<div class="prof_icon">
					  <img src="{{ url($schoolProfile->school_logo) }}" class="img-fluid image s_logo" width="150px">
					  <div class="overlay">
					  <div href="#" class="icon" title="User Profile" data-toggle="modal" data-target="#logoUpdate">
					    <i class="material-icons imgset">camera_alt</i>            
					  	<p>Update</p>
					  </div>
					  </div>
					</div>
					<div class="row">
			    		<div class="col-md-10 pr-0">
			      			<div class="prof_desc pb-3 pt-3">
								<h5 class="school-title school_name">{{ $schoolProfile->school_name }}</h5>
								<p><span class="address1">{{ $schoolProfile->address1 }}, </span><span class="address2">{{ $schoolProfile->address2 }}</span></p>
								<p><span class="school_city">{{ ucfirst($schoolProfile->city) }}</span> , <span class="state">{{ $schoolProfile->stateName->state }}</span> - <span class="pincode">{{ $schoolProfile->pincode }}</span></p>
							</div>

			    		</div>
			    		<div class="col-md-2 pt-4 pl-0">		
			    		<a href="" class="btn btn-primary btnTable">View as Profile</a>	      			
			    		</div>
			    	</div>			    	        
				</div>
			</div>
		</div>  
		<div class="card overview_inner mb-3">
	    <div class="card-body over_sec">
	    	<div class="row">
	    		<div class="col-md-6">
	    			<div class="mdc-top-app-bar__title">About Us</div>	      			
	    		</div>
	    		<div class="col-md-6">
	      		<div class="pencil"><i class="fa fa-pencil" id="aboutusm" data-toggle="modal" data-target="#aboutUs" data-id="{{Auth::user()->id}}"></i></div>
	    		</div>
	    	</div>
	    	<span id="school_about"></span>
	    </div>                          
	  </div>
	  <div class="card over_sec">
	  <div class="card-body">
	  	<div class="row">
    		<div class="col-md-6">
    			<div class="mdc-top-app-bar__title">School Details</div>
    		</div>
    		<div class="col-md-6">
      		<div class="pencil"><i class="fa fa-pencil" data-toggle="modal" data-target="#schoolDetails" id="detail_e"></i></div>
    		</div>
    	</div>		
	     <div class="row">
	        <div class="col-md-12">
	          <div class="form-group">
	            <h6 class="card-title"><strong>School Name :</strong></h6>
	            <p class="school_name s_n">{{ $schoolProfile->school_name }}</p>
	          </div>
	        </div>
	        <div class="col-md-12">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Address :</strong></h6>	            
	            <p><span class="address1 a1">{{ $schoolProfile->address1 }}</span>&nbsp;&nbsp;<span class="address2 a2">{{ $schoolProfile->address2 }}</span></p>
	          </div>
	        </div>
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Board/University :</strong></h6>
	            <p class="school_board">{{ $schoolProfile->board }}</p>
	          </div>
	        </div>
	       	<div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Year founded :</strong></h6>
	            <p class="school_year_founded y_f">{{ $schoolProfile->year_founded }}</p>
	          </div>
	        </div>	        
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>City :</strong></h6>
	            <p class="school_city ct">{{ $schoolProfile->city }}</p>
	          </div>
	        </div>  
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>State :</strong></h6>
	            <p class="school_state st" data-state="{{ $schoolProfile->stateName->id }}">{{ $schoolProfile->stateName->state }}</p>
	          </div>
	        </div>   
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Pin Code :</strong></h6>
	            <p class="school_pincode pn">{{ $schoolProfile->pincode }}</p>
	          </div>
	        </div>
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Website :</strong></h6>
	            <p class="school_website">{{ $schoolProfile->website }}</p>
	          </div>
	        </div>  
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Phone1 :</strong></h6>
	            <p><span class="phone1">{{ $schoolProfile->phone1 }}</span></p>
	          </div>
	        </div> 
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Phone2 :</strong></h6>
	            <p><span class="phone1">{{ $schoolProfile->phone2 }}</span></p>
	          </div>
	        </div> 
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Phone3 :</strong></h6>
	            <p><span class="phone1">{{ $schoolProfile->phone3 }}</span></p>
	          </div>
	        </div> 
	        <div class="col-md-6">
	          <div class="form-group">
	            <h6 class="card-title"><strong>Email id :</strong></h6>
	            <p class="school_email">{{ $schoolProfile->school_email }}</p>
	          </div>
	        </div> 
	      </div>
	    </div>
	  </div>
  </div>  
</section>  
</div>  
<!-- School Banner Update -->
<div class="modal fade" id="bannerUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="card-body modal-content">
         <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="head-md mb-2" id="exampleModalLabel">Update Banner Picture</h5>
         </div>
         <form id="banner_form" name="banner_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="md-form form-group my-0">
               <p for="upload_image" class="custom-file-label browserfile">Upload Photo</p>
               <input class="custom-file-input form-control" name="upload_image" type="file" id="upload_image">          
            </div>
            <div class="text-right mt-3">
               <input type="submit" class="btn btn-primary ban_btn" value="Save changes">
               <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- School Logo Update -->
<div class="modal fade" id="logoUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="card-body modal-content">
         <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="head-md mb-2" id="exampleModalLabel">Update School Logo</h5>
         </div>
         <form id="logo_form" name="logo_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="md-form form-group my-0">
               <p for="upload_image1" class="custom-file-label browserfile">Upload Photo</p>
               <input class="custom-file-input form-control" name="upload_image1" type="file" id="upload_image1">          
            </div>
            <div class="text-right mt-3">
               <input type="submit" class="btn btn-primary lo_btn" value="Save changes">
               <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Popup About Us -->
<div class="modal fade" id="aboutUs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="card-body modal-content">
			<form name='aboutUsForm' id='aboutUsForm' action='#' method='post'>
				<div class="">
					<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="mdc-top-app-bar__title">Edit About Us</div>				
				</div>
				<div class="">
					<div class="form-group form-group mt-0">						
						 <textarea id="aboutusId" name="aboutus">{{ $schoolProfile->about }}</textarea>
						 <input type="hidden" name="id" id="profile_n">
					</div>
				</div>
				<div class="text-right mt-3">
					<button type="button" class="btn btn-outline-primary btnTable m-0" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary btnTable pull-right __aer_" value="Update">  				
				</div>
			</form>
		</div>
	</div>
</div> 
<div class="modal fade" id="schoolDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="card-body modal-content">
			<div class="">
				<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>				
				<div class="mdc-top-app-bar__title">Edit School Details</div>
			</div>		
			<form id="schoolDetail" name="schoolDetail" class="form-horizontal" enctype="multipart/form-data">
				<div class="form-group"> 
					<label for="school_name">School Name <font color="red">*</font></label> 
					<input class="form-control mbd-label" name="school_name" id="school_name" type="text">
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="board">Affiliated Board<font color="red">*</font></label> 
							<input id="school_board" type="text" class="form-control mbd-label" name="board" placeholder="Board">										
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="school_year_founded">Year Founded<font color="red">*</font></label> 
							<input id="school_year_founded" type="text" class="form-control mbd-label" name="school_year_founded" placeholder="Year Founded">		
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="address1">Address Line 1<font color="red">*</font></label> 
							<input id="address1" type="text" class="form-control mbd-label" name="address1" placeholder="Address Line 1">										
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="address2">Address Line 2<font color="red">*</font></label> 
							<input id="address2" type="text" class="form-control mbd-label" name="address2" placeholder="Address Line 2">    							
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="school_city">City<font color="red">*</font></label> 
							<input id="school_city" type="text" class="form-control mbd-label  mbd-label" name="city" placeholder="City">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="school_name">State<font color="red">*</font></label> 
							<select class="form-control mbd-select" id="state" name="state">
								<option value="">------Select State------</option>
								@foreach($state as $value)
								<option value="{{ $value->id}}">{{ $value->state }}</option>
								@endforeach                                           
							</select>							
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                   
							<label for="school_pincode">Pincode<font color="red">*</font></label> 
							<input id="school_pincode" type="text" class="form-control mbd-label" name="pincode" placeholder="Pincode">							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="phone1">Phone No. 1<font color="red">*</font></label> 
							<input id="phone1" type="text" class="form-control mbd-label" name="phone1" placeholder="Phone 1">							
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="phone2">Phone No. 2</label> 
							<input id="phone2" type="text" class="form-control mbd-label" name="phone2" placeholder="Phone 2 (optional)">  							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone3">Phone No. 3</label> 
							<input id="phone3" type="text" class="form-control mbd-label" name="phone3" placeholder="Phone 3 (optional)">     							
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6">
						<div class="form-group">                                    
							<label for="school_email">Email Address<font color="red">*</font></label> 
							<input id="school_email" type="text" class="form-control mbd-label" name="school_email" placeholder="School Email">		 							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="school_website">Website</label> 
							<input id="school_website"  type="text" class="form-control mbd-label" name="website" placeholder="http://www.xyz.com(optional)" placeholder="Website"> 
						</div>
					</div>
				</div>
				<div class="text-right mt-3">
					<button type="button" class="btn btn-outline-primary btnTable m-0" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary btnTable pull-right" id="schoolDetailUpdate" value="Update">	
				</div>                   
            </form>
		</div>
	</div>
</div>
<div id="uploadimageModal" class="modal" role="dialog" >
   <div class="modal-dialog  modal-lg modal-dialog-centered">
      <div class="card-body modal-content">
         <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="head-md mb-2" id="exampleModalLabel">Update Banner Picture</h5>
         </div>
         <div class="">
            <div class="text-center">
               <div id="image_demo" style="width:100%;"></div>
            </div>
         </div>
         <div class="text-right mt-3">
            <input type="submit" class="btn btn-primary crop_image" value="Crop & Upload Image">
            <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div id="uploadimageModal1" class="modal" role="dialog" >
   <div class="modal-dialog  modal modal-dialog-centered">
      <div class="card-body modal-content">
         <div class="">
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="head-md mb-2" id="exampleModalLabel1">Update schoo logo</h5>
         </div>
         <div class="">
            <div class="text-center">
               <div id="image_demo1" style="width:100%;"></div>
            </div>
         </div>
         <div class="text-right mt-3">
            <input type="submit" class="btn btn-primary btnTable crop_image1" value="Crop & Upload Image">
            <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">

	//-------------Banner Update---------//
	$image_crop1 = $('#image_demo').croppie({
        enableExif: true,
        mouseWheelZoom: true,
        viewport: {
            width: 760,
            height: 200,
            type:'square'
        },
        boundary:{
            width: 760,
            height: 300
        }
    });

    $('#upload_image').on('change', function(){
        var reader1 = new FileReader();
        reader1.onload = function (event) {
            $image_crop1.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
    	reader1.readAsDataURL(this.files[0]);
        	$('#uploadimageModal').modal('show');
    	});

    	$('.crop_image').click(function(event){
        	$image_crop1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        	}).then(function(response){        

     			var successFun = function (data){
	            var result = JSON.parse(data);  
	            console.log(result);               
 $('#ember1409-target-image').attr('src', result.data);
	            $("#uploadimageModal").modal('hide');
	            $("#bannerUpdate").modal('hide');
	            $("#loader").hide();
	            $("#toast-container").show();
	            $(".md-toast-message").html(result.msg); 
	            setTimeout(function() {
	               $("#toast-container").css('display','none').fadeOut('slow');
	            }, 4000);           
		      }
		      var response = {'image':response};
	      	var url = '{{ url("profile/up/banner") }}'; 
	      	AjaxCall("POST",url,response,true,successFun);
        })
    });

	//-------------Logo Update-----------//
	$image_crop = $('#image_demo1').croppie({
        enableExif: true,
        mouseWheelZoom: true,
        viewport: {
            width: 180,
            height: 180,
            type:'square'
        },
        boundary:{
            width: 180,
            height: 180
        }
    });

    $('#upload_image1').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            $image_crop.croppie('bind', {            	
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
    	reader.readAsDataURL(this.files[0]);
        	$('#uploadimageModal1').modal('show');
    	});

    	$('.crop_image1').click(function(event){

        	$image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        	}).then(function(response){        

     			var successFun = function (data){
	            var result = JSON.parse(data);  
	            console.log(result);           
	            $('.s_logo').attr('src', result.data);    
	            $("#uploadimageModal1").modal('hide');
	            $("#logoUpdate").modal('hide');
	            $("#loader").hide();
	            $("#toast-container").show();
	            $(".md-toast-message").html(result.msg); 
	            setTimeout(function() {
	               $("#toast-container").css('display','none').fadeOut('slow');
	            }, 4000);           
		      }
		      var response = {'image':response};
	      	var url = '{{ url("profile/up/logo") }}'; 
	      	AjaxCall("POST",url,response,true,successFun);
        })
    });



// });
	$("#detail_e").click(function(){
		$("#school_name").val($(".s_n").text());
		$("#school_board").val($(".school_board").text());
		$("#address1").val($(".a1").text());
		$("#address2").val($(".a2").text());
		$("#school_city").val($(".ct").text());
		$("#school_state").val($(".st").text());
		$("#school_pincode").val($(".pn").text());
		$("#school_year_founded").val($(".y_f").text());
		$("#school_website").val($(".school_website").text());
		$("#phone1").val($(".phone1").text());
		$("#phone2").val($(".phone2").text());
		$("#phone3").val($(".phone3").text());
		$("#school_email").val($(".school_email").text());
		$('[name="state"]').val('{{$schoolProfile->state}}');
	});

	$.noConflict();

	$("#school_about").html("{{ htmlspecialchars_decode($schoolProfile->about, ENT_QUOTES) }}");

	$(".ban_btn").on("click",function(){

	var form = $("[name=banner_form]");
   	form.validate({
	      rules: {				
				upload_image: {
				  	required: true
				}
	      },
	      messages:{
				upload_image: {
					required: "Select Image Banner"
				}
	      },
	  });  
 	}); 

	$(".lo_btn").on("click",function(){

	var form = $("[name=logo_form]");
   	form.validate({
	      rules: {				
				upload_image1: {
				  	required: true
				}
	      },
	      messages:{
				upload_image1: {
					required: "Select Image Logo"
				}
	      },
	  });  
 	}); 
	

	$(".__aer_").on("click",function(){

		var form = $("[name=aboutUsForm]");
   	form.validate({
	      rules: {
				aboutus: {
				  	required: true
				},
				id: {
				  	required: true
				}
	      },
	      messages:{
				id: {
					required: "Enter some about Us"
				},
				aboutus: {
					required: "Enter some about Us"
				}
	      },
      	submitHandler: function(form)
      	{
          	formdata=$("#aboutUsForm").serialize();
          	var successFun = function (data){
	            var result = JSON.parse(data);  
	            console.log(result);                         
	            $("#loader").hide();
	            $("#toast-container").show();
	            $(".md-toast-message").html(result.msg); 
	            setTimeout(function() {
	               $("#toast-container").css('display','none').fadeOut('slow');
	            }, 4000);           
		      }

		      var url = '{{ url("profile/update?tab=about_us") }}'; 
		      AjaxCall("POST",url,formdata,true,successFun);
      	}
  		});		
	});

 	$("#schoolDetailUpdate").on("click",function(){

  	var form = $("[name=schoolDetail]");
   form.validate({
      rules: {
          school_name: {
              required: true
          },
          board: {
              required: true
          },
          address1: {
              required: true
          },
          address2: {
            required: true
          },
          city: {
              required: true
          },
          state: {
              required: true
          },
          pincode: {
              required: true
          },
          phone1: {
            required: true
          },
          school_email: {
              required: true
          },
          school_year_founded: {
              required: true
          }
      },
      messages:
      {
			school_name: {
				required: "Please Enter Your School Name."
			},
			board: {
				required: "Please Enter Your Board Name."
			},
			address1: {
				required: "Please Enter Your Address Line 1."
			},
			address2: {
				required: "Please Enter Your Your Address Line 2."
			},
			city: {
				required: "Please Enter Your City."
			},
			state: {
				required: "Please Enter Your State."
			},
			pincode: {
				required: "Please Enter Your Pincode."
			},
			phone1: {
				required: "Please Enter Your Phone Number."
			},
			school_email: {
				required: "Please Enter Your Email."
			}
      },
      submitHandler: function(form)
      {
         formdata=$("#schoolDetail").serialize();
         var successFun = function (data){
         	
            var result = JSON.parse(data);  

            console.log(result);                         
            var dataresult = result.data;
            for(var key in dataresult){
            	$("."+key).text(dataresult[key]);
				  console.log(key + ' - ' + dataresult[key]);
				}
            $("#loader").hide();
            $("#schoolDetails").modal('hide');
            $("#toast-container").show();
            $(".md-toast-message").html(result.msg); 
            setTimeout(function() {
               $("#toast-container").css('display','none').fadeOut('slow');
            }, 4000);           
	      }
	      var url = '{{ url("profile/update?tab=details") }}'; 
	      AjaxCall("POST",url,formdata,true,successFun);
      }
  });
  });
</script>
@endsection