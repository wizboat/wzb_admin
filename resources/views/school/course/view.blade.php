@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
<div class="main-panel">
	<section class="content">
		<div class="mb-3">
			<span class="mdc-top-app-bar__title">Class - I</span>
			<a href="{{ url('/course')}}" class="btn-link float-right"><i class="fa fa-arrow-left pr-2"></i>Back to Course</a>
		</div>
		<div class="card mb-3">
		  	<div class="">
		  <ul class="list-group list-group-flush">

		    	<!-- <li class="list-group-item p-0 pb-2">
		    		<h5 class="proftable_title mb-0">Class - I</h5>
		    	</li> -->

				<div class="row">
					<div class="col-6 pr-0">
						<div class="">
							<h5 class="proftable_title mb-0 border-bottom" style="padding: 12px 20px;">Seat Intake</h5>
							<p class="mb-0" style="padding: 12px 20px;">60</p>      
						</div>
					</div>
					<div class="col-6 pl-0">
						<div class="">
							<h5 class="proftable_title mb-0 border-bottom" style="padding: 12px 20px;">Fee</h5>
							<p class="mb-0" style="padding: 12px 20px;">18,600</p>						
						</div>								
					</div>
				</div>
			</ul>
			</div>
		</div>

		<div class="card mb-3">
		  <ul class="list-group list-group-flush">
		    <li class="list-group-item">
		    	<h5 class="proftable_title mb-0">Eligibility Criteria</h5>
		    </li>
		    <li class="list-group-item">
		    	<small>Dapibus ac facilisis in Dapibus ac facilisis in Dapibus ac facilisis in</small>
		    </li>
		    <li class="list-group-item">
		    	<small>Vestibulum at eros Dapibus ac facilisis in Dapibus ac facilisis in</small>
		    </li>
		  </ul>
		</div>

		<div class="card">
		  <ul class="list-group list-group-flush">
		    <li class="list-group-item">
		    	<h5 class="proftable_title mb-0">Syllabus</h5>
		    </li>
		    <li class="list-group-item">
		    	<small>English</small></li>
		    <li class="list-group-item">
		    	<small>Hindi</small>
		    </li>
		    <li class="list-group-item">
		    	<small>Environment & Science</small>
		    </li>
		    <li class="list-group-item">
		    	<small>Science</small>
		    </li>
		  </ul>
		</div>		

			<!-- <a href="{{ url('/course') }}" class="btn btn-link float-right">Back to Course List</a> -->
	</section>  
</div>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
@endsection