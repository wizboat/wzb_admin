<!DOCTYPE html>
<html>
<title>Wizboat Login</title>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500&amp;lang=en&amp;display=fallback" nonce="ACUEifZ_4E3itaNgpkugZA">
</head>
<body>
  <!--Navbar -->
  <nav class="mb-1 navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand" href="#"><img src="{{ url('assets/images/logo.png') }}" width="120" class="img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
      aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-user"></i> {{ Auth::user()->name }} </a>

          <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
            <a class="dropdown-item" href="#">Change Password</a>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
  </div>
</div>
</nav>
<!--/.Navbar -->

<!-- School Account login -->
<div class="padtp schoolsconfirmation">
  <div class="">
    <div class="container">

      <div class="row py-5 d-flex justify-content-center">
        <div class="col-md-2 pl-5 pl-md-0 pb-5">

          <!-- Stepper -->
          <div class="steps-form-3">
            <div class="steps-row-3 setup-panel-3 d-flex justify-content-between">
              <div class="steps-step-3">
                <a href="#step-5" type="button" class="btn btn-info btn-circle-3 waves-effect ml-0" data-toggle="tooltip" data-placement="top" title="Email Verification"><img src="{{ url('assets/images/email.png') }}"></a>
                <p class="weight-bold">Email Verification</p>
              </div>
              <!-- <div class="steps-step-3">
                <a href="#step-6" type="button" class="btn btn-info btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Basic Information"><img src="{{ url('assets/images/card-payment.png') }}"></a>
              </div> -->
              <div class="steps-step-3">
                <a href="#step-7" type="button" class="btn btn-info btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="Profile Application"><img src="{{ url('assets/images/university.png') }}"></a>
                <p class="weight-bold">School Profile</p>
              </div>
              <div class="steps-step-3">
                <a href="#step-7" type="button" class="btn btn-info btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="Profile Verification"><img src="{{ url('assets/images/review.png') }}"></a>
                <p class="weight-bold">Profile Verification</p>
              </div>
              <div class="steps-step-3 no-height">
                <a href="#step-8" type="button" class="btn btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Course Selections"><img src="{{ url('assets/images/books.png') }}"></a>
                <p>Course Selection</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="profile_review card">
            <div class="card-body text-center">
              <img src="{{ url('assets/images/thankyou.png') }}" class="img-fluid">
              <h2 class="mainheading mb-3">Thank You !</h2>
              <p>Your school profile application has been submitted succussfully. Our team will review your application and we will contact you shortly.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/mdb.min.js') }}"></script>

</body>
</html>