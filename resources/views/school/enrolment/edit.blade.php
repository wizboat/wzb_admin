@extends('layouts.app')

@section('content')

<div class="main-panel">
  <section class="content"> 
    <div class="row">                                                      
      <div class="offset-3 col-md-5">
       <div class="card mb-5 mt-5">
          <div class="card-body">           
            <h5 class="card-title mb-0" style="font-size: 18px;"><i class="fa fa-upload pr-2"></i><strong>Update Enrollment</strong></span>        
            <div class="">  
              <div class="mt-3">
                {{ Form::open(array('url' => 'enrolments/update', 'method'=>'PUT' ,'files' => true, 'enctype' => 'multipart/form-data')) }}
                  <input type="hidden" name="enid" value="{{ $enrollment->id}}">
        					<div class="md-form form-group">
        						<label for="enrollment_no" class="active">Enrollment No.</label>
        						<input type='text' name='enrollment_number' class='form-control' id='enrollment_no' value="{{ $enrollment->enrollment_number }}">
        					</div>
        					<div class="form-group">
        						<label for="dob">Date of Birth</label>
        						<input type='text' name='dob' class='mbd-select  form-control form-control-sm' id='dob' value="{{ $enrollment->dob }}">
        					</div>           
                   <div class="">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary primaryModal')) }}
                    <a href="{{ url('/bank')}}" class="btn closebtn">Back</a>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


@endsection