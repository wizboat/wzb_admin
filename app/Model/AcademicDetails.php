<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AcademicDetails extends Model
{
    protected $table = 'tbl_academic_details';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['tbl_admission_transacition_details_id','examination','school_college','board_university','year_passing', 'marks_type','marks','created_at','updated_at'];    
}
