<?php

namespace App\Http\Middleware;
use App\Models\User;
use Auth;
use Closure;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_status != getenv('STATUS_22')) {
            return redirect('/');
        }
        return $next($request);
    }
}
