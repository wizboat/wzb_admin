<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MeritList extends Model
{
    protected $table = 'tbl_meritlist';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['tbl_admission_open_details_id','tbl_admission_transacition_details_id','rank', 'merit_status']; 
    	
    public function admissionOpenDetails (){
    	return $this->belongsTo('App\Model\AdmissionOpenDetails', 'tbl_admission_open_details_id', 'id');
    } 

    public function admissionTransactionDetails(){
    	return $this->belongsTo('App\Model\AdmissionTransactionDetails', 'tbl_admission_transacition_details_id', 'id');
    }
}