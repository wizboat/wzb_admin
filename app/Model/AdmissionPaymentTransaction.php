<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionPaymentTransaction extends Model
{
	protected $table = 'tbl_admission_payment_transaction';
	protected $hidden = [];
	public $timestamps = true;
	protected $fillable = ['tbl_admission_transacition_details_id', 'TXNID', 'TXNAMOUNT' ,'PAYMENTMODE','CURRENCY','TXNDATE','STATUS','RESPCODE', 'RESPMSG', 'GATEWAYNAME' ,'BANKTXNID','BANKNAME','CHECKSUMHASH','is_reported'];
}
