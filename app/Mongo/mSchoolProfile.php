<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class mSchoolProfile extends Model
{
    protected $connection = 'mongodb';
    protected $collection = "school_profile";
    protected $hidden = [];
    protected $fillable = [ 'schoolName','schoolId','about','board','address1','address2','state','city','pincode','phone1','phone2','phone3','website','pincode','pincode','pincode','courseList'];

}
