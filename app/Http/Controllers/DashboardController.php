<?php   

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\SchoolProfile;
use App\Model\state;
use App\Model\TempSchoolProfile;
use App\Model\SchoolPack;
class DashboardController extends Controller
{
  public function __construct(){

    $this->middleware('auth');
  }

  public function dashboard(){             

    if(!Auth::user()->email_verify){

      return redirect('school/email-verify');

    }else if(!Auth::user()->profile_verify){

      return redirect('school-profile-verify');  

    }else if(!Auth::user()->school_verify){

      return redirect('school-profile-review');

    }else if(!Auth::user()->course_verify){

      return redirect('course-selections');  

    }else{
     return view('home');
   }   

   if(Auth::user()->user_status==getenv('STATUS_11'))
   {
     if(Auth::user()->email_verify===0)
     {
      $userHash = Auth::user()->user_hash;
      $userEmail = Auth::user()->email;            
      $verifyString = "account/verify?token=$userHash&email=$userEmail";
      return view('admin.emailverify',['verifyString'=>$verifyString]);
    }else{

      $userId = Auth::user()->id;
      $schoolData = TempSchoolProfile::where('user_id',$userId)->first();
      $schoolPack = SchoolPack::where('user_id',$userId)->where('status',1)->orderBy('id','desc')->first();


      if(!$schoolData){
       return redirect('school/account/profile');
     }else if(!$schoolPack){
       return redirect('school/account/review');
     }else{               
       return view('processing');               
     }            
   }
 }else if(Auth::user()->user_status==getenv('STATUS_22')){
   $userData  = User::where('id',Auth::user()->id)->with('schoolprofile')->first();                  
   Session::put('school_id', $userData->schoolprofile->id); 
   return view('admin.home');
 }else{


 }         
}
}
