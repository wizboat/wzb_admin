@extends('layouts.app')

@section('content')
<section class="content">                                                     
    <div class="">
        <div id="msg"></div>
        <div class="mbdtable">
          <div class="main-panel">
              <section class="content">
                  <!-- <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus pr-2"></i>Add Gallery</button> -->
                  <div class="mainbar__title">Enrolments</div>
              </section>
          </div>
          <table id="erolltable" class="table cardmbd table-borderless"></table>
      </div>
  </div>
</section>
<script>
    var BS_COLUMNS  = [];

    function indexFormatter( value, row, index ){

        return index+1;
    }

    function editFormatter( value, row, index ){
        
       var url = "{{ url('enrolments/:id/list') }}";
       var urlUpload = "{{ url('enrolments/:id/upload') }}";
       
       url = url.replace(':id', row.id);
       urlUpload = urlUpload.replace(':id', row.id);

       return "<a href="+url+" class='btn btn-outline-primary btnTable'>List Enrollment</a><span><a href="+urlUpload+" class='btn btn-outline-secondary btnTable'>Upload Enrollment</a></span>";
   }

   BS_COLUMNS.unshift(
   {
    field       : 'id',
    title       : 'Sr.No',
    align       : 'center' ,
    formatter   : indexFormatter 
},{
    field       : 'course_name',
    title       : 'Classes',
    align       : 'center' ,
},
{
    field       : '',
    title       : 'Action',
    align       : 'center',
    formatter   : editFormatter 
}
);

   var BS = {
    url             : "{{ url('enrolments/enrollmentList') }}",
    columns         : BS_COLUMNS,
    pagination      : true,
    search          : true,        
    sidePagination  : 'client',
    PageRefresh     : true,
    pageNumber      : 1,
    pageSize        : 10,
    showRefresh     : true
};

var t= $("#erolltable").bootstrapTable(BS);

</script>
@endsection