@extends('layouts.app')
@section('content')
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<section class="content">
	<div class="status_sec">
		<div class="mdc-top-app-bar__title">Admissions Status</div>
		<div class="admsionstatus"> 
		<nav>
		  <div class="nav nav-tabs" id="nav-tab" role="tablist">
			 <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Comming Admissions</a>
			 <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Current Admissions</a>
			 <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Closed Admissions</a>
		  </div>
		</nav>
		<div class="tab-content" id="na6+v-tabContent">
		<!-- Comming Admissions -->
		  <div class="card tab-pane fade arrow-up" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
			<div class="table-responsive-lg mt-3">
				<table class="table table-hover">
					 <thead>
						<tr>
						  <th scope="col">Code</th>
						  <th scope="col">Course Name</th>
						  <th scope="col">Start Date</th>
						  <th scope="col">Last Date</th>
						  <th></th>
						</tr>
					 </thead>
					 <tbody>             
					 @if(isset($admissionData))                   
						@foreach($admissionData as $key=>$value)
						@if($value['start_date'] > date('Y-m-d'))
							<tr>
								<th scope="row">2</th>
								<td>{{ $value['course_name'] }}</td>
								<td>{{ $value['start_date'] }}</td>
								<td>{{ $value['close_date'] }}</td>
								<td>
									<div>   
										<button type="button" class="btn btn-outline-warning btn-sm course-edit-btn" id="course-update-btn" data-toggle="modal" data-target="#editCourseModal">Edit</button>                                                                    
										<button type="button" class="btn btn-outline-danger  btn-sm course-status" data-toggle="modal" data-target="#statusCourseModal"> <i class="fa fa-times" aria-hidden="true"></i>
										</button>                                    
									</div>
								</td>
							</tr>
							 @endif
						@endforeach
						 @endif
					 </tbody>
				  </table>
			</div>
		  </div>
			<!-- Current Admissions -->
		  <div class="card tab-pane fade show active arrow-up arrow-upone" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
			<div class="table-responsive-lg mt-3">
				<table class="table table-hover">
				  <thead>
					 <tr>
						<th scope="col">Code</th>
						<th scope="col">Course Name</th>
						<th scope="col">Start Date</th>
						<th scope="col">Close Date</th>
						<th scope="col"></th>
					 </tr>
				  </thead>
				  <tbody>
					@if(isset($admissionData))
					 @foreach($admissionData as $key=>$value)
					 @if($value['start_date'] <= date('Y-m-d') && $value['close_date'] >= date('Y-m-d'))
						<tr>
						  <th scope="row"></th>
						  <td class="current-course-name">{{ $value['course_name'] }}</td>
						  <td>{{ $value['start_date'] }}</td>
						  <td>
							@if(isset($value['extend_date']) && $value['extend_date']!=null )
								<p>{{ $value['start_date'] }} &nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;{{ $value['extend_date'] }}</p>
								<p class="badge badge-success">{{ $value['admission_comment'] }}</p>
							@else
								{{ $value['close_date'] }} 
							@endif                                    
						</td>
						  <td>
								<button type="button" class="btn btn-outline-primary btn-sm current-btn" data-course-ha="{{ $value['course_hash'] }}" data-course-name="{{ $value['course_name'] }}" data-extend-date="{{ date('d-m-Y',strtotime($value['close_date'])) }}"  data-toggle="modal" data-target="#extendDateCourseModal">Extend Date</button>
						  </td>
						</tr>
						@endif
					@endforeach
					@endif
				  </tbody>
				</table>
			</div>
		  </div>
		  <!-- Closed Admissions -->
		  <div class="card tab-pane fade arrow-up arrow-uptwo" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
			<div class="table-responsive-lg mt-3">
				<table class="table table-hover">
				  <thead>
					 <tr>
						<th scope="col">Code</th>
						<th scope="col">Course Name</th>
						<th scope="col">Start Date</th>
						<th scope="col">Close Date</th>
						<th></th>
					 </tr>
				  </thead>
				  <tbody>
					  @if(isset($admissionData))  
					 @foreach($admissionData as $key=>$value)
					 @if($value['close_date'] < date('Y-m-d'))
						<tr>
						  <th scope="row">2</th>
						  <td>{{ $value['course_name'] }}</td>
						  <td>
								<span>{{ $value['start_date'] }} <i class="fa fa-arrow-right" aria-hidden="true"></i>{{ isset($value['extend_date']) ? $value['extend_date'] : ''}}</span>
							</td>
							<td>
								{{ $value['close_date'] }}
							</td>
							<td>
								 <button type="button" class="btn btn-outline-primary btn-sm current-btn" data-course-ha="{{ $value['course_hash'] }}" data-course-name="{{ $value['course_name'] }}" data-extend-date="{{ date('d-m-Y',strtotime($value['close_date'])) }}"  data-toggle="modal" data-target="#extendDateCourseModal">Extend Date</button>
						  </td>
						</tr>
						 @endif
					@endforeach
					 @endif
				  </tbody>
				</table>
			</div>
		  </div>
		</div>
	 </div>
	</div>
</section> 

<div class="modal" id="extendDateCourseModal">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-header">
				<h6 class="modal-title _c_s_n"></h6>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form name="extendDateForm" id="extendDateForm" enctype="multipart/form-data">
				{!! csrf_field() !!}
				<input type="hidden" name="course_h" class="course-h">            
				<div class="modal-body">
					<div class="form-group model-success" style="display: none;">          
						<div class="alert alert-success">
						 <strong>Success!</strong>&nbsp;Extend date updated successfully.
						</div> 
					</div>
					<div class="form-group model-danger" style="display: none;">          
						<div class="alert alert-danger">
						  <strong>Error</strong>Something went wrong
						</div> 
					</div>
					<div class="form-group">
						<label>Select Date</label>
						<input type="text" id="startdatepicker" class="extend-datepick" name="extend_date"  width="276" readonly="readonly"/>   
						<span class="badge badge-warning mt-3">Please select date more &nbsp;<span class="extend-datepick-label"></span></span>               
					</div>  
					<div class="form-group">
						<label>Admission Comment</label>
						<textarea class="form-control" name="admission_comment"></textarea>
					</div>                               
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-sm admission-extend-date-btn">Create</button>              
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal" id="extendDateCourseModal">
	<div class="modal-dialog">
		<div class="modal-content">      
			<div class="modal-header">
				<h6 class="modal-title _c_s_n"></h6>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form name="extendDateForm" id="extendDateForm" enctype="multipart/form-data">
				{!! csrf_field() !!}
				<input type="hidden" name="course_h" class="course-h">            
				<div class="modal-body">
					<div class="form-group model-success" style="display: none;">          
						<div class="alert alert-success">
						 <strong>Success!</strong>&nbsp;Extend date updated successfully.
						</div> 
					</div>
					<div class="form-group model-danger" style="display: none;">          
						<div class="alert alert-danger">
						  <strong>Error</strong>Something went wrong
						</div> 
					</div>
					<div class="form-group">
						<label>Select Date</label>
						<input type="text" id="startdatepicker" class="extend-datepick" name="extend_date"  width="276" readonly="readonly"/>   
						<span class="badge badge-warning mt-3">Please select date more &nbsp;<span class="extend-datepick-label"></span></span>               
					</div>  
					<div class="form-group">
						<label>Admission Comment</label>
						<textarea class="form-control" name="admission_comment"></textarea>
					</div>                               
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-sm admission-extend-date-btn">Create</button>              
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function()
{
	$(".current-btn").click(function(){
		var courseH   	  = $(this).data("course-ha");   
		var courseName    = $(this).data("course-name");   
		var extendDate    = $(this).data("extend-date");
			 
		$(".extend-datepick").val(extendDate); 
		$(".extend-datepick-label").text(extendDate); 
		$(".course-h").val(courseH); 
		$("._c_s_n").text("Date extend for course "+courseName); 
		
		$('#startdatepicker').datepicker({
			minDate:extendDate,
			format: 'd-m-yyyy',
			uiLibrary: 'bootstrap4'
		});
	});

	$(".admission-extend-date-btn").click(function(){           
		var form = $("[name=extendDateForm]");
		form.validate({
		  submitHandler: function()
		  {
				formdata=$("#extendDateForm").serialize();
				$.ajax({
					type: "POST",
					url: "{{ url('admission/extend-date') }}",
					data: formdata,
					success: function (result){    
						console.log(result);
						if(result.code=='200'){                     
							$(".model-success").show();                                                               
							// window.location.reload(4000);
						}else{
							$(".model-danger").show();
							// window.location.reload(4000);                                          
						}
					}
				});
			} 
		});
	});
});
</script>
@endsection
