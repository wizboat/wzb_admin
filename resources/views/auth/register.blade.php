<!DOCTYPE html>
<html>
<title>Registration</title>
<head>        
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500&amp;lang=en&amp;display=fallback" nonce="ACUEifZ_4E3itaNgpkugZA">
</head>
<body>
<div class="registermain">
    <div class="container">
        <div class="login_sec">
            <img class="login_img img-fluid" src="assets/images/logo-school-account.png" alt="logo" width="120">
            <div class="card login_innersec">
                <div class="card-body">
                    <h5 class="login_header">Create School Account</h5>                    
                    <form method="POST" action="{{ route('register') }}">
                        @csrf 
                        <div class="md-form">
                            <label for="school_name">School name <font color="red">*</font></label>
                            <input type="text" name="school_name" class="form-control{{ $errors->has('school_name') ? ' is-invalid' : '' }}" id="school_name" value="{{ old('school_name') }}">
                            @if ($errors->has('school_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('school_name') }}</strong>
                                </span>
                            @endif              
                        </div>
                        <div class="md-form">
                            <label for="email">E-mail Address <font color="red">*</font></label>
                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif  
                        </div>
                        <div class="md-form">
                            <label for="mobile_no">Mobile Number (Optional)</label>
                            <input type="text" name="mobile_no" class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}" id="mobile_no" value="{{ old('mobile_no') }}">
                            @if ($errors->has('mobile_no'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile_no') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                            <label for="password">Password <font color="red">*</font></label>
                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                            <label for="password-confirm">Confirm Password <font color="red">*</font></label>
                            <input type="password" name="password_confirmation" class="form-control" id="password-confirm">   
                        </div>
                        <span style="font-size: 17px;display: block;line-height: normal;">We will send you a verify code to verify your email id.</span>
                        <div class="md-form">
                            <button class="btn btn-primary btn-block my-4 waves-effect z-depth-0" type="submit">Continue</button>
                        </div>                         
                    </form>
                    <div class="signbtm">                            
                        <p class="mb-0">Already have school account<a href="{{ route('login') }}"> Sign In</a></p>                               
                    </div> 
                </div>
            </div>
            <p class="copyright_regis">Copyright © 2019 All rights reserved | wizboat.com </p>
        </div>
    </div>
</div>
<script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('assets/js/mdb.min.js') }}"></script>
</body>
</html>