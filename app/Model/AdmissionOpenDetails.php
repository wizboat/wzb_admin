<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdmissionOpenDetails extends Model
{
    protected $table = 'tbl_admission_open_details';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['course_link_id','seat_intake','fee','admission_open_date','admission_close_date','admission_status','admission_extend_date','merit_calculation','merit_date','doc_start_date','doc_end_date','admission_confirm_date','session_start_date','admission_comment','admission_status','admission_extend_date'];  

    public function transaction(){
        return $this->hasMany('App\Model\AdmissionTransactionDetails', 'tbl_admission_open_details_id', 'id');
    }

	public function courseLinkDetails(){
        return $this->belongsTo('App\Model\LinkCourseSchool', 'course_link_id', 'id');
    }     
}
