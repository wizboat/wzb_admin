<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Wizboat-Online Admission Platform</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/mdb.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/fontawesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/demo/dashboard_style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/dashboard-styles.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
    <!-- <script type="text/javascript" src="{{ url('assets/js/popper.min.js') }}"></script> -->
    <!-- <script type="text/javascript" src="{{ url('assets/js/moment.min.js') }}"></script> -->
    <!-- <script type="text/javascript" src="{{ url('assets/js/jquery.dataTables.min.js') }}"></script> -->
    <!-- <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script> -->
</head>  
<body>
    <script src="{{ url('assets/js/preloader.js') }}"></script>
    <div id="loader" style="display: none;">
      <img id="loader_image" src="/assets/images/loading.gif">
    </div>
    <div class="body-wrapper">       
        <div class="main-wrapper mdc-drawer-app-content">            
            <div class="page-wrapper mdc-toolbar-fixed-adjust">
                <main class="">
                    <style type="text/css">
                        .admisssiion_inner {
                            padding-top: 10px;
                            padding-left: 24px;
                            text-align: center;
                        }
                        .admisssiion_inner p {
                            margin-bottom: 0;
                            line-height: 24px;
                            font-size: 16px;
                        }
                        .merit_main_title {
                            text-align: center;
                            font-weight: 600;
                            font-size: 20px;
                        }
                        .admisssion_detail {
                            /*margin: 50px auto;*/
                            padding-right: 0;
                            color: #000;
                        }
                        .admisssion_detail_inner {
                            background-color: #fff;                            
                            border: 1px solid #ddd;
                            padding: 45px 50px 45px;                            
                        }
                        .table {
                            color: #000;
                        }
                        span.left_imgs {
                            text-align: center;
                            display: block;
                            width: 100%;
                        }
                        .list-img {
                            width: 70px;
                            height: 70px;
                        }
                        table.table th, table.table td {
                            padding: 0.75rem;
                            font-size: 15px;
                        }
                        table.table td {
                            font-weight: 400;
                        }
                    </style>
                    <section class="padtp">
                        <!-- <div class="container"> -->
                            <div class="admisssion_detail">
                                <div class="admisssion_detail_inner">
                                    <div class="admisnstd_blobk mb-4">
                                        <span class="left_imgs">
                                            <img src="{{ $schoolProfile->school_logo }}" class="list-img">
                                        </span>                               
                                        <div class="admisssiion_inner">                      
                                            <h4 class="school-name mb-1">{{ $schoolProfile->school_name }}</h4>
                                            <p>{{ $schoolProfile->address1 }}, {{ $schoolProfile->address2 }}, <br>{{ $schoolProfile->city }} , {{ $schoolProfile->stateName->state }} - {{ $schoolProfile->pincode }}. Ph. {{ $schoolProfile->pincode }}</p>
                                            <p>e-mail : {{ $schoolProfile->school_email }}</p>
                                            <p>url : {{ $schoolProfile->website }}</p>
                                        </div>
                                    </div>
                                    <div class="admission_inner_card">                      
                                        <h5 class="merit_main_title">{{ $courseDetails->merit_calculation ==1 ? 'Merit List' : 'Admission List'}} </h5> 
                                        <div class="left_merit_list my-3">
                                            <span class="pr-3"><strong>Course Name :</strong>{{ ucfirst($courseDetails->course_name) }}</span> 
                                            <span><strong>Open Seat :</strong> {{ $courseDetails->seat_intake }}</span>
                                            <span class="float-right">
                                                <button class="btn btn-primary primaryTable mr-2 d-inline">Download</button>
                                                <button class="btn btn-primary primaryTable d-inline" onclick="printJS('printJS-form', 'html')">Print</button>
                                            </span>
                                        </div>   
                                        <table class="table table-bordered table-hover text-center" width="100%">
                                            @if($courseDetails->merit_calculation == 1) 
                                            <thead>
                                                <tr>
                                                    <th width="80">Sr. No.</th>
                                                    <th>Admission ID</th>
                                                    <th>Application Name</th>
                                                    <th>Percentage</th>
                                                    <th>Rank</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($meritListDetails) && !empty($meritListDetails) )
                                                @foreach($meritListDetails as $key=>$value)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $value->addmission_refrence }}</td>
                                                    <td>{{ $value->firstname }} {{ $value->lastname }}</td>
                                                    <td>{{ $value->academic_percentage }}</td>
                                                    <td>{{ $key }}</td>
                                                </tr>     
                                                @endforeach    
                                                @else
                                                <tr>
                                                    <td colspan="5">No Records found...</td>
                                                </tr>

                                                @endif                                      
                                            </tbody>
                                            @else
                                                <thead>
                                                <tr>
                                                    <th width="80">Sr. No.</th>
                                                    <th>Admission ID</th>
                                                    <th>Application Name</th>                                              
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($meritListDetails) && !empty($meritListDetails) )
                                                @foreach($meritListDetails as $key=>$value)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $value->addmission_refrence }}</td>
                                                    <td>{{ $value->firstname }} {{ $value->lastname }}</td>                
                                                </tr>     
                                                @endforeach    
                                                @else
                                                <tr>
                                                    <td colspan="3">No Records found...</td>
                                                </tr>

                                                @endif                                      
                                            </tbody>


                                            @endif
                                        </table>                       
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-primary primaryTable mr-2 d-inline">Download</button>
                                        <button class="btn btn-primary primaryTable d-inline" onclick="printJS('printJS-form', 'html')">Print</button>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                    </section>
                </main>
            </div>            
        </div>
    </div>
</body>
</html>




