@extends('layouts.app')

@section('title', '| Add Bank')

@section('content')
<div class="main-panel">
  <section class="content"> 
    <div class="row">                                                      
      <div class="offset-3 col-md-5">
       <div class="card mb-5 mt-5">
          <div class="card-body">                                
            <h5 class="card-title" style="font-size: 18px;"><i class="fa fa-university pr-2"></i><strong>Add Bank</strong></span>        
            <div class="">  
              <div class="mt-3">
                {{ Form::open(array('url' => 'bank','files' => true, 'enctype' => 'multipart/form-data')) }}
                  <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                    <label>Bank Name</label>
                    <select name="bank_name" class="mbd-select form-control pl-0">
                      <option value="">--select--</option>
                      @foreach($bank as $value)
                      <option value="{{ $value->id }}">{{ $value->bank_name }}</option>
                      @endforeach
                    </select>                               
                    @if ($errors->has('bank_name'))
                      <span class="help-block error-label">
                        <label>{{ $errors->first('bank_name') }}</label>
                      </span>
                    @endif
                  </div>
                  <div class="md-form form-group{{ $errors->has('bank_ifsc') ? ' has-error' : '' }}">
                    {{ Form::label('bank_ifsc', 'IFSC Code') }}
                    {{ Form::text('bank_ifsc', '', array('class' => 'form-control')) }}
                    @if ($errors->has('bank_ifsc'))
                      <span class="help-block error-label">
                        <label>{{ $errors->first('bank_ifsc') }}</label>
                      </span>
                    @endif
                  </div>
                  <div class="md-form form-group{{ $errors->has('bank_branch') ? ' has-error' : '' }}">
                    {{ Form::label('bank_branch', 'Bank Branch') }}
                    {{ Form::text('bank_branch', '', array('class' => 'form-control')) }}
                    @if ($errors->has('bank_branch'))
                      <span class="help-block error-label">
                        <label>{{ $errors->first('bank_branch') }}</label>
                      </span>
                    @endif
                  </div>
                  <div class="md-form form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                    {{ Form::label('account_number', 'Account Number') }}
                    {{ Form::text('account_number', '', array('class' => 'form-control')) }}
                    @if ($errors->has('account_number'))
                      <span class="help-block error-label">
                        <label>{{ $errors->first('account_number') }}</label>
                      </span>
                    @endif
                  </div>                           
                   <div class="form-group">
                    {{ Form::submit('Add', array('class' => 'btn btn-primary primaryModal')) }}
                    <a href="{{ url('/bank')}}" class="btn closebtn">Back</a>
                  </div>
                {{ Form::close() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection