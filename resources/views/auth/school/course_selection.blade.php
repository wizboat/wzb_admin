<!DOCTYPE html>
<html>
<title>Wizboat Login</title>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Google+Sans:400,500&amp;lang=en&amp;display=fallback" nonce="ACUEifZ_4E3itaNgpkugZA">
</head>
<body>
  <!--Navbar -->
  <nav class="mb-1 navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand" href="#"><img src="{{ url('assets/images/logo.png') }}" width="120" class="img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
      aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-user"></i> {{ Auth::user()->name }} </a>

          <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
            <a class="dropdown-item" href="#">Change Password</a>
            <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    </ul>
  </div>
</div>
</nav>
<!--/.Navbar -->

<!-- School Account login -->
<div class="padtp schoolsconfirmation">
  <div class="confirmation_ver">
    <div class="container">

      <div class="row pt-5 d-flex justify-content-center">
        <div class="col-md-2 pl-5 pl-md-0 pb-5">

          <!-- Stepper -->
          <div class="steps-form-3">
            <div class="steps-row-3 setup-panel-3 d-flex justify-content-between">
              <div class="steps-step-3">
                <a href="#step-5" type="button" class="btn btn-info btn-circle-3 waves-effect ml-0" data-toggle="tooltip" data-placement="top" title="Email Verification"><img src="{{ url('assets/images/email.png') }}"></a>
                <p class="weight-bold">Email Verification</p>
              </div>
             <!--  <div class="steps-step-3">
                <a href="#step-6" type="button" class="btn btn-info btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Basic Information"><img src="{{ url('assets/images/card-payment.png') }}"></a>
              </div> -->
              <div class="steps-step-3">
                <a href="#step-7" type="button" class="btn btn-info btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="School Profile"><img src="{{ url('assets/images/university.png') }}"></a>
                <p class="weight-bold">School Profile</p>
              </div>
              <div class="steps-step-3">
                <a href="#step-7" type="button" class="btn btn-info btn-circle-3 waves-effect" data-toggle="tooltip" data-placement="top" title="Profile Verification"><img src="{{ url('assets/images/review.png') }}"></a>
                <p class="weight-bold">Profile Verification</p>
              </div>
              <div class="steps-step-3 no-height">
                <a href="#step-8" type="button" class="btn btn-info btn-circle-3 waves-effect p-3" data-toggle="tooltip" data-placement="top" title="Course Selection"><img src="{{ url('assets/images/books.png') }}"></a>
                <p class="weight-bold">Course Selection</p>
              </div>
            </div>
          </div>

        </div>


        <div class="col-md-8">

          <div class="mt-3">
           <!-- <div class="mdc-top-app-bar__title">Select Your Course</div> -->
           <h3 class="confirmation_header text-left">Select Your Course</h3>
           <!-- <h3><strong>Select Your Course</strong></h3> -->
           <hr>
           <div class="response_message"></div>
           <div class="row justify-content-center mt-5">
            <form name='course_selection' id="course_selection" action="#" method="post" style="display: contents;">
              @csrf
              @foreach($course as $row=>$data)       
              <div class="col-lg-4 col-md-3 col-sm-6" >            
                <div class="card mb-3">
                  <div class="card-body">
                    <div class="select_course_switch">
                      <input type="checkbox" id="switch{{$row}}" name="courseList[{{ $data->id }}]" value="{{ $data->course_name }}">
                      <label class="label-default" for="switch{{$row}}"></label>
                      <span>{{ ucfirst($data->course_name) }}</span>
                    </div>
                  </div>
                </div>                  
              </div>     
              @endforeach
              <div class="col-md-12 text-right">
                <input type="submit" class="btn btn-primary primaryModal mt-4 courseSelectionSubmit" value="Confirm">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/js/mdb.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
  var form = $("[name=course_selection]");
  form.validate(
  {         
    rules: {
      course: {                
        required: true                
      }
    },        
    messages: {
      course: "Please select at least 2 course"        
    },   
    submitHandler: function()
    {
      formdata=$("#course_selection").serialize();

      var a = JSON.stringify(formdata);

      var your_object = JSON.parse(a);

      $.ajax({
        type: "POST", 
        url: "/course-selections/update/{{ Session::get('mongoSchoolProfileId') }}",
        data: formdata,
        async: true ,
        success: function (result)
        {                               
          if(result.code == 200)
          {
            $(".courseSelectionSubmit").remove();
            $('#loader').hide();
            $(".response_message").html("<div class='alert alert-success'><strong>Success ! </strong>" + result.message + "</div>"); 
            setTimeout(function(){ 
                            // window.location.reload();
                          }, 5000);                       

          }
          else{  

            $('#loader').hide();
            $(".response_message").html("<div class='alert alert-danger'><strong>Alert ! </strong>" + result.message + "</div>"); 
            return false;
          }
        },
        beforeSend: function(){
          $('#loader').show();
        }
      });
    }
  });
</script>
</body>
</html>