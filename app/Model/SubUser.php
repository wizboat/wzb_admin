<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubUser extends Model
{
    protected $table = 'sub_user';
    protected $hidden = [];
    protected $fillable = [ 'school_id', 'master_user_id', 'user_id'];

     public function userdata(){
        return $this->belongsTo('App\Model\User','user_id');
    }
}
