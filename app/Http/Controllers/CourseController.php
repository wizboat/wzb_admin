<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use Hash;
use DB;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Course;
use App\Model\Syllabus;
use App\Model\CourseMaster;
use App\Model\SchoolProfile;
use App\Model\linkCourseSchool;
use App\Model\CourseSyllabus;
use App\Model\CourseDetails;
use App\Model\CourseEligibility;

class CourseController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $userId = Auth::user()->id;        
        try{                                                                                                
            $courseMaster  = CourseMaster::wherehas('course',function($q) use ($userId){
                                                    $q->where('user_id', $userId);                                         
                                                })->with('course.courseDetails')->get()->toArray();
                                                                              
            return view('school.course.index',compact('courseMaster'));

        }catch(Exception $e){
            print_r($e); 
            exit;
        }
    }

    public function edit(Request $request,$linkId){
        
        try{
            if(isset($linkId)){
                $linkCourseSchool = linkCourseSchool::where('id',$linkId)->where('user_id', Auth::user()->id)->first();
                if($linkCourseSchool){
                    $result = CourseDetails::where('course_link_id',$linkCourseSchool->id)->first();
                    return view('school.course.edit',compact('result','linkId','linkCourseSchool'));                   
                }else{
                    return redirect('/course');
                }
            }else{
                return redirect('/course');
            }
        }catch(Exception $exception){
            return redirect('/course');
        }
    }

    public function status(Request $request){
        $post = $request->all();

        $validator = \Validator::make($post, ['data'=>'required','id'=>'required']);        
        if ($validator->fails()) {            
            return response()->json(['status' => false, 'message' => $validator]);
        }

        $result = linkCourseSchool::where(['id'=>$post['id'],'user_id'=>Auth::user()->id])->first();
        if($result){

            $result->course_status = ($post['data'] == 0) ? '0' : '1';
            $result->save();

            return response()->json(['status' => true,'msg'=>'Course Status Updated Successfully']);   
        }else{
            return response()->json(['status' => false]);    
        }
        print_r($result);
    }

    public function courseDetailsUpdate(Request $request,$lineCourseId){
        
        $post = $request->all();        
        $validator = \Validator::make($post, ['seat_intake'=>'required','application_fee'=>'required','fee'=>'required']);        
        if ($validator->fails()) {            
            return response()->json(['status' => false, 'message' => $validator]);
        }
        if(!isset($lineCourseId)){
            return response()->json(['status' => false]);   
        }
        try{
            $linkCourseSchool = linkCourseSchool::where('id',$lineCourseId)->where('user_id', Auth::user()->id)->first();
            if(isset($linkCourseSchool)){            
                $courseDetails = CourseDetails::where('course_link_id',$lineCourseId)->first();
                if(isset($courseDetails)){
                    $courseDetails->seat_intake = $post['seat_intake'];
                    $courseDetails->application_fee = $post['application_fee'];
                    $courseDetails->fee = $post['fee'];
                    $courseDetails->save();
                    return response()->json([                    
                        'status' => true,                                        
                        'message' => 'Course Successfully Updated!'
                    ]);
                }else{                    
                    $courseDetails = new CourseDetails;
                    $courseDetails->course_link_id = $lineCourseId;
                    $courseDetails->seat_intake = $post['seat_intake'];
                    $courseDetails->fee = $post['fee'];
                    $courseDetails->application_fee = $post['application_fee'];
                    $courseDetails->save();                    
                    return response()->json([                    
                        'status' => true,                        
                        'message' => 'Course Successfully Updated!'
                    ]);
                }
            }else{
                return response()->json([                  
                    'status' => false,
                    'message' => 'Something went wrong'
                ]);
            }
        }catch(Exception $exception){
            return response()->json([                    
                'status' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }    

    public function eligibilityList(Request $request,$linkId){
      try{

         if(isset($linkId)){

            $linkCourseSchool = linkCourseSchool::where('id',$linkId)->where('user_id', Auth::user()->id)->first();

               if(isset($linkCourseSchool)){
        
                    $courseDetails = CourseDetails::where('course_link_id',$linkId)->first();

                    if(isset($courseDetails)){

                        $result = CourseEligibility::select('id','eligibility','deleted','disabled')->where('deleted',1)->where('course_link_id',$linkId)->orderBy('id','desc')->get()->toArray();

                        return response()->json($result);
                    }else{
                         return response()->json(['status' => false, 'message' => 'Something went wrong1']);
                    }
                }else{

                    return response()->json(['status' => false, 'message' => 'Something went wrong2']);
                }
            }else{

                 return response()->json(['status' => false, 'message' => 'Something went wrong3']);
            }
        }catch(Exception $exception){

            return response()->json(['status' => false, 'message' => 'Something went wrong4']);
        }    
    }

    public function eligibilityStore(Request $request){
        $post = $request->all();
        $userId = Auth::user()->id;
        $validator = \Validator::make($post, ['_h'=>'required','eligibility'=> 'required']);
         if ($validator->fails()) {           
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);     
        }

        $result = LinkCourseSchool::where('user_id',$userId)->where('id',$post['_h'])->first();

        if(isset($result)){
            foreach($post['eligibility'] as $key=>$value){
                if(!empty($value)){                    
                    CourseEligibility::create([
                        'course_link_id' => $post['_h'],
                        'eligibility' => $value
                     ]);
                }

            }
            return response()->json(['status' => true, 'message' => 'Successfully updated']);           
        }else{
            return response()->json(['status' => false, 'message' => 'Something went wrong']);       
        }        
    }

    public function eligibilityDelete(Request $request){

        $post = $request->all();    
      
        $validator = \Validator::make($post, ['__hash'=>'required']);

        if ($validator->fails()) {
            
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);     
        }


        $result = CourseEligibility::where('id', $post['__hash'])->first();

        if($result){
            $result->deleted  = '0';
            $result->save();
        }
    
        if($result->id){
            $message = '<div class="alert alert-success" role="alert"> Deleted Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);       
    }

    public function eligibilityDisable(Request $request){

        $post = $request->all();

        $result = CourseEligibility::where('id', $post['__hash'])->first();

        if(isset($result)){
            if($result->disabled == '1'){

                $result->disabled  = '0';
            }else{
                $result->disabled  = '1';
            }

            $result->save();    
        }else{
            return response()->json(['status' => false, 'message' => '<div class="alert alert-success" role="alert"> Something went wrong</div>']);
        }
        
        if($result->id){
            $message = '<div class="alert alert-success" role="alert"> Disabled Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);
    }

    public function syllabusList(Request $request,$linkId){

        $userId = Auth::user()->id;

        try{
         if(isset($linkId)){
            $linkCourseSchool = linkCourseSchool::where('id',$linkId)->where('user_id', $userId)->first();
               if(isset($linkCourseSchool)){
                    $courseDetails = CourseDetails::where('course_link_id',$linkId)->first();
                    if(isset($courseDetails)){
                        $result = Syllabus::select('id','link_course_school_id AS lnk_id','syllabus_name','deleted','disabled')->where('deleted','1')->where('link_course_school_id',$linkId)->orderBy('id','desc')->get()->toArray();
                        return response()->json($result);
                    }else{
                         return response()->json(['status' => false, 'message' => 'Something went wrong1']);
                    }
                }else{
                    return response()->json(['status' => false, 'message' => 'Something went wrong2']);
                }
            }else{
                 return response()->json(['status' => false, 'message' => 'Something went wrong3']);
            }
        }catch(Exception $exception){
            return response()->json(['status' => false, 'message' => 'Something went wrong4']);
        }
    }

    public function syllabusStore(Request $request){
        $post = $request->all();
        $user_id = Auth::user()->id;
        $validator = \Validator::make($post, ['__hash'=>'required','syllabus'=>'required']);
        if ($validator->fails()) {
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);
        }
        $result = linkCourseSchool::where('id', $post['__hash'])->where('user_id', $user_id)->first();
        if($result){
                foreach ($post['syllabus'] as $key => $value) {
                    if(!empty($value)){
                        Syllabus::create(['link_course_school_id'=>$post['__hash'],'syllabus_name'=>$value]);
                    }

                }
            return response()->json(['status'=>true,'message'=>'Successfully Updated']);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong']);
        }
    }

    public function syllabusDelete(Request $request){

        $post = $request->all();    
      
        $validator = \Validator::make($post, ['__hash'=>'required','__sylbs'=>'required']);

        if ($validator->fails()) {            
            $status = false;
            return response()->json(['status' => $status, 'message' => $validator]);     
        }

        $userId = Auth::user()->id;
        $linkData = linkCourseSchool::where('user_id',$userId)->where('id',$post['__hash'])->first();

        if(!isset($linkData)){
             return response()->json(['status' => false]);     
        }

        $result = Syllabus::where('id', $post['__sylbs'])->first();

        // print_r($result); exit;
        if($result){
            $result->deleted  = '0';
            $result->save();
        }
    
        if($result->id){
            $message = '<div class="alert alert-success" role="alert">Deleted Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert">Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);       
    }

    public function syllabusDisable(Request $request){

        $post = $request->all();

        $validator = \Validator::make($post, ['__hash'=>'required']);

        if ($validator->fails()) {        
            return response()->json(['status' => false]);     
        }

        $result = Syllabus::where('id', $post['__hash'])->first();

        if(isset($result)){
            if($result->disabled == '1'){
                $result->disabled  = '0';
            }else{
                $result->disabled  = '1';
            }

            $result->save();    
        }else{
            return response()->json(['status' => false, 'message' => '<div class="alert alert-success" role="alert"> Something went wrong</div>']);
        }
        
        if($result->id){
            $message = '<div class="alert alert-success" role="alert"> Disabled Successfully!!!</div>';
            $status = true;
        }
        else{
            $message = '<div class="alert alert-success" role="alert"> Something went wrong</div>';
            $status = false;            
        }

        return response()->json(['status' => $status, 'message' => $message]);
    }
}
