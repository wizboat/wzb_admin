@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
<div class="main-panel">
    <section class="content"> 
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h5 class="card-title">Eligibility</h5>
            </div>
            <div class="col-md-12 col-lg-12 alert-message">
            	<textarea name="content" id="editor" style="height: 5rem;"></textarea>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
@endsection