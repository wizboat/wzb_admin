<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseMaster extends Model
{
    protected $table = 'course_master';
    public $timestamps = true;
    protected $hidden = [];
    protected $fillable = ['course_name','course_master_hash','course_status']; 	
    
  	 public function course(){
  	 	
        return $this->hasMany('App\Model\LinkCourseSchool','course_master_id', 'id');
    }
}
