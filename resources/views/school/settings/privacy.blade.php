@extends('school.settings.app')
@section('content')
<section class="content">                
   <a href="{{ url('/settings/changePassword') }}" class="btn btn-outline-primary editBtn pull-right">Edit</a>
   <div class="mdc-top-app-bar__title">Privacy</div>
   <div class="">                    
      <div class="edit-general-info">         		   
         <div class="card">
            <div class="card-body">
               <h5 class="card-title"><strong> Find People and Contact You </strong></h5>
               <div class="pricy_desc">
                  <h6>Every people can lookup your profile.</h6>     
                  <h6>Every people can find your email address and phone numner with address.</h6>
                  <h6>Every student can take admission from any where.</h6>
               </div>
            </div>
         </div>                                         
      </div>
   </div>
</section> 
@endsection