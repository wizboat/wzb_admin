@extends('layouts.app')

@section('content')
<section class="content">                                                     
   <div id="msg"></div>
	<div class="mbdtable">
		<div class="main-panel">
		   <section class="content">
		     	<h2>Confirm Admission List</h2>
		     	<table id="confirm_list" class="table cardmbd table-borderless"></table>
		   </section>
		</div>
	</div>
</section>
<script type="text/javascript">
	var BS_COLUMNS  = [];

	function indexFormatter( value, row, index ){
	  	return index+1;
	}

	function buttonFormatter( value, row, index ){		
	 	return "<a href="+window.location.href+"/d?aid="+row.admission_open_id+" class='btn btn-outline-primary btnTable delete-gallery'>View List</a>";
	}

	BS_COLUMNS.unshift(
	  	{
	      field       : 'id',
	      title       : 'S.No',
	      align       : 'center' ,
	      formatter   : indexFormatter 
	  	},{
	      field       : 'admission_open_id',
	      title       : 'Admission Open ID',
	      align       : 'center'
	  	},{
	      field       : 'course_name',
	      title       : 'Course Name',
	      align       : 'center'
	  	},{
	      field       : 'admission_confirm_date',
	      title       : 'Confirm Date',
	      align       : 'center'
	  	},{
	      field       : 'disabled',
	      title       : 'Action',
	      align       : 'center' ,
	      formatter   : buttonFormatter 
	  	}
	);

	var BS = {
		url             : "{{ url('admission/confirm?ap=1') }}",
		columns         : BS_COLUMNS,
		pagination      : true,
		search          : true,        
		sidePagination  : 'client',
		PageRefresh     : true,
		pageNumber      : 1,
		pageSize        : 10,
		showRefresh     : true
	};

	var t = $("#confirm_list").bootstrapTable(BS);
</script>
@endsection