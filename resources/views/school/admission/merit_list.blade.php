@extends('layouts.app')

@section('content')

<section class="content">                                                     
	<div class="">
		<div id="msg"></div>
		<div class="mbdtable">
			<div class="main-panel">
				<section class="content">
					<h2>Merit or Admission List</h2>
					<div class="mainbar__title">Select course for merit list</div>
					<select name="courseMaster" id="courseMaster" class="form-control">
						@foreach($courseMaster as $value)
						<option value="{{ $value['id'] }}">{{ $value['course_name'] }}</option>
						@endforeach
					</select>
					@if(isset($admissionOpenData) && !empty($admissionOpenData) )
					<div class="mainbar__title">Select admission open Id</div>
					<table class="table">
						@foreach($admissionOpenData as $key=>$value)
						<tr>
							<td>{{ ++$key }}</td>		            		
							<td>{{ $value->admission_open_id }}</td>
							<td>
								@if($value->admission_status == 1)
								<span class="badge badge-success">Active</span>
								@elseif($value->admission_status == 2)
								<span class="badge badge-warning">Inactive</span>
								@elseif($value->admission_status == 0)
								<span class="badge badge-danger">Closed</span>
								@endif
							</td>
							<td><button  onclick='openInNewTab("{{ $value->admission_open_id }}","{{ $courseId }}","{{ $sessionId }}");'>view</button></td>
						</tr>
						@endforeach
					</table>
					@else
					@if($courseId)
					<h4>Not yet opened admission for <span id='course_n'></span></h4>		           
					@endif
					@endif
				</section>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">

	document.getElementById("courseMaster").addEventListener("change", getAdmisionOpenId);

	function getAdmisionOpenId() {
		var x = document.getElementById("courseMaster");
		var url = window.location.href;
		url = url.split('?')[0];	  	
		window.location = url+'?c_i='+x.value;	  	
	}

	$('[name="courseMaster"]').val("{{ isset($courseId) ? $courseId : '' }}");
	var optionText = $("#courseMaster option:selected").text();
	$('#course_n').html(optionText);

	function openInNewTab(a_o_i,c_i,s_i) {
		var meritListUrl = "{{ url('admission/merit-list') }}"+"?c_i="+c_i+"&a_o_i="+a_o_i+"&s_i="+s_i;
		window.open(meritListUrl, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=0,left=200,width=800,height=600");
	}
</script>
@endsection