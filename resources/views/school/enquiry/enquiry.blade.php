@extends('layouts.app')

@section('title', '| Permissions')

@section('content')



@if(Session::get('success_message'))
    <div class="alert alert-success" role="alert"><strong>Success!</strong>&nbsp;&nbsp;{{Session::get('success_message')}}</div>
@endif 
@if ($errors->has('upload_file'))
    <div class="alert alert-danger" role="alert"><strong>Warning!</strong>&nbsp;&nbsp;{{ $errors->first('upload_file') }}</div>
@endif
<!-- Delete -->
<div class="modal fade" id="deletepopup" tabindex="-1" role="dialog" aria-labelledby="deletepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <form id="deleted_gallery" name="deleted_gallery" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Delete</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disabled -->
<div class="modal fade" id="disablepopup" tabindex="-1" role="dialog" aria-labelledby="disablepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to disable these records? This process cannot be undone.</p>
            </div>
            <form id="disabled_gallery" name="disabled_gallery" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Save Change</button> 
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Add Gallery -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter"
aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <div class="">
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="mdc-top-app-bar__title" id="exampleModalLabel">Upload Gallery Images</h5>
            </div>
            <form action="{{ url('gallery/upload') }}" id="gallery_upload" method="post" enctype="multipart/form-data">
                @csrf
                <div id="gallery_popup">
                    <label for="upload_file" class="add_gallerypopup"><i class="fa fa-plus pr-2"></i>Upload Images</label>
                    <input type="file" id="upload_file" class="custom-file-input form-control d-none" name="upload_file[]" onchange="preview_image();" multiple/>
                    <div id="image_preview"><div class="row"></div></div>
                </div>
                <div class="text-right mt-3">
                    <input type="submit" class="btn btn-primary primaryModal" name='submit_image' value="Upload Image"/>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="msg"></div>
<div class="mbdtable">
    <div class="main-panel">
        <section class="content">
            <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus pr-2"></i>Add Gallery</button>
            <div class="mainbar__title">Gallery List </div>
        </section>
    </div>
    <table id="table" class="table cardmbd table-borderless"></table>
</div>
<script type="text/javascript">

    var BS_COLUMNS  = [];

    function indexFormatter( value, row, index ){

        return index+1;
    }

    function logoFormatter( value, row, index ){

        return '<img src="'+value+'" width="100" />';
    }
    
    function statusFormatter( value, row, index ){

        return row.disabled == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
    }

    function disabledFormatter( value, row, index ){

        var disabledBtn = row.disabled == 1 ? "<button class='btn btn-outline-primary disable-gallery btnTable' onclick='disableGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Disable</button>" : "<button class='btn btn-outline-primary disable-gallery btnTable' onclick='disableGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Enable</button>";

        return "<button class='btn btn-outline-danger btn-md mr-3 m-0 weight-bold delete-gallery btnTable' onclick='deleteGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#deletepopup'>Delete</button>"+ disabledBtn;



    }

    BS_COLUMNS.unshift(
        {
            field       : 'id',
            title       : 'S.No',
            align       : 'center' ,
            formatter   : indexFormatter 
        },{
            field       : 'image_path',
            title       : 'Image',
            align       : 'center' ,
            formatter   : logoFormatter 
        },
        {
            field       : 'disabled',
            title       : 'Status',
            align       : 'center' ,
            formatter   : statusFormatter 
        },
        {
            field       : 'image_path',
            title       : 'Image',
            align       : 'center' ,
            formatter   : disabledFormatter 
        }
    );

    var BS = {
        url             : "{{ url('gallery/list') }}",
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 10,
        showRefresh     : true
    };

    var t = $("#table").bootstrapTable(BS);

    $.noConflict();
    $('#enlist').DataTable();

    var form = $("[name=deleted_gallery]");
    form.validate({

        submitHandler: function(form){
            formdata=$("#deleted_gallery").serialize();

            var successFun = function (data){
                
                var result = JSON.parse(data);
                var url = "{{ url('gallery/list') }}";

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#deletepopup").modal("hide");
                $("#loader").hide();

                $("#msg").html(result.message);
            }

            var url = "{{ url('gallery/deleted') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
    });

    var form = $("[name=disabled_gallery]");
    form.validate({

        submitHandler: function(form){
            formdata=$("#disabled_gallery").serialize();

            var successFun = function (data){

                var result = JSON.parse(data);
                var url = "{{ url('gallery/list') }}";

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#disablepopup").modal("hide");
                $("#loader").hide();
                $("#msg").html(result.message);               
            }

            var url = "{{ url('gallery/disabled') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
    });


function preview_image() {

    var total_file=document.getElementById("upload_file").files.length;
    for(var i=0 ; i<total_file ; i++){
        $('#image_preview .row').append("<div class='col-md-3 popimgsec'><img src='"+URL.createObjectURL(event.target.files[i])+"' class='img-fluid w-100'></div>");
    }
}
</script>
@endsection