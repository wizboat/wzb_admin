@extends('layouts.app')

@section('content')
<style type="text/css">
	body{background: #FFF;}
</style>
<section class="content" style="background: #FFF;"> 
   	<div class="row">
      	<div class="col-md-6 mt-5 offset-2">
      		@if (count($errors)) 
		        @foreach($errors->all() as $error) 
		            
		            <div class="alert alert-danger">
	            	<span class="help-block">
	                	<label>{{ $error}}</label>
	            	</span>
	            </div>
		        @endforeach 
			@endif 
      		<div class="card">
		  		<div class="card-header">
		    		<h5 class="card-title mb-0">Edit Syllabus for class V</h5>
		  		</div>
		  		<form action="{{ url('course/syllabus/update') }}" method="post" name="updatesyllabus">
		  			{!! csrf_field() !!}
		  			<input type="hidden" name="_sy1" value="{{ $courseId }}">
			  		<div class="card-body">		    		
			    		<div class="form-group">			    			
			    			<div class="field_wrapper">
			    				@foreach($syllabusData as $key=>$value)
							    <div class="mb-3" style="display: flex;">
							        <input type="text" class="form-control mr-4" name="syllabus_name[{{$value['id']}}]" value="{{ $value['syllabus_name'] }}" placeholder="Syllabus name" />
							        @if($key==0)
							        <a href="javascript:void(0);" class="add_button"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
							        @endif
							    </div>
							    @endforeach
							</div>
			    		</div>
			    		<div class="form-group mr-4 mb-5">
			    			<a href="{{ url('/course')}}" class="btn btn-warning btn-sm pull-right">Back</a>
			    			<input type="submit" name="createsyllabus" class="btn btn-primary btn-sm mr-3 pull-right">
			    		</div>
			  		</div>
				</form>
			</div>
      	</div>
  	</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){

		 var maxField = 10; //Input fields increment limitation
	    var addButton = $('.add_button'); //Add button selector
	    var wrapper = $('.field_wrapper'); //Input field wrapper
	    var fieldHTML = ' <div class="mb-3" style="display: flex;"><input type="text" class="form-control mr-2" name="syllabus_name[]" value="" placeholder="Syllabus name"/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-times" aria-hidden="true"></i></a></div>'; //New input field html 
	    var x = 1; //Initial field counter is 1
	    
	    //Once add button is clicked
	    $(addButton).click(function(){
	        //Check maximum number of input fields
	        if(x < maxField){ 
	            x++; //Increment field counter
	            $(wrapper).append(fieldHTML); //Add field html
	        }
	    });
	    
	    //Once remove button is clicked
	    $(wrapper).on('click', '.remove_button', function(e){
	        e.preventDefault();
	        $(this).parent('div').remove(); //Remove field html
	        x--; //Decrement field counter
	    });

	});
</script>
@endsection