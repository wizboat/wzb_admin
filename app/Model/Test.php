<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;
class Test extends Model
{
	protected $connection = 'mongodb';
    protected $collection = "test";
}
