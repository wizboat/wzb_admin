@extends('layouts.app')

@section('content')

<style type="text/css">
    .admisssion_detail_inner {
        background-color: #fff;
        border-radius: 3px;
        border: 1px solid #ddd;
        padding: 45px;
        -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
    }
    .document_inner button.btn {
        margin: 0;
        padding: 8px 16px;
        background-color: transparent !important;
        color: #000;
    }
    .document_inner .form-control {
        height: 100%;
    }
    .document_inner {
        margin-bottom: 30px;
    }
    .document_inner h5, .document_inner_card h5 {
        margin-bottom: 15px;
        font-size: 18px;
        font-weight: 600;
    }
    .table {
        color: #000;
    }
    table.table th, table.table td {
        padding: 0.75rem;
        font-size: 15px;
    }
    table.table td {
        font-weight: 400;
    }
</style>
<section class="padtp">
    <div class="mdc-top-app-bar__title">Admission Documents</div>
    <div class="document_detail">
        <div class="admisssion_detail_inner">                          
            <div class="document_inner">                      
                <h5>Select Your Course</h5> 
                <select name="course" class="form-control select-dropdown selectpicker" data-size="5" data-dropup-auto="false">
                    <option value="">Select Course</option>
                    <option class="banner_drop" data-tokens="Class I" value="Class I">Class I</option>
                    <option class="banner_drop" data-tokens="Class II" value="Class II">Class II</option>
                    <option class="banner_drop" data-tokens="Class III" value="Class III">Class III</option>
                    <option class="banner_drop" data-tokens="Class IV" value="Class IV">Class IV</option>
                </select>
            </div>
            <div class="document_inner_card">                      
                <h5>Admission Open for Class I</h5>
                <table class="table table-bordered table-hover text-center" width="100%">
                    <thead>
                        <tr>
                            <th width="80">Sr. No.</th>
                            <th>Admission Open ID</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>256563656</td>
                            <td>Active</td>
                            <td><a href="{{ url('/admission_open_id') }}" class="btn btn-primary primaryTable">View Student List</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>65986598</td>
                            <td>Active</td>
                            <td><a href="#" class="btn btn-primary primaryTable">View Student List</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>986598989</td>
                            <td>Closed</td>
                            <td><a href="#" class="btn btn-primary primaryTable">View Student List</a></td>
                        </tr>
                    </tbody>
                </table>                       
            </div>
        </div>
    </div>
</section>
@endsection