<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerifyMail;
use App\User;
use Auth;

class RegisterController extends Controller
{
    use RegistersUsers;
    
    protected $redirectTo = '/home';

    public function __construct(){
        $this->middleware('guest');
        $this->redis = Redis::connection();
    }
    
    protected function showRegistrationForm(){

        return view('auth.register');
    }

    protected function register(Request $request){

        $post = $request->all();

        $validator = Validator::make($request->all(), [            
            'email' => 'required|email|max:50',
            'school_name' => 'required|string|max:65',
            'password' => 'required|min:3|confirmed|max:20',
            'mobile_no' => 'nullable|min:10|max:10'
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }       

        try{       

         
            // if($this->redis->sismember('school_email',$post['email'])){  
            
            //     return redirect()->back()->withInput()->withErrors(['email'=>'The email has already registered.']);      
            // }else{

            //     $redisResult = $this->redis->sadd('school_email',$post['email']); 
            
            //     if($redisResult === 1){

            $userData['name']       = request('school_name');
            $userData['email']      = request('email');
            $userData['password']   = Hash::make(request('password'));
            $userData['otp']        = rand(1,999999);
            $userData['user_hash']  = Hash::make(rand(1,99999));
            $userData['user_role']  = getenv('ADMIN_ROLE');
            $user = User::create($userData);

            Mail::to('chocolateboy7900@gmail.com')->send(new EmailVerifyMail($userData));

            auth()->login($user);

            return redirect('school/email-verify');
                // }else{

                //     $validator = array('serverError'=>'Server error try again');
                //     return redirect()->back()->withInput()->withErrors($validator);  
                // }
            // }
        }catch(Exception $e){

         return redirect('register')->back();
     }        
 }    
}
