<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Wizboat-Online Admission Platform</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/mdb.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/fontawesome.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/css/vendor.bundle.base.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/flag-icon-css/css/flag-icon.min.css') }}">
  <!-- <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/jvectormap/jquery-jvectormap.css') }}"> -->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/demo/dashboard_style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/dashboard-styles.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css"  />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
  <link rel="stylesheet" href="{{ url('assets/css/toastr.min.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css">

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script type="text/javascript" src="{{ url('assets/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/mdb.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/popper.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/moment.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/dataTables.bootstrap4.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
  <script type="text/javascript" src="{{ url('assets/js/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.min.js" ></script>
  <script type="text/javascript" src="{{ url('assets/vendors/js/vendor.bundle.base.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/vendors/chartjs/Chart.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ url('assets/vendors/jvectormap/jquery-jvectormap.min.js') }}"></script>
      <script type="text/javascript" src="{{ url('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> -->
      <script type="text/javascript" src="{{ url('assets/js/material.js') }}"></script>
      <script type="text/javascript" src="{{ url('assets/js/misc.js') }}"></script>
      <script type="text/javascript" src="{{ url('assets/js/dashboard.js') }}"></script>
      <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.js"></script>
      <script src="{{ url('assets/js/_c.js') }}"></script>
      <style type="text/css">
        #toast-container {
          position: fixed;
          z-index: 999999;
        }
        .md-toast-bottom-right {
          right: 12px;
          bottom: 12px;
        }
        #toast-container .md-toast {
          position: relative;
          width: 20.75rem;
          padding: 15px 15px 15px 50px;
          margin: 0 0 6px;
          overflow: hidden;
          color: #fff;
          filter: alpha(opacity=95);
          background-repeat: no-repeat;
          background-position: 15px center;
          -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
          box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
          opacity: .95;
        }
        .md-toast-message {
          word-wrap: break-word;
          font-weight: 500;
          margin-left: 10px;
          letter-spacing: 0.5px;
        }
        .md-toast-info {
          background-color: #33b5e5;
        }
      </style>
    </head>  
    <body>
      <script src="{{ url('assets/js/preloader.js') }}"></script>
      <div id="loader" style="display: none;">
        <img id="loader_image" src="/assets/images/loading.gif">
      </div>
      <div class="body-wrapper">
        @include('layouts.sidebar')
        <div class="main-wrapper mdc-drawer-app-content">
          @include('layouts.header')
          <div class="page-wrapper mdc-toolbar-fixed-adjust">
            <main class="content-wrapper">
              @yield('content')
            </main>
          </div>
          @include('layouts.footer')
        </div>
      </div>
    </body>
    </html>
