@extends('layouts.app')

@section('content')
<div id="msg"></div>
<div class="mbdtable">
  <div class="main-panel">
    <section class="content">
      <div id="message-success"></div>
      <div class="mainbar__title">Confirm Payment </div>
    </section>
  </div>
  <table id="table" class="table cardmbd table-borderless"></table>
</div>
<script type="text/javascript">

  var BS_COLUMNS  = [];

  function indexFormatter( value, row, index ){
    return index+1;
  }

  function statusFormatter( value, row, index ){

    return row.admission_status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
  }

  function disabledFormatter( value, row, index ){

    var url = "{{ url('payment/confirm_payment/:id/view') }}";
    url = url.replace(':id', row.admission_open_id);
    return "<a href="+url+" class='btn btn-outline-primary btnTable'>View</a>"

  }

  BS_COLUMNS.unshift(
  {
    field       : 'id',
    title       : 'Sr.No',
    align       : 'center' ,
    formatter   : indexFormatter 
  },
  {
    field       : 'admission_open_id',
    title       : 'Admission Open ID',
    align       : 'center'
  },{
    field       : 'course_name',
    title       : 'Course Name',
    align       : 'center'
  },
  {
    field       : 'admission_status',
    title       : 'Payment Status',
    align       : 'center' ,
    formatter   : statusFormatter 
  },
  {
    field       : 'disabledFormatter',
    title       : 'Action',
    align       : 'center' ,
    formatter   : disabledFormatter 
  }
  );

  var BS = {
    url             : "{{ url('payment/confirm_payment?i=0') }}",
    columns         : BS_COLUMNS,
    pagination      : true,
    search          : true,        
    sidePagination  : 'client',
    PageRefresh     : true,
    pageNumber      : 1,
    pageSize        : 10,
    showRefresh     : true
  };

  var t= $("#table").bootstrapTable(BS);

</script>

@endsection