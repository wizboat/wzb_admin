<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    protected $table = 'enrollment_details';
    protected $hidden = [];
    public $timestamps = true;
    protected $fillable = ['school_id','enrollment_number','dob', 'deleted']; 
    	
}