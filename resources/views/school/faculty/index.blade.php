@extends('layouts.app')

@section('title', '| Permissions')

@section('content')


@if(Session::get('success_message'))
    <div class="alert alert-success" role="alert"><strong>Success!</strong>&nbsp;&nbsp;{{Session::get('success_message')}}</div>
@endif 
@if ($errors->has('upload_file'))
    <div class="alert alert-danger" role="alert"><strong>Warning!</strong>&nbsp;&nbsp;{{ $errors->first('upload_file') }}</div>
@endif

<!-- Delete -->
<div class="modal fade" id="deletepopup" tabindex="-1" role="dialog" aria-labelledby="deletepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <form id="deleted_gallery" name="deleted_gallery" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Delete</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Disabled -->
<div class="modal fade" id="disablepopup" tabindex="-1" role="dialog" aria-labelledby="disablepopup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <div class="fun-header"> 
                <img src="{{ url('assets/images/alert.png') }}" width="100" class="img-responsive" alt="confirmation alert">
            </div>
            <div class="funconf">
                <h3>Are you sure ?</h3>
                <p>Do you really want to disable these records? This process cannot be undone.</p>
            </div>
            <form id="disabled_gallery" name="disabled_gallery" method="post">
                <div class="">
                    @csrf
                    <input type="hidden" name="__hash" class="__hash">        
                </div>
                <div class="fun-footer"> 
                    <button type="submit" class="btn btn-primary primaryModal">Save Change</button>
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button> 
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Add Faculty -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter"
aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="card-body modal-content">
            <div class="">
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="mdc-top-app-bar__title" id="exampleModalLabel">Add Faculty</div>
            </div>
            {{ Form::open(array('url' => 'faculty','files' => true, 'name'=>'add_form' ,'enctype' => 'multipart/form-data')) }}
                <div class="mt-0 form-group{{ $errors->has('faculty_name') ? ' has-error' : '' }}">
                    {{ Form::label('faculty_name', 'Faculty Name') }}
                    {{ Form::text('faculty_name', '', array('class' => 'form-control mbd-label')) }}
                    @if ($errors->has('faculty_name'))
                        <span class="help-block">
                            <label>{{ $errors->first('faculty_name') }}</label>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="input-group">

                        <input for="faculty_image" type="text" class="form-control white mbd-label" value="Choose image" readonly>

                        <div class="input-group-btn">
                            <button class="fileUpload btn">
                                <span class="upl" id="upload">Upload Image</span>
                                <input type="file" class="upload up" name="faculty_image" id="up"/>
                            </button>                          
                        </div>
                         (recommonded image 200*200)
                    </div>
                </div>

                <div class="form-group {{ $errors->has('faculty_degree') ? ' has-error' : '' }}">
                    {{ Form::label('faculty_degree', 'Faculty Degree') }}
                    {{ Form::text('faculty_degree','' ,array('class' => 'form-control mbd-label')) }}
                    @if ($errors->has('faculty_degree'))
                        <span class="help-block">
                            <label>{{ $errors->first('faculty_degree') }}</label>
                        </span>
                    @endif
                </div>  
                <div class="form-group float-right mb-0">
                    {{ Form::submit('Add', array('class' => 'btn btn-primary primaryModal add_faculty')) }}
                    <button type="button" class="btn closebtn" data-dismiss="modal">Cancel</button>
                    <!-- <a href="{{ url('/faculty')}}" class="btn closebtn">Back</a> -->
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<div id="msg"></div>
<div class="mbdtable">
    <div class="main-panel">
        <section class="content">
            <div id="message-success"></div>
                <button class="btn btn-primary primaryTable marTable" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-plus pr-2"></i>Add Faculty</button>
            <div class="mainbar__title">Faculty List </div>
        </section>
    </div>
    <table id="table" class="table cardmbd table-borderless"></table>
</div>
<script type="text/javascript">
    var BS_COLUMNS  = [];
    function indexFormatter( value, row, index ){
        return index+1;
    }

    function logoFormatter( value, row, index ){
        return '<img src="'+value+'" width="100" />';
    }
    
    function statusFormatter( value, row, index ){
        return row.disabled == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Disabled</span>';
    }

    function disabledFormatter( value, row, index ){
        var disabledBtn = row.disabled == 1 ? "<button class='btn btn-outline-primary btnTable disable-gallery' onclick='disableGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Disable</button>" : "<button class='btn btn-outline-primary btnTable disable-gallery' onclick='disableGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#disablepopup'>Enable</button>";

        return "<button class='btn btn-outline-danger btnTable delete-gallery' onclick='deleteGallary("+row.id+")' data-h='"+row.id+"' data-toggle='modal' data-target='#deletepopup'>Delete</button>"+ disabledBtn;
    }

    BS_COLUMNS.unshift(
        {
            field       : 'id',
            title       : 'S.No',
            align       : 'center' ,
            formatter   : indexFormatter 
        },{
            field       : 'faculty_name',
            title       : 'Faculty Name',
            align       : 'center'
        },{
            field       : 'faculty_degree',
            title       : 'Faculty Degree',
            align       : 'center'
        },{
            field       : 'faculty_image',
            title       : 'Image',
            align       : 'center' ,
            formatter   : logoFormatter 
        },
        {
            field       : 'disabled',
            title       : 'Status',
            align       : 'center' ,
            formatter   : statusFormatter 
        },
        {
            field       : 'disabled',
            title       : 'Action',
            align       : 'center' ,
            formatter   : disabledFormatter 
        }
    );

    var BS = {
        url             : "{{ url('faculty/list') }}",
        columns         : BS_COLUMNS,
        pagination      : true,
        search          : true,        
        sidePagination  : 'client',
        PageRefresh     : true,
        pageNumber      : 1,
        pageSize        : 10,
        showRefresh     : true
    };

    var t= $("#table").bootstrapTable(BS);

    $.noConflict();
    $('#enlist').DataTable();
    $(".add_faculty").on("click",function(){

       var form = $("[name=add_form]");
           form.validate({
               rules: {                
                   faculty_name: {
                       required: true
                   },faculty_degree: {
                       required: true
                   }
             },
             messages:{
                   faculty_name: {
                       required: "Enter faculty name"
                   },faculty_degree: {
                       required: "Enter faculty degree"
                   }
             },
              submitHandler: function(form){ 
                   $("#loader").fadeIn();
              }
           });  
       }); 

    var form = $("[name=deleted_gallery]");
    form.validate({
        
        submitHandler: function(form){
            formdata=$("#deleted_gallery").serialize();

            var successFun = function (data){
                
                var result = JSON.parse(data);
                var url = "{{ url('faculty/list') }}";

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#deletepopup").modal("hide");
                $("#loader").hide();

                $("#msg").html(result.message);
            }

            var url = "{{ url('faculty/deleted') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
    });

     var form = $("[name=disabled_gallery]");
    form.validate({

        submitHandler: function(form){
            formdata=$("#disabled_gallery").serialize();

            var successFun = function (data){

                var result = JSON.parse(data);
                var url = "{{ url('faculty/list') }}";

                t.bootstrapTable('refresh', {
                    url: url
                }); 

                $("#disablepopup").modal("hide");
                $("#loader").hide();
                $("#msg").html(result.message);               
            }

            var url = "{{ url('faculty/disabled') }}";
            AjaxCall("POST",url,formdata,true,successFun);
        }
    });
</script>
@endsection